#module writeContract
import os

def generate(name,trajet):
	
	os.chdir("../trajectory")

	contracts = open(name,"w") 
	contracts.write("FlightId,LatitudeDegrees,LongitudeDegrees,AltitudeFeet,AirspeedKnots")
	
	for point in trajet:
		contracts.write("\n301817127,"+str(point[0])+","+str(point[1])+","+str(point[2])+","+str(point[3]))

	os.chdir("../code")
