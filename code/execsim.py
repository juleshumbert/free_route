import os
import subprocess
from writeContracts import *
from readResult import *
from time import sleep

"execsim -simserver depuis le terminal pour lancer le serveur mto"

"""
Un exemple de contrat :
[[33,-12,17000,350],[33.536,-15.9112,41000,450],[33.942,-18.408,17000,450]]
"""

"""
Parametres :
-  : liste de points de la forme [lat,long,h,vitesse] (float)
- contractName : string
- resultName : string
"""


def check_file(resultfilename):
    result = open("../res/" + resultfilename, "r")
#   	except:
#		print("Erreur d'ouverture du fichier resultat de la simulation")
#		sys.exit(0)

    resultFile = result.read()

    if len(resultFile) == 0:
        return False
    else:
        return True

    result.close()
    os.chdir('../code')


def evaluate(trajet=[[33, -12, 17000, 350], [33.536, -15.9112, 41000, 450], [33.942, -18.408, 17000, 450]], resultName="simresult.csv",
             contractName="simcontract.csv",
             meteo="ARPEGE_0.1_IP1_00H12H_201510150000.grib2", timestep=36, initTime=0):
    generate("simcontract.csv", trajet)
    if os.sys.platform == 'linux2':
        os.system('./../bin/grib.usr/bin/sim ../trajectory/' + contractName +
                  ' -out ../res/' + resultName + ' -cache ../bin/grib.usr/bin/.weather')

    else:
        os.chdir('../bin')
        # subprocess.Popen(["simserver.exe -weather
        # ../grib/ARPEGE_0.1_IP1_00H12H_201510150000.grib2 -cache .weather"])

        if len(sys.argv) >= 2:
            if sys.argv[-1] == '-simserver':
                os.system(
                    "start simserver.exe -weather ../grib/ARPEGE_0.1_IP1_00H12H_201510150000.grib2 -cache .weather")

                # Pour etre sur que le serveur a demarre... A modifier et
                # enlever !
                sleep(5)

        os.system('sim ../trajectory/' + contractName +
                  ' -out ../res/' + resultName + ' -cache .weather' + ' -timeStep ' + str(timestep) + ' -initTime ' + str(initTime))

    while check_file(resultName) == False:
        print "echec... relancement"
        os.system('sim ../trajectory/' + contractName +
                  ' -out ../res/' + resultName + ' -cache .weather' + ' -timeStep ' + str(timestep) + ' -initTime ' + str(initTime))

    return readCost(resultName)
    os.chdir('../code')

# if __name__ == "__main__":
#     cout = evaluate(
#         [[33., -12., 25000., 300.], [33.942, -18.408, 25000., 300.]])
#     print cout
#     from readFlight import *

# cout=evaluate([[85,-5,25000.,300.],[85,-16.,25000.,300.]])
#     print("\n\n" + str(cout) + "\n\n")

#     vol = Flight()
#     vol.loadFromCSV("simresult.csv")
#     vol.plotFlight()

#     os.system("pause")


def evaluate2(resultName="result_corde.csv", contractName="contracts_corde.csv", meteo="ARPEGE_0.1_IP1_00H12H_201510150000.grib2"):
    com = './../bin/grib.usr/bin/sim ../trajectory/' + contractName + ' -out ../res/' + \
        resultName + \
        ' -weather ../grib/ARPEGE_0.1_IP1_00H12H_201510150000.grib2'
    if os.sys.platform == 'linux2':
        os.system(com)

    return readCost(resultName)


if __name__ == "__main__":
    cout = evaluate([[33., -12., 25000., 300.],
                     [33.942, -18.408, 25000., 300.]])
    print cout
    from readFlight import *

    cout = evaluate([[85, -5, 25000., 300.], [85, -16., 25000., 300.]])
    print("\n\n" + str(cout) + "\n\n")

    vol = Flight()
    vol.loadFromCSV("simresult.csv")
    vol.plotFlight()

    os.system("pause")
