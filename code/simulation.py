# -*- coding: utf-8 -*-
"""
Created on Tue Jan 12 17:42:14 2016

@author: Rodolphe
"""

import numpy as np
import matplotlib.pyplot as plt
from graph import *
from pylab import *
from execsim import *
from readFlight import *

def makePath(graphe,nodePath,altitude=25000.,speed=300.):

	path=[]
	for node in nodePath:
		path.append([graphe.nodes[node].lat,graphe.nodes[node].longi,graphe.nodes[node].altitude,graphe.nodes[node].speed])
	
	return path
	
def initialisation(graph):
	for nodeNumb in graph.nodes:
		graph.nodes[nodeNumb].cost=-1
		graph.nodes[nodeNumb].distance=-1
		graph.nodes[nodeNumb].marquage=0
	graph.nodes[0,0].cost=0
	graph.nodes[0,0].pathActuel=[(0,0)]
	
def rechercheNoeudMin(graph):
	cost=-1
	for nodeNumb in graph.nodes:
		if graph.nodes[nodeNumb].marquage == 0:
			newCost=graph.nodes[nodeNumb].cost
			if (cost > newCost and newCost != -1)  or cost == -1 :
				cost = newCost
				number = nodeNumb
	return number

def majPathCost(graph,node1,node2):
	cost2=graph.nodes[node2].cost
	cost2passantS1=evaluate(makePath(graph,graph.nodes[node1].pathActuel+[node2]))
	if cost2 > cost2passantS1 or cost2 == -1 :
		graph.nodes[node2].cost=cost2passantS1
		graph.nodes[node2].pathActuel=graph.nodes[node1].pathActuel+[node2]

		
def dijkstra(graph):
	initialisation(graph)
	continuer=1
	while continuer:
		s1=rechercheNoeudMin(graph)
		print float(s1[1])/graph.n*100
		if s1 == (0,graph.n):
			continuer = 0
		else:
			graph.nodes[s1].marquage=1
			for s2 in graph.nodes[s1].children :
				majPathCost(graph,s1,s2)
			
	return graph.nodes[0,graph.n].pathActuel

if __name__ == "__main__":

# Paris-Toulouse
#	A=[43.6,1.43]
#	B=[48.51,2.20]

	A=[53.,18.]
	B=[42.,1.]
	
	pas=100
	angle=0.25
	
	#2 arcs sup mini pour le moment : bug
	nombreArcsSup=2
	
	grapheO=GrapheO(A,B,pas,angle,nombreArcsSup)
	grapheO.plot()

#	pathOptim=dijkstra(grapheO)
#  #path=[[A[0],A[1],25000.,300.],[B[0],B[1],25000.,300.]] 
#  #path entre depart et arrivee = choisi par le modele avion	
#	
#	#path entre depart et arrivee = choisi par le modele avion	
#	path=[[A[0],A[1],25000.,300.],[B[0],B[1],25000.,300.]] 
#	#path entre depart et arrivee = choisi par le modele avion	
#	cout=evaluate(path)
#	print("\n\n\n-------------------\nCout du vol direct modele avion : "+str(cout)+"\n\n")
#	
#	cout=evaluate(makePath(grapheO,pathOptim))
#	print("\n\nCout du vol optimise : "+str(cout)+"\n\n")

	vol=Flight()
	vol.loadFromCSV("pathOptim_dijkstra2.csv")
	
	vol.plotFlightOnGraph(grapheO)
#	volPovede.plotFlightOnGraph(grapheO)
#	volJules.plotFlightOnGraph(grapheO)
#	

