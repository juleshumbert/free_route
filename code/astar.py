<<<<<<< HEAD
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 11:29:14 2016

@author: Rodolphe
"""

from graph import *
from pylab import *
from execsim import *
from readFlight import *
from extractFlight import *
from orthodromie import *
from buildAlt import *
import simulation as sim
import os

class Astar:
	
	def __init__(self,graphe,altitude=35000.,speed=440.):

		self.altitude=altitude
		self.speed=speed		
		
		self.graphe=graphe
		self.s=(0,0)
		self.T=(0,graphe.n)
		
		self.g={(0,0):0}
		self.f={self.s:self.heuristique(self.s)}
		self.peres={(0,0):None}
		
		self.F=[]
		self.O=[self.s]
		
		self.traj=[]
		
	def heuristique(self,node):
		if node != self.T :
			return evaluate(sim.makePath(self.graphe,[node,self.T],self.altitude,self.speed))
		else:
			return 0
	
	def extractNode(self):
		mini=-1
		for node in self.O:
			cost=self.f[node]
			if mini==-1 or cost<mini:
				mini=cost
				minNode=node
		self.O.remove(minNode)
		return minNode
		
	def insertInF(self,node):
		self.F.append(node)
		
	def insertInO(self,node):
		self.O.append(node)
		
	def developNode(self,node):
		for child in self.graphe.nodes[node].children:
			k=evaluate(sim.makePath(self.graphe,[node,child],self.altitude,self.speed))
			if (((child in self.F) == False) and ((child in self.O) == False)):		
				self.g[child] = self.g[node]+k
				self.f[child] = self.g[child]+self.heuristique(child)
				self.peres[child]=node
				self.insertInO(child)
			else:
				if self.g[child] > self.g[node]+k:
					self.g[child] = self.g[node]+k
					self.f[child] = self.g[child]+self.heuristique(child)
					self.peres[child]=node
					self.insertInO(child)
					
	def buildTraj(self,node):
		if node==(0,0):
			return [(0,0)]
		else :
			return self.buildTraj(self.peres[node])+[node]
		
	def compute(self):
		i=0
		while len(self.O) != 0:
			print i
			i=i+1
			x=self.extractNode()
			self.insertInF(x)
			if x == self.T:
				self.traj=self.buildTraj(x)
				return self.traj
			else:
				self.developNode(x)
		return "pas de solution"
		
def testing_set():
	
	longueurGraphes=10.
	angle=0.2
	nombreArcsSup=5
	
	os.chdir("../res")
	res = open("resultats_astar_set2.csv","w") 
	res.write("FlightId,Depart,Arrivee,Cout Direct,Cout A*")
	os.chdir("../code")
	
	i=0
	for trajet in ListFlight():
		i=i+1
		print "\n\n--------\nCalcul trajet "+str(i)+" : "+str(trajet)+"\n----------\n"
		
		depart=trajet[0]
		arrivee=trajet[1]
		
		pas=distanceOrtho(depart,arrivee)/longueurGraphes
		
		grapheO=GrapheO(depart,arrivee,pas,angle,nombreArcsSup)
		grapheO.plot()

		algo = Astar(grapheO)
		algo.compute()
	
		#path entre depart et arrivee = choisi par le modele avion	
		path=[depart,arrivee] 
	
		cout1=evaluate(path)
		print("\n\n\n-------------------\nCout du vol direct modele avion : "+str(cout1)+"\n\n")

		cout2=evaluate(sim.makePath(grapheO,algo.traj))
		print("\n\nCout du vol optimise : "+str(cout2)+"\n\n")

		vol=Flight()
		vol.loadFromCSV("simresult.csv")
	
		vol.plotFlightOnGraph(grapheO)
		
		res.write("\n"+str(i)+","+str(depart)+","+str(arrivee)+","+str(cout1)+","+str(cout2))
		
	res.close()

		

if __name__ == "__main__":
	
	#testing_set()

# Paris-Toulouse
#	A=[43.6,1.43]
#	B=[48.51,2.20]
#	pas=10
#	angle=0.1

#Trajet 2
#	A=[55.,-10.,35000.,440.]
#	B=[65.,12.,35000.,440.]
	
#Trajet 3
#	A=[58.,-10.]
#	B=[68.,12.]

	
#Trajet 4
#	A=[58.,-10.]
#	B=[68.,20.]

	
#Trajet 5
#	A=[68.,20.,35000.,440.]
#	B=[58.,-10.,35000.,440.]


#Trajet 6	
#	A=[70.,20.]
#	B=[58.,-10.]


#Trajet 7
#	A=[40.,-10.]
#	B=[55.,20.]

	
#Trajet 8
	A=[50.,-14.,35000.,440.]
	B=[40.,-25.,35000.,440.]
	
##
#
#LONLI; 50.07472229003906; 11.226388931274414;
#MANGO; 51.692779541015625; 0.7905555367469788;
#TIMUS; 53.68333435058594; 49.43166732788086;
#EELDE; 53.21333312988281; 7.216944217681885;
#ROBTU; 48.823890686035156; 22.375;
	
	A=[50.07472229003906, 11.226388931274414,20001.,366]
	B=[51.692779541015625, 0.7905555367469788,20001.,366]
	
#	A=[53.21,7.21,35000.,366.]
#	B=[48.82,22.37,35000.,366.]
	
#	A=[53.21,7.21,35000.,366.]
#	B=[53.68,49.43,35000.,366.]

	#A=[53.68,49.43,20000.,366.]
	#B=[48.82,22.37,20000.,366.]

#------ PARAMETRES	
	pas=50
	angle=0.2	
	
	#2 arcs sup mini
	nombreArcsSup=5
	
	grapheO=GrapheO(A,B,pas,angle,nombreArcsSup)
	grapheO.plot()

	algo = Astar(grapheO)
	algo.compute()
	
	#path entre depart et arrivee = choisi par le modele avion	
	path=[A,B] 
	
	cout=evaluate(path)
	print("\n\n\n-------------------\nCout du vol direct modele avion : "+str(cout)+"\n\n")

	cout=evaluate(sim.makePath(grapheO,algo.traj))
	print("\n\nCout du vol optimise : "+str(cout)+"\n\n")

	vol=Flight()
	vol.loadFromCSV("simresult.csv")
	
	vol.plotFlightOnGraph(grapheO)
##	volPovede.plotFlightOnGraph(grapheO)
##	volJules.plotFlightOnGraph(grapheO)

=======
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 11:29:14 2016

@author: Rodolphe
"""

from graph import *
from pylab import *
from execsim import *
from readFlight import *
from extractFlight import *
from orthodromie import *
from buildAlt import *
import simulation as sim
import os

class Astar:
	
	def __init__(self,graphe,altitude=35000.,speed=440.):

		self.altitude=altitude
		self.speed=speed		
		
		self.graphe=graphe
		self.s=(0,0)
		self.T=(0,graphe.n)
		
		self.g={(0,0):0}
		self.f={self.s:self.heuristique(self.s)}
		self.peres={(0,0):None}
		
		self.F=[]
		self.O=[self.s]
		
		self.traj=[]
		
	def heuristique(self,node):
		if node != self.T :
			path=sim.makePath(self.graphe,[node,self.T])
			path[1][2]=path[0][2]
			path[1][3]=path[0][3]
			return evaluate(path)
		else:
			return 0
	
	def extractNode(self):
		mini=-1
		for node in self.O:
			cost=self.f[node]
			if mini==-1 or cost<mini:
				mini=cost
				minNode=node
		self.O.remove(minNode)
		return minNode
		
	def insertInF(self,node):
		self.F.append(node)
		
	def insertInO(self,node):
		self.O.append(node)
		
	def developNode(self,node):
		for child in self.graphe.nodes[node].children:
			k=evaluate(sim.makePath(self.graphe,[node,child],self.altitude,self.speed))
			if (((child in self.F) == False) and ((child in self.O) == False)):		
				self.g[child] = self.g[node]+k
				self.f[child] = self.g[child]+self.heuristique(child)
				self.peres[child]=node
				self.insertInO(child)
			else:
				if self.g[child] > self.g[node]+k:
					self.g[child] = self.g[node]+k
					self.f[child] = self.g[child]+self.heuristique(child)
					self.peres[child]=node
					self.insertInO(child)
					
	def buildTraj(self,node):
		if node==(0,0):
			return [(0,0)]
		else :
			return self.buildTraj(self.peres[node])+[node]
		
	def compute(self):
		i=0
		while len(self.O) != 0:
			print i
			i=i+1
			x=self.extractNode()
			self.insertInF(x)
			if x == self.T:
				self.traj=self.buildTraj(x)
				return self.traj
			else:
				self.developNode(x)
		return "pas de solution"
		
def testing_set():
	
	longueurGraphes=10.
	angle=0.2
	nombreArcsSup=5
	
	os.chdir("../res")
	res = open("resultats_astar_set2.csv","w") 
	res.write("FlightId,Depart,Arrivee,Cout Direct,Cout A*")
	os.chdir("../code")
	
	i=0
	for trajet in ListFlight():
		i=i+1
		print "\n\n--------\nCalcul trajet "+str(i)+" : "+str(trajet)+"\n----------\n"
		
		depart=trajet[0]
		arrivee=trajet[1]
		
		pas=distanceOrtho(depart,arrivee)/longueurGraphes
		
		grapheO=GrapheO(depart,arrivee,pas,angle,nombreArcsSup)
		grapheO.plot()

		algo = Astar(grapheO)
		algo.compute()
	
		#path entre depart et arrivee = choisi par le modele avion	
		path=[depart,arrivee] 
	
		cout1=evaluate(path)
		print("\n\n\n-------------------\nCout du vol direct modele avion : "+str(cout1)+"\n\n")

		cout2=evaluate(sim.makePath(grapheO,algo.traj))
		print("\n\nCout du vol optimise : "+str(cout2)+"\n\n")

		vol=Flight()
		vol.loadFromCSV("simresult.csv")
	
		vol.plotFlightOnGraph(grapheO)
		
		res.write("\n"+str(i)+","+str(depart)+","+str(arrivee)+","+str(cout1)+","+str(cout2))
		
	res.close()

		

if __name__ == "__main__":
	
	#testing_set()

# Paris-Toulouse
#	A=[43.6,1.43]
#	B=[48.51,2.20]
#	pas=10
#	angle=0.1

#Trajet 2
#	A=[55.,-10.,35000.,440.]
#	B=[65.,12.,35000.,440.]
	
#Trajet 3
#	A=[58.,-10.]
#	B=[68.,12.]

	
#Trajet 4
#	A=[58.,-10.]
#	B=[68.,20.]

	
#Trajet 5
#	A=[68.,20.,35000.,440.]
#	B=[58.,-10.,35000.,440.]


#Trajet 6	
#	A=[70.,20.]
#	B=[58.,-10.]


#Trajet 7
#	A=[40.,-10.]
#	B=[55.,20.]

	
#Trajet 8
	A=[50.,-14.,35000.,440.]
	B=[40.,-25.,35000.,440.]
	
##
#
#LONLI; 50.07472229003906; 11.226388931274414;
#MANGO; 51.692779541015625; 0.7905555367469788;
#TIMUS; 53.68333435058594; 49.43166732788086;
#EELDE; 53.21333312988281; 7.216944217681885;
#ROBTU; 48.823890686035156; 22.375;
	
#	A=[50.,11.,20000.,366.]
#	B=[51.,0.,20000.,366.]
	
	A=[53.21,7.21,20000.,366.]
	B=[48.82,22.37,20000.,366.]
#	
#	A=[50.07472229003906,11.226388931274414,20000.,366.]
#	B=[53.21333312988281, 7.216944217681885,38000.,366.]
#
#	A=[51.692779541015625, 0.7905555367469788,20000.,366.]
#	B=[48.82,22.37,20000.,366.]

#------ PARAMETRES	
	pas=20
	angle=0.25	
	
	#2 arcs sup mini
	nombreArcsSup=5
	
	grapheO=GrapheO(A,B,pas,angle,nombreArcsSup)
	grapheO.plot()
	
#	print grapheO.nodes[(0,0)].altitude,grapheO.nodes[(0,0)].speed
#	print grapheO.nodes[(0,1)].altitude,grapheO.nodes[(0,1)].speed
#	
#	for node in grapheO.nodes:
#		
#		print node
#		print (grapheO.nodes[node].altitude,grapheO.nodes[node].speed)

#
	algo = Astar(grapheO)
	algo.compute()
	
	#path entre depart et arrivee = choisi par le modele avion	
	path=[A,B] 
	
	cout=evaluate(path)
	print("\n\n\n-------------------\nCout du vol direct modele avion : "+str(cout)+"\n\n")

	cout=evaluate(sim.makePath(grapheO,algo.traj))
	print("\n\nCout du vol optimise : "+str(cout)+"\n\n")

	vol=Flight()
	vol.loadFromCSV("simresult.csv")
	
	vol.plotFlightOnGraph(grapheO)
##	volPovede.plotFlightOnGraph(grapheO)
##	volJules.plotFlightOnGraph(grapheO)

>>>>>>> 0f0bfca6115ec7d96ad52f533ca0d2feea943af7
