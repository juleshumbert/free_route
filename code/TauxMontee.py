import numpy as np
import matplotlib.pyplot as plt
import GoTo3D as gt3
import orthodromie as ort

def tauxMontee(alt = 25000, wgt = 161388):
	tc = np.exp(-3.775229+3.226931*10**(-6)*np.sqrt(alt)+5.633243*10**(-6)*wgt+\
			2.077038*10**(-11)*alt**(1.2)*wgt)
	taux = 1/(tc*((3.226931*10**(-6)+2.077038*10**(-11)*wgt)*1.2*alt**(0.2)))
	return taux		
def tauxDescente(alt = 35000):
	return -1/(0.00017045*alt **(-0.3))

class simuMontee:
	
	def __init__(self,dep=[40,10,25000,300],altMax = 35000, dt = 60):
		self.dep = dep
		self.gt3 = gt3.interpolateAlt()
		self.alt = [dep[2]]
		self.t = [0]
		self.v = [dep[3]]
		#dt en secondes
		self.dt = 60
		self.altMax = altMax
		self.endtime = 0
		self.distance = 0
	def simuMontee(self):
		currentAlt = self.alt[-1]
		currentT = self.t[-1]
		currentV = self.v[-1]
		while currentAlt<self.altMax:
			self.distance += currentV*self.dt/3600 
			currentAlt = min(currentAlt + self.dt*tauxMontee(alt = currentAlt)/3600, self.altMax)
			currentT += self.dt
			currentV = self.gt3.interpolate(alt = currentAlt)
			self.alt.append(currentAlt)
			self.v.append(currentV)
			self.t.append(currentT)
		self.endtime = self.t[-1]	
	def simuOneStep(self,alt = 25000):
		nextAlt = min(alt + self.dt*tauxMontee(alt=alt)/3600,self.altMax)
		nextV = self.gt3.interpolate(alt = nextAlt)
		return nextAlt,nextV
	
class simuDescente:
	
	def __init__(self,arr=[40,10,25000,300],altMax = 35000, dt = 60):
		self.arr = arr
		self.gt3 = gt3.interpolateAlt()
		self.alt = [arr[2]]
		self.t = [0]
		self.v = [arr[3]]
		#dt en secondes
		self.dt = dt
		self.altMax = altMax
		self.endtime = 0
		self.distance = 0
	def simuDescente(self):
		currentAlt = self.alt[-1]
		currentT = self.t[-1]
		currentV = self.v[-1]
		while currentAlt<self.altMax:
			self.distance += currentV*self.dt/3600 
			currentAlt = min(currentAlt - self.dt*tauxDescente(alt = currentAlt)/3600, self.altMax)
			currentT += self.dt
			currentV = self.gt3.interpolate(alt = currentAlt)
			self.alt.append(currentAlt)
			self.v.append(currentV)
			self.t.append(currentT)
		self.endtime = self.t[-1]	
		
	def simuOneStep(self,alt = 35000):
		nextAlt = max(alt + self.dt*tauxDescente(alt=alt)/3600,self.arr[2])
		nextV = self.gt3.interpolate(alt = nextAlt)
		return nextAlt,nextV	

if __name__=="__main__":
	tm = simuMontee()
	tm.simuMontee()
	print tm.alt
	print tm.v
	print tm.distance
	print tm.endtime
	print tm.t
	
	tm = simuDescente()
	tm.simuDescente()
	print tm.alt[::-1]
	print tm.v[::-1]
	print tm.distance
	print tm.endtime
	print tm.simuOneStep()