'''
Created on 7 dec. 2015

@author: Guillaume
'''
from abc import ABCMeta
from graph import *
from execsim import *
import interpolator as it
import csv


class Pathfinding():
    __metaclass__ = ABCMeta

    def __init__(self, graph):
        self.graph = graph

    def __iter__(self):
        while False:
            yield None


class PathDijkstra(Pathfinding):

    def __init__(self, graph, nodeDep, nodeEnd):
        self.graph = graph
        self.nodeDep = nodeDep
        self.nodeEnd = nodeEnd
        self.rangeLat = [0, 0]
        self.rangeLong = [0, 0]
        self.rangeLat[0] = min(graph[nodeDep].lat, graph[nodeEnd].lat)
        self.rangeLat[1] = max(graph[nodeDep].lat, graph[nodeEnd].lat)
        self.rangeLong[0] = min(graph[nodeDep].longi, graph[nodeEnd].longi)
        self.rangeLong[1] = max(graph[nodeDep].longi, graph[nodeEnd].longi)

    def load_map(self, u, v, lats, lons, alt, time):
        inte = it.interpol_from_npz(u, v, lats, lons, alt, time)
        self.map = inte

    # Demarque tous les noeuds du graphe sauf le noeud de depart
    # Met leur cout a + infini. Sauf pour noeud de depart
    def initializeGraph(self):
            for node in self.graph:
                self.graph[node].marquage = False
                self.graph[node].positionPrec = []
                self.graph[node].cost = float('inf')

            self.graph[self.nodeDep].marquage = True
            self.graph[self.nodeDep].cost = 0
            self.graph[self.nodeDep].positionPrec = [self.nodeDep]

    # Calcule une estimation de cout entre un noeud et l'arrivee (utilise la
    # ligne droite)
    def calculateHeuristic(self):
        for node in self.graph:
            self.graph[node].heuris = evaluate(
                [[self.graph[node].lat, self.graph[node].longi, 25000., 300.],
                 [self.graph[self.nodeEnd].lat, self.graph[self.nodeEnd].longi, 25000., 300.]])
            # self.graph[node].heuris += self.heuristique(node,250.,3600.)
        self.graph[self.nodeDep].totalcost = self.graph[self.nodeDep].heuris
        self.graph[self.nodeEnd].heuris = 0

    # Algorithme de plus court chemin de dijkstra, renvoie le parcours optimal
    # Renvoie liste de noeuds
    # son cout
    def dijkstra(self):

        self.initializeGraph()
        nodes = set(self.graph)

        while nodes:
            min_node = None
            for node in nodes:
                if self.graph[node].marquage == True:
                    if min_node is None:
                        min_node = node
                    elif self.graph[node].cost <= self.graph[min_node].cost:
                            min_node = node

            nodes.remove(min_node)

            current_weight = self.graph[min_node].cost

            for child in self.graph[min_node].children:
                pathTmp = list(self.graph[min_node].positionPrec)
                pathTmp.append(child)
                weight = self.evaluateDistance(pathTmp)

                if (self.graph[child].marquage == False) or (weight < self.graph[child].cost):
                    self.graph[child].cost = weight
                    self.graph[child].positionPrec = pathTmp
                    self.graph[child].marquage = True

        return self.graph[self.nodeEnd].positionPrec, self.graph[self.nodeEnd].cost

    # Variante de l'algorithme de dijkstra avec un choix heuristique du prochain noeud a marquer
    # Encore a travailler
    def a_star(self):

        self.initializeGraph()
        self.calculateHeuristic()
        nodes = set(self.graph)

        while nodes:
            min_node = None
            for node in nodes:
                if self.graph[node].marquage == True:
                    if min_node is None:
                        min_node = node
                    elif self.graph[node].totalcost <= self.graph[min_node].totalcost:
                            min_node = node

            nodes.remove(min_node)
            if(min_node == self.nodeEnd):
                print('fin')
                break

            current_weight = self.graph[min_node].cost

            for child in self.graph[min_node].children:
                pathTmp = list(self.graph[min_node].positionPrec)
                pathTmp.append(child)
                weight = self.evaluateDistance(pathTmp)

                if (self.graph[child].marquage == False) or (weight < self.graph[child].cost):
                    self.graph[child].cost = weight
                    self.graph[child].positionPrec = pathTmp
                    self.graph[child].marquage = True
                    self.graph[child].totalcost = weight + self.graph[
                        child].heuris
        return self.graph[self.nodeEnd].positionPrec, self.graph[self.nodeEnd].cost

    # Evalue le cout du trajet defini par une liste d'identifiant de noeud
    # 'pathTmp'
    def evaluateDistance(self, pathTmp):
        if len(pathTmp) == 1:
            return 0
        else:
            return evaluate(pathToList(self.graph, pathTmp))

    # plot le graph et la map des vents a l altitude alt au temps t
    # avec un pas spatial de h (en degre latitude, longitude)
    def plotMapGraph(self, h, alt, t):
        plotGraph(self.graph)
        self.map.plot2(h, alt, t)

    # plot le graphe et le chemin p en etoile rouge
    # permet de visualiser le plus court chemin par exemple
    def plotParcours(self, h, alt, t, p):
        self.plotMapGraph(h, alt, t)
        for node in p:
            plt.plot(self.graph[node].longi, self.graph[node].lat, 'r*')

    # definit une heuristique pour le choix de noeud : on prend celui qui a le vent
    # le plus dirige vers la destination
    def heuristique(self, node, alt, t):
        lat = self.graph[node].lat
        longi = self.graph[node].longi
        # estimation du vent au noeud
        wind = self.map.inter(lat, longi, alt, t)
        dirEnd = np.array([self.graph[self.nodeEnd].lat - self.graph[node].lat,
                           self.graph[self.nodeEnd].longi - self.graph[node].longi])

        dirDeb = np.array([self.graph[self.nodeDep].lat - self.graph[node].lat,
                           self.graph[self.nodeDep].longi - self.graph[node].longi])
        distEnd = np.linalg.norm(dirEnd)
        if(distEnd != 0):
            dirEnd = dirEnd / distEnd
        distDeb = np.linalg.norm(dirDeb)
        print(distEnd)
        print(np.dot(wind, dirEnd))
        return -np.dot(wind, dirEnd)


def pathToList(graph, path):
    list = []
    for node in path:
        list.append([graph[node].lat, graph[node].longi, 25000., 300.])

    return list


def saveParcours(graphe, p, cost):
    c = csv.writer(open("../res/resultatOptim.csv", "wb"))
    c.writerow(["Etape", "Latitude", "Longitude", "Altitude", "Vitesse"])

    l = pathToList(graphe, p)

    i = 0
    for e in l:
        i = i + 1
        c.writerow([i, e[0], e[1], e[2], e[3]])

    c.writerow("Cout")
    c.writerow(cost)


if __name__ == "__main__":
    A = [33., -12.]
    B = [33.942, -18.408]

    A = [40., -18.]e
    B = [45., -18.2]
    angleMin = 0.5
    angleMax = 4
    nbArcs = 2
    nbPointsParArc = 3

    graphe = makeGraph(A, B, nbArcs, nbPointsParArc, angleMin, angleMax)

    pd = PathDijkstra(graphe, 'Depart', 'Arrivee')
    pd.load_map('u_small.npz', 'v_small.npz', 'lats_small.npz',
                'lons_small.npz', 'altitude_small.npz', 'time_small.npz')

    p, c = pd.a_star()
    # p,c = pd.dijkstra()

    print 'Cout : '
    print(c)
    print 'Parcours :'
    print(p)
    pd.plotParcours(0.2, 250., 3600., p)
    plt.show()

    saveParcours(graphe, p, c)
