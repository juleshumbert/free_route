import pygrib
from scipy import interpolate
from datetime import datetime
import numpy as np


grbs = pygrib.open('../grib/AROME_0.01_HP1_00H_201602251200.grib2')
grbs.seek(0)

grbv = grbs.select(name='U component of wind')

# Get the lats and lons array
grb = grbv[0]
data, lats, lons = grb.data()
time = []
altitude = []

# Get the time and altitude array
t0 = grbv[0].validDate.timetuple()
t0 = t0.tm_hour * 3600 + t0.tm_min * 60 + t0.tm_sec
h = 0
t = 0
for grb in grbv:
    if t != grb.validDate.timetuple():
        t = grb.validDate.timetuple()
        time.append(t.tm_hour * 3600 + t.tm_min * 60 + t.tm_sec)
for grb in grbv:
    h = grb.level
    if not (h in altitude):
        altitude.append(h)
time = np.array(time)
altitude = np.array(altitude)

print len(time), len(altitude)
# Get the values of U
values = np.zeros((len(lats[:, 0]), len(lons[0, :]), len(altitude), len(time)))
grb = grbv[0]
t = grb.validDate.timetuple()
j = 0
i = 0
for grb in grbv:
    print grb.validDate, grb.level, i, j
    if t != grb.validDate.timetuple():
        i = 0
        t = grb.validDate.timetuple()
        data, lats, lons = grb.data()
        values[:, :, i, j] = data
        j += 1
        i += 1
    else:
        data, lats, lons = grb.data()
        values[:, :, i, j] = data
        i += 1


np.savez_compressed('u', values)
np.savez_compressed('lats', lats[:, 0])
np.savez_compressed('lons', lons[0, :])
np.savez_compressed('time', time)
np.savez_compressed('altitude', altitude)



print values[32,15,1,2]
print data[32,15] 

grbv = grbs.select(name='V component of wind')

# Get the values of U
values = np.zeros((len(lats[:, 0]), len(lons[0, :]), len(altitude), len(time)))
grb = grbv[0]
t = grb.validDate.timetuple()
j = 0
i = 0
for grb in grbv:
    print grb.validDate, grb.level, i, j
    if t != grb.validDate.timetuple():
        i = 0
        t = grb.validDate.timetuple()
        data, lats, lons = grb.data()
        values[:, :, i, j] = data
        j += 1
        i += 1
    else:
        data, lats, lons = grb.data()
        values[:, :, i, j] = data
        i += 1

np.savez_compressed('v', values)
