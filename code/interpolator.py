'''
Created on 2 dec. 2015

@author: Guillaume
'''
# from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import numpy as np


class interpol_from_npz:

    '''
    class used to interpolate a 2D wind field in a 4D Domain
    '''
    ordLat = 1
    ordLong = 1
    ordAlt = 1

    def __init__(self, fileU, fileV, fileLat, fileLong, fileAlt, fileT):
        '''
        Construct an instance of interpol
        U : northern speed
        V : Eastern speed
        '''
        # Files Loading
        U = np.load(fileU)
        V = np.load(fileV)
        Lat = np.load(fileLat)
        Long = np.load(fileLong)
        Alt = np.load(fileAlt)
        T = np.load(fileT)

        # Data Extraction
        self.U = U[U.files[0]]
        self.V = V[V.files[0]]
        self.Lat = Lat[Lat.files[0]]
        self.Long = Long[Long.files[0]]
        self.Alt = Alt[Alt.files[0]]
        self.T = T[T.files[0]]

        # Domains
        self.rangeLat = np.array([self.Lat[0], self.Lat[-1]])
        self.rangeLong = np.array([self.Long[0], self.Long[-1]])
        self.rangeAlt = np.array([self.Alt[0], self.Alt[-1]])
        self.rangeT = np.array([self.T[0], self.T[-1]])
        # Discretization step
        self.stepLat = np.abs(
            (self.rangeLat[1] - self.rangeLat[0]) / (len(self.Lat) - 1))
        self.stepLong = np.abs(
            (self.rangeLong[1] - self.rangeLong[0]) / (len(self.Long) - 1))
        self.stepAlt = np.abs(
            (self.rangeAlt[1] - self.rangeAlt[0]) / (len(self.Alt) - 1))
        self.stepT = np.abs(
            (self.rangeT[1] - self.rangeT[0]) / (len(self.T) - 1))
        # Order of discretization : decreasing : -1 , increasing : 1
        # About the time, we suppose it's always ascending..
        if(self.Lat[0] > self.Lat[-1]):
            self.ordLat = -1
        if(self.Long[0] > self.Long[-1]):
            self.ordLong = -1
        if(self.Alt[0] > self.Alt[-1]):
            self.ordAlt = -1

    def inter(self, lat, longi, alt, t):
        # Compute the 'integer part' of corresponding coordinates
        # Not necessarly the nearest ones. We consider it doesn't
        # really matter for lat,long.

        alt = alt2press(alt)
        iLat = np.floor(np.abs((lat - self.rangeLat[0]) / self.stepLat))
        iLong = np.floor(np.abs((longi - self.rangeLong[0]) / self.stepLong))
        iT = np.floor(np.abs((t - self.rangeT[0]) / self.stepT))
        iAlt = min(range(len(self.Alt)), key=lambda i: abs(self.Alt[i] - alt))
        iAlt = 0
        while self.Alt[iAlt] <= alt:
            iAlt = iAlt + 1
        iAlt = iAlt - 1

        # How much the point is close to the 'integer part+1'
        dLat = np.abs((lat - self.Lat[iLat]) / self.stepLat)
        dLong = np.abs((longi - self.Long[iLong]) / self.stepLong)
        dAlt = (alt - self.Alt[iAlt]) / self.stepAlt
        if iAlt < len(self.Alt) - 1:
            dAlt = (alt - self.Alt[iAlt]) / (
                self.Alt[iAlt + 1] - self.Alt[iAlt])

        dT = (t - self.T[iT]) / self.stepT
        # Initialisation of vector at time (iT) et (iT+1)
        resultT = [0, 0]
        resultT1 = [0, 0]
        # We use a simple linear regression along the altitude
        resultT[0] = (1 - dAlt) * self.U[iLat, iLong, iAlt, iT] + \
            dAlt * \
            self.U[
            iLat, iLong, min(iAlt + 1, len(self.Alt) - 1), iT]
        resultT[1] = (1 - dAlt) * self.V[iLat, iLong, iAlt, iT] + \
            dAlt * \
            self.V[
            iLat, iLong, min(iAlt + 1, len(self.Alt) - 1), iT]
        resultT1[0] = (1 - dAlt) * self.U[iLat, iLong, iAlt, min(iT + 1, len(self.T) - 1)] + \
            dAlt * \
            self.U[
            iLat, iLong, min(iAlt + 1, len(self.Alt) - 1), min(iT + 1, len(self.T) - 1)]
        resultT1[1] = (1 - dAlt) * self.V[iLat, iLong, iAlt, min(iT + 1, len(self.T) - 1)] + \
            dAlt * \
            self.V[
            iLat, iLong, min(iAlt + 1, len(self.Alt) - 1), min(iT + 1, len(self.T) - 1)]

#         latplus=min(iLat+1,len(self.Lat)-1)
#         longplus=min(iLong+1,len(self.Long)-1)
#         altplus=min(iAlt+1,len(self.Alt)-1)
#         tplus=min(iT+1,len(self.T)-1)
#
#         P=np.zeros(4)
#         P[0]=np.sqrt((self.Lat[iLat]-lat)**2+(self.Long[iLong]-longi)**2)
#         P[1]=np.sqrt((self.Lat[latplus]-lat)**2+(self.Long[iLong]-longi)**2)
#         P[2]=np.sqrt((self.Lat[iLat]-lat)**2+(self.Long[longplus]-longi)**2)
#         P[3]=np.sqrt((self.Lat[latplus]-lat)**2+(self.Long[longplus]-longi)**2)
#
#         for i in range(4):
#             if P[i]==0:
#                 P=np.zeros(4)
#                 P[i]=1
#                 break
#             else:
#                 P[i]=1/P[i]

# Valeur au temps iT aux quatres coins autour du point ou le vent est approxime
#         result1T0 = [ (1-dAlt)*self.U[iLat,iLong,iAlt,iT]+dAlt*self.U[iLat,iLong,altplus,iT] , \
#                      (1-dAlt)*self.V[iLat,iLong,iAlt,iT]+dAlt*self.V[iLat,iLong,altplus,iT] ]
#         result2T0 = [ (1-dAlt)*self.U[latplus,iLong,iAlt,iT]+dAlt*self.U[latplus,iLong,altplus,iT] , \
#                      (1-dAlt)*self.V[latplus,iLong,iAlt,iT]+dAlt*self.V[latplus,iLong,altplus,iT] ]
#         result3T0 = [ (1-dAlt)*self.U[iLat,longplus,iAlt,iT]+dAlt*self.U[iLat,longplus,altplus,iT] , \
#                      (1-dAlt)*self.V[iLat,longplus,iAlt,iT]+dAlt*self.V[iLat,longplus,altplus,iT] ]
#         result4T0 = [ (1-dAlt)*self.U[latplus,longplus,iAlt,iT]+dAlt*self.U[latplus,longplus,altplus,iT] , \
#                      (1-dAlt)*self.V[latplus,longplus,iAlt,iT]+dAlt*self.V[latplus,longplus,altplus,iT] ]
#
        # Valeur au temps iT+1 aux quatres coins autour du point ou le vent est approxime
#         result1T1 = [ (1-dAlt)*self.U[iLat,iLong,iAlt,tplus]+dAlt*self.U[iLat,iLong,altplus,tplus] , \
#                      (1-dAlt)*self.V[iLat,iLong,iAlt,tplus]+dAlt*self.V[iLat,iLong,altplus,tplus] ]
#         result2T1 = [ (1-dAlt)*self.U[latplus,iLong,iAlt,tplus]+dAlt*self.U[latplus,iLong,altplus,tplus] , \
#                      (1-dAlt)*self.V[latplus,iLong,iAlt,tplus]+dAlt*self.V[latplus,iLong,altplus,tplus] ]
#         result3T1 = [ (1-dAlt)*self.U[iLat,longplus,iAlt,tplus]+dAlt*self.U[iLat,longplus,altplus,tplus] , \
# (1-dAlt)*self.V[iLat,longplus,iAlt,tplus]+dAlt*self.V[iLat,longplus,altplus,tplus] ]
#         result4T1 = [ (1-dAlt)*self.U[latplus,longplus,iAlt,tplus]+dAlt*self.U[latplus,longplus,altplus,tplus] , \
#                      (1-dAlt)*self.V[latplus,longplus,iAlt,tplus]+dAlt*self.V[latplus,longplus,altplus,tplus] ]
#

        # result1=interpolRotation(result1T0,result1T1,dT)
        # result2=interpolRotation(result2T0,result2T1,dT)
        # result3=interpolRotation(result3T0,result3T1,dT)
        # result4=interpolRotation(result4T0,result4T1,dT)

        # result=(P[0]*result1+P[1]*result2+P[2]*result3+P[3]*result4)/np.sum(P)

        # Compute module and argument of wind field
        norm = np.linalg.norm(resultT, 2)
        norm1 = np.linalg.norm(resultT1, 2)
        ang = np.arctan2(resultT[1], resultT[0])
        ang1 = np.arctan2(resultT1[1], resultT1[0])

        # We use a linear regression for the module and angle : the wind will
        # turn and not evolve linearly through time..
        result = (norm + dT * (norm1 - norm)) * np.array(
            [np.cos(ang + (ang1 - ang) * dT), np.sin(ang + (ang1 - ang) * dT)])

        return result

    def plot(self, alt=35000, t=0):
        # plot the entire interpolate field

        XX, YY = np.meshgrid(self.Long, self.Lat)
        Ut = np.zeros((len(self.Lat), len(self.Long)))
        Vt = np.zeros((len(self.Lat), len(self.Long)))

        for i in range(0, len(self.Lat)):

            for j in range(0, len(self.Long)):
                r = self.inter(self.Lat[i], self.Long[j], alt, t)
                Ut[i, j] = r[0]
                Vt[i, j] = r[1]

        plt.quiver(XX, YY, Vt, Ut)
        #CS = plt.contour(XX, YY, Ut)
        #plt.clabel(CS, inline=2, fontsize=10)
        #plt.colorbar(CS, shrink=0.8, extend='both')
        plt.pause(0.0001)
        plt.show()

    def plot2(self, h, alt, t):
        # plot the interpolate wind field with discretization h along both direction
        # For altitude alt, time t
        # limit of the domain

        latmin, latmax, longmin, longmax = self.rangeLat[
            0], self.rangeLat[1], self.rangeLong[0], self.rangeLong[1]
        # Horizontal discrete
        X = np.arange(longmin, longmax, self.ordLong * h)
        # Vertical discrete
        Y = np.arange(latmin, latmax, self.ordLat * h)
        XX, YY = np.meshgrid(X, Y)
        Ut = np.zeros((len(Y), len(X)))  # lat puis long
        Vt = np.zeros((len(Y), len(X)))

        for i in range(0, len(Y)):

            for j in range(0, len(X)):
                r = self.inter(Y[i], X[j], alt, t)
                Ut[i, j] = r[0]
                Vt[i, j] = r[1]

        XX, YY = np.meshgrid(X, Y)
        # lat puis long
        Ut = np.zeros((len(Y), len(X)))
        Vt = np.zeros((len(Y), len(X)))
        Nt = np.zeros((len(Y), len(X)))
        for i in range(0, len(Y)):
            for j in range(0, len(X)):
                r = self.inter(Y[i], X[j], alt, t)
                Ut[i, j] = r[0]
                Vt[i, j] = r[1]
                Nt[i,j] = np.sqrt(r[0]**2+r[1]**2)

        #plt.clf()
        plt.quiver(XX, YY, Vt, Ut)

        CS = plt.contour(XX, YY, Nt)
        # self.plot_map()
        plt.clabel(CS, inline=1, fontsize=10)
        #plt.colorbar(CS, shrink=0.8, extend='both')
        plt.axis([self.rangeLong[0] - 0.2, self.rangeLong[1]
                 + 0.2, self.rangeLat[1] - 0.2, self.rangeLat[0] + 0.2])




    def extract(self, rangelat, rangelong, rangealt, rangeT):
        # Extract a sub interpolator object given by intervals
        # range are supposed to be given in the same order as the original
        # object to make the extraction easier
        idLat = np.floor(
            np.abs((rangelat[0] - self.rangeLat[0]) / self.stepLat)) + 1
        idLong = np.floor(
            np.abs((rangelong[0] - self.rangeLong[0]) / self.stepLong)) + 1
        idAlt = np.floor(
            np.abs((rangealt[0] - self.rangeAlt[0]) / self.stepAlt)) + 1
        idT = np.floor(np.abs((rangeT[0] - self.rangeT[0]) / self.stepT)) + 1
        ieLat = np.ceil(
            np.abs((rangelat[1] - self.rangeLat[0]) / self.stepLat)) + 1
        ieLong = np.ceil(
            np.abs((rangelong[1] - self.rangeLong[0]) / self.stepLong)) + 1
        ieAlt = np.ceil(
            np.abs((rangealt[1] - self.rangeAlt[0]) / self.stepAlt)) + 1
        ieT = np.ceil(np.abs((rangeT[1] - self.rangeT[0]) / self.stepT)) + 1

        Lat = self.Lat[idLat:ieLat]
        Long = self.Long[idLong:ieLong]
        Alt = self.Alt[idAlt:ieAlt]
        T = self.T[idT:ieT]
        U = self.U[idLat:ieLat, idLong:ieLong, idAlt:ieAlt, idT:ieT]
        V = self.V[idLat:ieLat, idLong:ieLong, idAlt:ieAlt, idT:ieT]

        inter_extr = interpol_from_array(U, V, Lat, Long, Alt, T)

    def plot_evolution(self, h, t1, t2, dt, alt):
        plt.ion()
        fig = plt.figure()

        tt = np.arange(t1, t2, dt)

        for i in range(0, len(tt)):
            print tt[i]
            self.plot2(h, alt, tt[i])
            plt.draw()
            plt.pause(0.0001)

    # def plot_map(self):
     #   m = Basemap(resolution=None,llcrnrlon=self.rangeLong[0],llcrnrlat=self.rangeLat[1],urcrnrlon=self.rangeLong[1],urcrnrlat=self.rangeLat[0])
     #  m.bluemarble()

    def extract(self, rangelat, rangelong, rangealt, rangeT, name=''):
        # Extract a sub interpolator object given by intervals
        # range are supposed to be given in the same
        # order as the original object to make the extraction easier

        idLat = np.floor(
            np.abs((rangelat[0] - self.rangeLat[0]) / self.stepLat)) + 1
        idLong = np.floor(
            np.abs((rangelong[0] - self.rangeLong[0]) / self.stepLong)) + 1
        idAlt = np.floor(
            np.abs((rangealt[0] - self.rangeAlt[0]) / self.stepAlt)) + 1
        idT = np.floor(np.abs((rangeT[0] - self.rangeT[0]) / self.stepT)) + 1
        ieLat = np.ceil(
            np.abs((rangelat[1] - self.rangeLat[0]) / self.stepLat)) + 1
        ieLong = np.ceil(
            np.abs((rangelong[1] - self.rangeLong[0]) / self.stepLong)) + 1
        ieAlt = np.ceil(
            np.abs((rangealt[1] - self.rangeAlt[0]) / self.stepAlt)) + 1
        ieT = np.ceil(np.abs((rangeT[1] - self.rangeT[0]) / self.stepT)) + 1
        idLat = np.floor(np.abs((rangelat[0] - self.rangeLat[0]) / self.stepLat))+1
        idLong = np.floor(np.abs((rangelong[0] - self.rangeLong[0]) / self.stepLong))+1
        idAlt = np.floor(np.abs((rangealt[0] - self.rangeAlt[0]) / self.stepAlt))+1
        idT = np.floor(np.abs((rangeT[0] - self.rangeT[0]) / self.stepT))
        ieLat = np.ceil(np.abs((rangelat[1] - self.rangeLat[0]) / self.stepLat))+1
        ieLong = np.ceil(np.abs((rangelong[1] - self.rangeLong[0]) / self.stepLong))+1
        ieAlt = np.ceil(np.abs((rangealt[1] - self.rangeAlt[0]) / self.stepAlt))+1
        ieT = np.ceil(np.abs((rangeT[1] - self.rangeT[0]) / self.stepT))+1

        Lat = self.Lat[idLat:ieLat]
        Long = self.Long[idLong:ieLong]
        Alt = self.Alt[idAlt:ieAlt]
        T = self.T[idT:ieT]
        U = self.U[idLat:ieLat, idLong:ieLong, idAlt:ieAlt, idT:ieT]
        V = self.V[idLat:ieLat, idLong:ieLong, idAlt:ieAlt, idT:ieT]

        inter_extr = interpol_from_array(U, V, Lat, Long, Alt, T, name)
        return inter_extr


class interpol_from_array(interpol_from_npz):

    '''
    class used to interpolate a 2D wind field in a 4D Domain
    '''

    ordLat = 1
    ordLong = 1
    ordAlt = 1
    id1 = 'default'

    def __init__(self, U, V, Lat, Long, Alt, T, id1):
        '''
        Construct an instance of interpol
        U : northern speed
        V : Eastern speed
        '''
        # Data Extraction
        self.U = U
        self.V = V
        self.Lat = Lat
        self.Long = Long
        self.Alt = Alt
        self.T = T
        if(id1 != ''):
            self.id = id1

        # Domains
        self.rangeLat = np.array([self.Lat[0], self.Lat[-1]])
        self.rangeLong = np.array([self.Long[0], self.Long[-1]])
        self.rangeAlt = np.array([self.Alt[0], self.Alt[-1]])
        self.rangeT = np.array([self.T[0], self.T[-1]])

        # Discretization step
        self.stepLat = np.abs(
            (self.rangeLat[1] - self.rangeLat[0]) / (len(self.Lat) - 1))
        self.stepLong = np.abs(
            (self.rangeLong[1] - self.rangeLong[0]) / (len(self.Long) - 1))
        self.stepAlt = np.abs(
            (self.rangeAlt[1] - self.rangeAlt[0]) / (len(self.Alt) - 1))
        self.stepT = np.abs(
            (self.rangeT[1] - self.rangeT[0]) / (len(self.T) - 1))

        # Order of discretization : decreasing : -1 , increasing : 1
        # About the time, we suppose it's always ascending..
        if(self.Lat[0] > self.Lat[-1]):
            self.ordLat = -1
        if(self.Long[0] > self.Long[-1]):
            self.ordLong = -1
        if(self.Alt[0] > self.Alt[-1]):
            self.ordAlt = -1

    def save_to_npz(self):
        name = 'u_' + self.id
        np.savez_compressed(name, self.U)
        name = 'v_' + self.id
        np.savez_compressed(name, self.V)
        name = 'lats_' + self.id
        np.savez_compressed(name, self.Lat)
        name = 'lons_' + self.id
        np.savez_compressed(name, self.Long)
        name = 'time_' + self.id
        np.savez_compressed(name, self.T)
        name = 'altitude_' + self.id
        np.savez_compressed(name, self.Alt)

# 1 feet=0.3048 meter
# altitude in ft
# give the pression corresponding to altitude in Hpa
# source of formula :
# http://pvlib-python.readthedocs.org/en/latest/_modules/pvlib/atmosphere.html#absoluteairmass


def alt2press(altitude):
    ft = 0.3048
    alt = altitude * ft
    press = ((44331.514 - alt) / 11880.516) ** (1 / 0.190263)
    return press
# pressure in HPa
# give the corresponding altitude


def press2alt(pressure):
    alt = (44331.5 - 4946.62 * (pressure / 100) ** (0.190263)) / ft
    return alt

# Interpole le vecteur vent en interpolant sa norme et son angle (pour
# garder le sens physique...)


def interpolRotation(windT0, windT1, dT):
    norm = np.linalg.norm(windT0, 2)
    norm1 = np.linalg.norm(windT1, 2)
    ang = np.arctan2(windT0[1], windT0[0])
    ang1 = np.arctan2(windT1[1], windT1[0])
    # We use a linear regression for the module and angle : the wind will turn
    # and not evolve linearly through time..
    result = (norm + dT * (norm1 - norm)) * np.array(
        [np.cos(ang + (ang1 - ang) * dT), np.sin(ang + (ang1 - ang) * dT)])
    return result
<<<<<<< HEAD
=======


if __name__ == "__main__":


    
    print alt2press(25000.)




    inte=interpol_from_npz('../npz/u.npz','../npz/v.npz','../npz/lats.npz','../npz/lons.npz','../npz/altitude.npz','../npz/time.npz')
    #inte=interpol_from_npz('../npz/u_default.npz','../npz/v_default.npz',\
    #   '../npz/lats_default.npz','../npz/lons_default.npz','../npz/altitude_default.npz','../npz/time_default.npz')
    #inte=interpol_from_npz('../npz/u_bigger.npz', '../npz/v_bigger.npz', '../npz/lats_bigger.npz',\
    #          '../npz/lons_bigger.npz','../npz/altitude_bigger.npz','../npz/time_bigger.npz')
    #inteExtractWind = inte.extract([50,30],[-30,-8],[150,1000],[0,20000],'wind')
    #inteExtractWind.save_to_npz()
    #inteCheckExtract=interpol_from_npz('../npz/u_wind.npz','../npz/v_wind.npz',\
    #                                   '../npz/lats_wind.npz','../npz/lons_wind.npz',\
    #                                   '../npz/altitude_wind.npz','../npz/time_wind.npz')
    #print 'Verification :'
    #print 'latitude', inteCheckExtract.Lat
    #print 'longitude',inteCheckExtract.Long
    #print 'niveau pression ',inteCheckExtract.Alt
    #print 'temps', inteCheckExtract.T

    print 'Full Data recovered'
    fig =plt.figure()
    inte.plot2(1,35000,0)
    plt.savefig("grandecartevents.png")
    plt.show()
    #inteCheckExtract.plot_evolution(0.5, 0, 20000, 1500, 35000)
    #print 'Extraction smaller part'
    #inteTestToulouseParis=inte.extract([49,43],[1.2,2.4],[150,1000],[0,11000],'Par_Toul')
    #inteTestToulouseParis.save_to_npz()
    #print 'latitude', inteTestToulouseParis.Lat
    #print 'longitude',inteTestToulouseParis.Long
    #print 'niveau pression ',inteTestToulouseParis.Alt
    #print 'temps', inteTestToulouseParis.T
    #inteCheckExtract=interpol_from_npz('../npz/u_Par_Toul.npz','../npz/v_Par_Toul.npz',\
    #                                   '../npz/lats_Par_Toul.npz','../npz/lons_Par_Toul.npz',\
    #                                   '../npz/altitude_Par_Toul.npz','../npz/time_Par_Toul.npz')
    #print 'Verification :'
    #print 'latitude', inteCheckExtract.Lat
    #print 'longitude',inteCheckExtract.Long
    #print 'niveau pression ',inteCheckExtract.Alt
    #print 'temps', inteCheckExtract.T
    #intextra=inte.extract([55,40],[0,20],[200,1000],[0,72000],'bigger')
    #intextra.save_to_npz()
    #inte=interpol_from_npz('../npz/u_default.npz','../npz/v_default.npz',\
    #   '../npz/lats_default.npz','../npz/lons_default.npz','../npz/altitude_default.npz','../npz/time_default.npz')
    #inte.plot_evolution(1,0,20000,1000,500)
    #plt.show()

    print alt2press(25000.)
    # inte=interpol_from_npz('u.npz','v.npz','lats.npz','lons.npz','altitude.npz','time.npz')
    # print inte.Alt
    # print 'Full Data recovered'
    # print 'Extraction smaller part'
    # intextra=inte.extract([55,40],[0,20],[200,1000],[0,72000],'bigger')
    # intextra.save_to_npz()
    # inte=interpol_from_npz('u_default.npz','v_default.npz','lats_default.npz','lons_default.npz','altitude_default.npz','time_default.npz')
    #inte.plot_evolution(1,0,20000,1000,500)
    #plt.show()
>>>>>>> 4e125ca9e75da880cc1bbf21c84d74b94b0fccc8
