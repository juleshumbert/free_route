
import csv
import numpy as np




def ListFlight(filename = '20130830_m1.txt'):
	try:
		    cr = csv.reader( open( filename, 'rb'), delimiter=',' )
	except:
			print("Erreur d'ouverture du fichier resultat de la simulation")
			sys.exit(0)

	i=0
	list = []
	for row in cr:
		list.append(row)
	print list[1]	
	l = len(list)	
	
	listDep = [list[i] for i in range(0,l) if i%2==1]
	listEnd = [list[i] for i in range(0,l) if i%2==0 and i>=2]

	listDepMod=[[float(el[1]),float(el[2]),float(el[3]),float(el[4])] for el in listDep if len(el)==5]
	listEndMod=[[float(elem[1]),float(elem[2]),float(elem[3]),float(elem[4])] for elem in listEnd if len(elem)==5]
	
	listFilter=[]
	for i in range(0,len(listDepMod)):
		if abs(listDepMod[i][0]-listEndMod[i][0])+abs(listDepMod[i][1]-listEndMod[i][1])>12:
			listFilter.append([listDepMod[i],listEndMod[i]])
			
	c = csv.writer(open("../trajectory/trajetinteressants.csv","wb"))
	c.writerow(["Etape","Latitude","Longitude","Altitude","Vitesse"])
	i=0
	for e in listFilter:
		i=i+1
		c.writerow([i,e[0][0],e[0][1],e[0][2],e[0][3]])		
		c.writerow([i,e[1][0],e[1][1],e[1][2],e[1][3]])	
	
	return listFilter	
if __name__=="__main__":    
	ListFlight()

