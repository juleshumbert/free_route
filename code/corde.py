import matplotlib.pyplot as plt
import numpy as np
from scipy import interpolate
from execsim import *
from interpolator import *


def dist(position1, position2):
    return np.linalg.norm(position1 - position2)


class fstring:

    def __init__(
            self, stiffness, m, number, lenght, pos_start, pos_end, interp):
        self.listMass = []
        freeLength = lenght / number
        self.listMass.append(mass(pos_start, np.array([0, 0]),
                                  m, freeLength, stiffness))
        self.listMass[0].string = self
        self.interp = interp
        self.plotting = False
        self.velocity_stop = 0.001
        self.acc_stop = 0.002
        for i in range(1, number):
            self.listMass.append(mass(pos_start + (pos_end - pos_start)
                                      * i / float(number - 1),
                                      np.array([0, 0]), m,
                                      freeLength, stiffness))
            self.listMass[i].mass1 = self.listMass[i - 1]
            self.listMass[i - 1].mass2 = self.listMass[i]
            self.listMass[i].fstring = self
        self.interp_pot = interp_pot

    def update(self):

        for i in range(1, len(self.listMass) - 1):
            mass = self.listMass[i]
            mass.calculate_force()
        for i in range(1, len(self.listMass) - 1):
            mass = self.listMass[i]
            mass.integrate(float(0.1))
        cont = False
        velocity_null = True
        for i in range(1, len(self.listMass) - 1):
            if self.listMass[i].velocity > self.velocity_stop:
                velocity_null = False
        if velocity_null:
            for i in range(1, len(self.listMass) - 1):
                if self.listMass[i].norm_acc > self.acc_stop:
                    cont = True
        else:
            cont = True
        return cont

    def initialize(self, type_init):
        pos_end = self.listMass[-1].position
        pos_start = self.listMass[0].position
        direc = pos_end - pos_start
        number = len(self.listMass)
        if type_init == 1:
            for i in range(1, number - 1):
                cos_comp = 3 * np.cos(3.14 * float(i) / float(number - 1))
                self.listMass[i].position = pos_start + np.array(
                    [direc[0] * float(i) / float(number - 1),
                     direc[1] * float(i) / float(number - 1)
                     + cos_comp])

    def plot(self):
        if not(self.plotting):
            self.plotting = True
            global XX, YY, Ut, Vt
            XX, YY = np.meshgrid(self.interp.Long, self.interp.Lat)
            Ut = np.zeros(
                (len(self.interp.Lat), len(self.interp.Long)))
            Vt = np.zeros(
                (len(self.interp.Lat), len(self.interp.Long)))
            for i in range(0, len(self.interp.Lat)):
                for j in range(0, len(self.interp.Long)):
                    r = self.interp.inter(self.interp.Long[j],
                                          self.interp.Lat[i], 30000, 0)
                    Ut[i, j] = r[0]
                    Vt[i, j] = r[1]
        plt.quiver(XX, YY, Vt, Ut)
        CS = plt.contour(XX, YY, Ut)
        plt.axis([self.interp.Long[0], self.interp.Long[
                 -1], self.interp.Lat[-1], self.interp.Lat[0]])

        plt.clabel(CS, inline=2, fontsize=10)
        plt.colorbar(CS, shrink=0.8, extend='both')

        plt.draw()

        x = []
        y = []
        for mass in self.listMass:
            x.append(mass.position[0])
            y.append(mass.position[1])
        plt.figure(2)
        plt.plot(x, y)
        plt.scatter(x, y)
        plt.axis([self.interp.Long[0], self.interp.Long[
                 -1], self.interp.Lat[-1], self.interp.Lat[0]])
        plt.pause(0.01)
        plt.draw()
        plt.clf()

    def to_csv(self):
        csv_file = open("../trajectory/contracts_corde.csv", "wb")
        csv_file.write(
            "FlightId, LatitudeDegrees, LongitudeDegrees, AltitudeFeet, AirspeedKnots")
        for m in self.listMass:
            csv_file.write(
                "\n301817127," + str(m.position[1]) + "," + str(m.position[0]) + ",25000,400")
        csv_file.close()


class mass:

    def __init__(self, initPosition, initSpeed, m, freeLength, stiffness):
        self.mass = m
        self.position = initPosition
        self.speed = initSpeed
        self.velocity = np.linalg.norm(self.speed)
        self.mass1 = []
        self.mass2 = []
        self.force1 = np.array([0, 0])
        self.force2 = np.array([0, 0])
        self.force_visc = np.array([0, 0])
        self.force_att = np.array([0, 0])
        self.force_wind = np.array([0, 0])
        self.force_oth = np.array([0, 0])
        self.kVisc = 0.08
        self.freeLength = freeLength
        self.stiffness = stiffness
        self.acceleration = np.array([0, 0])
        self.fstring = []
        self.norm_acc = np.linalg.norm(self.acceleration)
        self.alt = 25000

    def calculate_force(self):
        # Force of the strings
        k = self.stiffness
        p = self.position
        p1 = self.mass1.position
        p2 = self.mass2.position
        l = self.freeLength

        deltax1 = dist(p1, p) - l
        deltax2 = dist(p2, p) - l
        theta1 = np.arctan2(p[1] - p1[1], p[0] - p1[0])
        theta2 = np.arctan2(p2[1] - p[1], p2[0] - p[0])
        norm_force1 = - k * deltax1
        norm_force2 = k * deltax2

        self.force1 = np.array([norm_force1 * np.cos(theta1),
                                norm_force1 * np.sin(theta1)])
        self.force2 = np.array([norm_force2 * np.cos(theta2),
                                norm_force2 * np.sin(theta2)])

        # Viscous force
        self.force_visc = - self.kVisc * self.speed

        # Wind force

        self.force_wind = self.fstring.interp.inter(
            self.position[0], self.position[1], self.alt, 0)

    def integrate(self, deltaT):
        force = self.force1 + \
            self.force2 + self.force_visc + self.force_wind * 0.0001
        self.acceleration = force / self.mass
        self.speed = self.speed + self.acceleration * deltaT
        self.position = self.position + self.speed * deltaT
        self.velocity = np.linalg.norm(self.speed)
        self.norm_acc = np.linalg.norm(self.acceleration)


class interp_pot():

    def __init__(self, inte, end_point):
        self.Lat = inte.Lat
        self.Long = inte.Long
        xx, yy = np.meshgrid(inte.Long, inte.Lat)
        Ut = np.zeros(
            (len(inte.Lat), len(inte.Long)))
        Vt = np.zeros(
            (len(inte.Lat), len(inte.Long)))
        ps = np.zeros((len(inte.Lat), len(inte.Long)))
        heading_t = np.zeros(
            (len(inte.Lat), len(inte.Long), 2))
        for i in range(0, len(inte.Lat)):
            for j in range(0, len(inte.Long)):
                r = inte.inter(inte.Lat[i],
                               inte.Long[j], alt, 0)
                Ut[i, j] = r[0]
                Vt[i, j] = r[1]
                heading = end_point - np.array([inte.Long[j], inte.Lat[i]])
                heading = heading / (np.linalg.norm(heading)) ** (1 / 4)
                heading_t[i, j, :] = heading
                ps[i, j] = heading[0] * r[1] + heading[1] * r[0]
        gs = np.gradient(-ps)

        # plt.figure(98)
        # plt.axes().set_aspect('equal', 'datalim')
        # plt.quiver(xx, yy, heading_t[:, :, 0], heading_t[:, :, 1])

        # plt.figure(99)
        # plt.axes().set_aspect('equal', 'datalim')
        # CS = plt.contourf(xx, yy, -ps)
        # plt.quiver(xx, yy, gs[0], gs[1])

        self.zx = interpolate.interp2d(xx[0, :], yy[:, 0], gs[0])
        self.zy = interpolate.interp2d(xx[0, :], yy[:, 0], gs[1])

    def inter(self, x, y, z, t):
        c1 = np.array([self.zx(x, y)[0], 0])
        c2 = np.array([0, self.zy(x, y)[0]])
        return -(c1 + c2)


def optim_corde(start_point=np.array([1.43, 43.6]), end_point=np.array([2.20, 48.51]), alt=25000):
    interp = interpol_from_npz(
        '../npz/u_Par_Toul.npz', '../npz/v_Par_Toul.npz', '../npz/lats_Par_Toul.npz', '../npz/lons_Par_Toul.npz', '../npz/altitude_Par_Toul.npz', '../npz/time_Par_Toul.npz')
    c = []
    alt = 25000

    interp_p = interp_pot(interp, end_point)
    corde = fstring(float(1), float(1), 30, 0.87 * np.linalg.norm(start_point - end_point),
                    start_point, end_point,
                    interp_p)
    corde.initialize(1)
    for i in range(1, 1000000):
        if not(corde.update()):
            corde.to_csv()
            break
        if i % 30 == 0:
            corde.plot()
            print i
    c.append(evaluate2())
    print c
alt = 25000
optim_corde()
