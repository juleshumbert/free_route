import numpy as np
import matplotlib.pyplot as plt
from orthodromie import *
from pylab import *
from buildAlt import *

def calculateTheta(x,y,x0,y0,r):
	X=(x-x0)/r
	Y=(y-y0)/r
	#return np.arctan2(Y,X)
	t1=np.arccos(X)
	t2=np.arcsin(Y)
	if (X>=0 and Y>=0) :
		theta=t1
	elif (X>=0 and Y<=0):
		theta=2*np.pi-t1
	elif (X<=0 and Y>=0):
		theta=t1
	else:
		theta=np.pi-t2

	return theta


#A=[1.,1.] point depart
#B=[2.,2.] point arrivee
#gamma en degres
#nb de points par arcs
#retourne un tableau de 4xnbpoints : x,y,x,y pour les deux arcs opposes
def calculerArc(A,B,gamma,nbpoints,position='non_central'):

	#coordonnees 
	xA=A[0]
	yA=A[1]
	xB=B[0]
	yB=B[1]

	#calcul milieu
	xM=(xA+xB)/2
	yM=(yA+yB)/2

	#calcul pente
	#PROBLEME QUAND Yb=Ya
	alpha=(yB-yA)/(xB-xA)

	if(position=='central'):
		x=linspace(xA,xB,nbpoints)
		y=linspace(yA,yB,nbpoints)
		result=[x,y]
	else:
		#conversion en radian
		gamma=np.pi*gamma/180

		#calcul rayon du cercle
		d=np.sqrt((xA-xM)**2+(yA-yM)**2)/2
		r=d/np.sin(gamma)

		#calcul centre du cercle
		coeff=[1+1/(alpha)**2,-2*(xA+yM/alpha+xM/alpha**2-yA/alpha),xA**2+(yM+xM/alpha-yA)**2-r**2]
		x0=np.roots(coeff)
		y0=-1/alpha*x0+yM+1/alpha*xM

		#calcul des bornes theta
		theta01=calculateTheta(xA,yA,x0[0],y0[0],r)
		thetaf1=calculateTheta(xB,yB,x0[0],y0[0],r)
		theta02=calculateTheta(xA,yA,x0[1],y0[1],r)
		thetaf2=calculateTheta(xB,yB,x0[1],y0[1],r)

		if (np.abs(thetaf1-theta01))>np.pi:
			if(theta01>=thetaf1):
				theta01=theta01-2*np.pi
			else:
				thetaf1=thetaf1-2*np.pi
		if (np.abs(thetaf2-theta02))>np.pi:
			if(theta02>=thetaf2):
				theta02=theta02-2*np.pi
			else:
				thetaf2=thetaf2-2*np.pi

		#discretisation theta
		theta1 = linspace(theta01, thetaf1, nbpoints)
		theta2 = linspace(theta02, thetaf2, nbpoints)

		#liste de coordonnees pour les deux arcs
		x1 = x0[0]+r*cos(theta1)
		y1 = y0[0]+r*sin(theta1)
		x2 = x0[1]+r*cos(theta2)
		y2 = y0[1]+r*sin(theta2)

		result=[x1,y1,x2,y2]
	return result

def plotArc(arc,A,B):
	#coordonnees 
	xA=A[0]
	yA=A[1]
	xB=B[0]
	yB=B[1]

	plot(arc[0],arc[1],'ro',arc[2],arc[3],'ro', [xA,xB],[yA,yB],'ro')
	axis([25,40,-20.,-10.])

class Node:
	"""
	-position
	-latitude
	-longitude
	-cost
	-children
	"""

	def __init__(self,position,lat=0,longi=0,cost=0,children=[],altitude=0,speed=0):
		self.children=children
		self.position=position
		self.lat=lat
		self.longi=longi
		self.altitude=altitude
		self.speed=speed
		self.cost=cost

class NodeDijkstra(Node):
	#Sous classe de node, possede un attribut marquage (noeud a ne pas revisiter dans l'algo de Dijkst)
	#et positionPrec : path entre un noeud de depart et le noeud lui meme, egalement utilise
	#dans l'algo
	#heuris cout estime d'un trajet node-arrivee : utilise dans algo A* : generalement ligne droite
	#total cost : estime trajet depart-node-arrivee : =cost+heuris
	def __init__(self,position,lat=0,longi=0,cost=0,children=[],altitude=0,speed=0,distance=-1,marquage=0,pathActuel=[]):
		Node.__init__(self,position,lat,longi,cost,children,altitude,speed)
		self.marquage=marquage
		self.distance=distance
		self.pathActuel=pathActuel
		self.parent=0
		
#Renvoie un graphe sous forme de dictionnaire :
#graph[i,j] = arc number i et noeud number j
#graph[0]=depart graph[nbpoints-1]=arrivee
def makeGraph(A,B,nbAngles,nbPointsParArc,angleMin,angleMax):
	graph = {}
	n=nbPointsParArc
	m=nbAngles
	arc=[]
	for gamma in np.linspace(angleMax,angleMin,num=m):
		arc.append(calculerArc(A,B,gamma,n))

	arc.append(calculerArc(A,B,gamma,n,'central'))
	for j in range(1,n-1):
		graph[m,j]=NodeDijkstra([m,j],arc[m][0][j],arc[m][1][j])


	for i in range(m):
		for j in range(1,n-1):
			graph[i,j]=NodeDijkstra([i,j],arc[i][0][j],arc[i][1][j])
			graph[2*m-i,j]=NodeDijkstra([2*m-i,j],arc[i][2][j],arc[i][3][j])

	

	graph['Depart']=NodeDijkstra('Depart',arc[0][0][0],arc[0][1][0])
	graph['Arrivee']=NodeDijkstra('Arrivee',arc[0][0][n-1],arc[0][1][n-1])

	for j in range(1,n-2):
		for i in range(1,2*m):
			graph[i,j].children=[(i-1,j+1),(i,j+1),(i+1,j+1)]

	for j in range(2*m+1):
		graph['Depart'].children=graph['Depart'].children+[(j,1)]
		graph[j,n-2].children=['Arrivee']

	for i in range(1,n-2):
		graph[0,i].children=[(0,i+1),(1,i+1)]
		graph[2*m,i].children=[(2*m,i+1),(2*m-1,i+1)]

	return graph

def plotGraph(graph):
	x=[]
	y=[]

	for nodeNumb in graph:
		x.append(graph[nodeNumb].lat)
		y.append(graph[nodeNumb].longi)

		for childNumb in graph[nodeNumb].children:
			plot([graph[childNumb].longi,graph[nodeNumb].longi],[graph[childNumb].lat,graph[nodeNumb].lat],'r')

		#print str(nodeNumb)+" : "+str(graph[nodeNumb].children)

	axisScale=0.1
	miniLongi=min(y)
	maxiLongi=max(y)
	miniLat=min(x)
	maxiLat=max(x) 
	deltaLongi=maxiLongi-miniLongi
	deltaLat=maxiLat-miniLat

	if deltaLongi==0:
		deltaLongi=10

	if deltaLat==0:
		deltaLat=10

	plot(y,x,'bo')
	axis([miniLongi-axisScale*deltaLongi,maxiLongi+axisScale*deltaLongi,miniLat-axisScale*deltaLat,maxiLat+axisScale*deltaLat])
	#show()
	
class GrapheO:
	
	"""Classe pour les graphes construits autour de l'orthodromie
	-A : point de depart
	-B : arrivee
	-pas : en NM entre deux points successifs le long de l'orthodromie centrale
	-angle : angle de deviation pour les autres branches, en RADIAN
	
	Le dictionnaire est l'attribut nodes. Depart indexe (0,0), arrive (0,n) n varie selon le pas'
	
	-attribut n = nombre de noeuds par arc	
	
	"""
	
	def __init__(self,A,B,pas,angle,arcsSup):
		self.A=A
		self.B=B
		self.pas=pas
		self.angle=angle
		self.m=arcsSup
		
		self.nodes={}
		
		alt_modele=buildAlt(A,B,0.95)
		
		#Arc central, indexe 0
		i=0
		for position in orthodromie(A,B,pas):
			[altitudeN,speedN]=alt_modele.alt_vit(position[0],position[1])
			self.nodes[0,i]=NodeDijkstra([0,i],position[0],position[1],altitude=altitudeN,speed=speedN)
			i=i+1
		
		#Arcs connexes indexes 1,2 puis 3,4 etc. autour de l'arc central. Il y a n+1 noeuds de 0 a n-1
			
		self.n=i-1
		n=self.n
		m=self.m
		for j in range(1,2*m,2):		
			angle=((j+1)/2)*self.angle			
			
			A=[self.nodes[0,0].lat+np.sin(angle)*(self.nodes[0,1].longi-self.nodes[0,0].longi)+np.cos(angle)*(self.nodes[0,1].lat-self.nodes[0,0].lat),self.nodes[0,0].longi+np.cos(angle)*(self.nodes[0,1].longi-self.nodes[0,0].longi)-np.sin(angle)*(self.nodes[0,1].lat-self.nodes[0,0].lat)]	
			B=[self.nodes[0,n].lat+np.sin(-angle)*(self.nodes[0,n-1].longi-self.nodes[0,n].longi)+np.cos(-angle)*(self.nodes[0,n-1].lat-self.nodes[0,n].lat),self.nodes[0,n].longi+np.cos(-angle)*(self.nodes[0,n-1].longi-self.nodes[0,n].longi)-np.sin(-angle)*(self.nodes[0,n-1].lat-self.nodes[0,n].lat)]	
		
			i=1
			
			#Le pas est modifie pour conserver le meme nombre de noeuds
			for position in orthodromie(A,B,n=n-2):
				[altitudeN,speedN]=alt_modele.alt_vit(position[0],position[1])
				self.nodes[j,i]=NodeDijkstra([j,i],position[0],position[1],altitude=altitudeN,speed=speedN)
				i=i+1
				
			angle=-angle
			A=[self.nodes[0,0].lat+np.sin(angle)*(self.nodes[0,1].longi-self.nodes[0,0].longi)+np.cos(angle)*(self.nodes[0,1].lat-self.nodes[0,0].lat),self.nodes[0,0].longi+np.cos(angle)*(self.nodes[0,1].longi-self.nodes[0,0].longi)-np.sin(angle)*(self.nodes[0,1].lat-self.nodes[0,0].lat)]	
			B=[self.nodes[0,n].lat+np.sin(-angle)*(self.nodes[0,n-1].longi-self.nodes[0,n].longi)+np.cos(-angle)*(self.nodes[0,n-1].lat-self.nodes[0,n].lat),self.nodes[0,n].longi+np.cos(-angle)*(self.nodes[0,n-1].longi-self.nodes[0,n].longi)-np.sin(-angle)*(self.nodes[0,n-1].lat-self.nodes[0,n].lat)]	
		
			i=1
			for position in orthodromie(A,B,n=n-2):
				[altitudeN,speedN]=alt_modele.alt_vit(position[0],position[1])
				self.nodes[j+1,i]=NodeDijkstra([j+1,i],position[0],position[1],altitude=altitudeN,speed=speedN)
				i=i+1
				
		#DCreation des arcs depart
		for j in range(0,2*m+1):
			self.nodes[0,0].children=self.nodes[0,0].children+[(j,1)]
			
		#Creation des arcs arrivee
		for j in range(0,2*m+1):
			self.nodes[j,n-1].children=self.nodes[j,n-1].children+[(0,n)]
			
		#Creations des arcs restants
		for i in range(1,n-1):
			self.nodes[2*m-1,i].children=self.nodes[2*m-1,i].children+[(2*m-1,i+1),(2*m-3,i+1)]
			self.nodes[2*m,i].children=self.nodes[2*m,i].children+[(2*m,i+1),(2*m-2,i+1)]
			self.nodes[0,i].children=self.nodes[0,i].children+[(0,i+1),(1,i+1),(2,i+1)]
			self.nodes[1,i].children=self.nodes[1,i].children+[(0,i+1),(1,i+1),(3,i+1)]
			for j in range(2,2*m-1):
				self.nodes[j,i].children=self.nodes[j,i].children+[(j,i+1),(j-2,i+1),(j+2,i+1)]
				
	def calculatePlot(self):	
		x=[]
		y=[]

		for nodeNumb in self.nodes:
			x.append(self.nodes[nodeNumb].lat)
			y.append(self.nodes[nodeNumb].longi)
	
			for childNumb in self.nodes[nodeNumb].children:
				plot([self.nodes[childNumb].longi,self.nodes[nodeNumb].longi],[self.nodes[childNumb].lat,self.nodes[nodeNumb].lat],'r')
		
		return x,y 
		
	def plot(self):
		x=[]
		y=[]

		x,y=self.calculatePlot	()
	
		axisScale=0.1
		miniLongi=min(y)
		maxiLongi=max(y)
		miniLat=min(x)
		maxiLat=max(x) 
		deltaLongi=maxiLongi-miniLongi
		deltaLat=maxiLat-miniLat

		if deltaLongi==0:
			deltaLongi=10

		if deltaLat==0:
			deltaLat=10

		plot(y,x,'bo')
		axis([miniLongi-axisScale*deltaLongi,maxiLongi+axisScale*deltaLongi,miniLat-axisScale*deltaLat,maxiLat+axisScale*deltaLat])
		show()
		
		return[x,y]


if __name__ == "__main__":
	B=[70.,2.]
	A=[70.,-74.]
	pas=50
	angle=0.1
	
	#2 arcs sup mini pour le moment lol
	nombreArcsSup=2
	
	angleMin=5
	angleMax=20
	nbArcs=1
	nbPointsParArc=10
	
	grapheO=GrapheO(A,B,pas,angle,nombreArcsSup)
	grapheO.plot()
	
	
#	graphe=makeGraph(A,B,nbArcs,nbPointsParArc,angleMin,angleMax)
#	plotGraph(graphe)

