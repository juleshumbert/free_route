<<<<<<< HEAD
import numpy as np
import matplotlib.pyplot as plt
from interpolator import *
from execsim import *
from scipy import interpolate
import csv
from buildAlt import *


def alt_vit(alt):
    return 2.669e2 + alt * 4.985e-3


class fstring:

    def __init__(
            self, stiffness, m, number, lenght, pos_start, pos_end, interp, buildalt):
        self.listMass = []
        freeLength = lenght / number
        self.listMass.append(mass(pos_start, np.array([0, 0]),
                                  m, freeLength, stiffness))
        self.listMass[0].string = self
        self.interp = interp
        self.plotting = False
        self.velocity_stop = 0.02
        self.acc_stop = 0.02
        self.pos_start = pos_start
        self.pos_end = pos_end
        self.buildalt = buildalt
        for i in range(1, number):
            self.listMass.append(mass(pos_start + (pos_end - pos_start)
                                      * i / float(number - 1),
                                      np.array([0, 0, 0]), m,
                                      freeLength, stiffness))
            self.listMass[i].mass1 = self.listMass[i - 1]
            self.listMass[i - 1].mass2 = self.listMass[i]
            self.listMass[i].fstring = self

    def update(self):

        for i in range(1, len(self.listMass) - 1):
            mass = self.listMass[i]
            mass.calculate_force()
        for i in range(1, len(self.listMass) - 1):
            mass = self.listMass[i]
            mass.integrate(float(0.1))
        for i in range(1, len(self.listMass) - 1):
            pt1 = {'lat': self.listMass[i].position[
                1], 'lon': self.listMass[i].position[0]}
            pt2 = {'lat': self.listMass[i + 1].position[1],
                   'lon': self.listMass[i + 1].position[0]}
            dist = self.dist(pt1, pt2)
            self.listMass[i + 1].cumultime = self.listMass[i +
                                                           1].cumultime + dist / self.listMass[i + 1].vit

        cont = False
        velocity_null = True
        for i in range(1, len(self.listMass) - 1):
            if self.listMass[i].velocity > self.velocity_stop:
                velocity_null = False
        if velocity_null:
            for i in range(1, len(self.listMass) - 1):
                if self.listMass[i].norm_acc > self.acc_stop:
                    cont = True
        else:
            cont = True
        return cont

    def dist(self, pt1, pt2):
        phia = np.deg2rad(pt1['lat'])
        phib = np.deg2rad(pt2['lat'])
        dl = np.deg2rad(pt2['lon'] - pt1['lon'])
        rt = 6378
        return np.arccos(np.sin(phia) * np.sin(phib)
                         + np.cos(phia) * np.cos(phib) * np.cos(dl)) * rt / 1.852

    def initialize(self, type_init):
        pos_end = self.listMass[-1].position
        pos_start = self.listMass[0].position
        direc = pos_end - pos_start
        number = len(self.listMass)
        if type_init == 1:
            for i in range(1, number - 1):
                cos_comp = 3 * np.cos(3.14 * float(i) / float(number - 1))
                self.listMass[i].position = pos_start + np.array(
                    [direc[0] * float(i) / float(number - 1),
                     direc[1] * float(i) / float(number - 1)
                     + cos_comp, pos_end[2]])
            # for i in range(1, number - 1):
            #     cos_comp = np.cos(3.14 * float(i) / float(number - 1))
            #     self.listMass[i].speed = np.array(
            #         [direc[0] * float(i) / float(number - 1),
            #          direc[1] * float(i) / float(number - 1)
            #          + cos_comp])

    def plot(self):
        # if not(self.plotting):
        #     self.plotting = True
        #     XX, YY = np.meshgrid(self.interp.Long, self.interp.Lat)
        #     Ut = np.zeros(
        #         (len(self.interp.Lat), len(self.interp.Long)))
        #     Vt = np.zeros(
        #         (len(self.interp.Lat), len(self.interp.Long)))
        #     for i in range(0, len(self.interp.Lat)):
        #         for j in range(0, len(self.interp.Long)):
        #             r = self.interp.inter(self.interp.Long[j], self.interp.Lat[
        #                                   i], self.listMass[0].position[2], 0)
        #             Ut[i, j] = r[0]
        #             Vt[i, j] = r[1]
        #     plt.figure(2)
        #     plt.quiver(XX, YY, Vt, Ut)
        #     CS = plt.contour(XX, YY, Ut)
        #     plt.axis([self.interp.Long[0], self.interp.Long[
        #              -1], self.interp.Lat[-1], self.interp.Lat[0]])

        #     plt.clabel(CS, inline=2, fontsize=10)
        #     plt.colorbar(CS, shrink=0.8, extend='both')

        #     plt.draw()

        x = []
        y = []
        z = []
        d = []
        z_tar = []
        for mass in self.listMass:
            x.append(mass.position[0])
            y.append(mass.position[1])
            z.append(mass.position[2] * 10000)
            z_tar.append(self.buildalt.alt_vit(
                mass.position[1], mass.position[0])[0])
            d.append(self.dist({'lat': mass.position[1], 'lon': mass.position[0]}, {
                     'lat': self.pos_start[1], 'lon': self.pos_start[0]}))
        plt.figure(2)
        plt.plot(x, y)
        plt.scatter(x, y)
        plt.axis([self.interp.Long[0], self.interp.Long[
                 -1], self.interp.Lat[-1], self.interp.Lat[0]])
        plt.pause(0.01)
        plt.draw()
        plt.clf()

        plt.figure(5)
        plt.plot(d, z)
        plt.scatter(d, z)
        plt.scatter(d, z_tar, c='r')
        plt.draw()
        plt.clf()

    def to_csv(self):
        csv_file = open("../trajectory/contracts_corde.csv", "wb")
        csv_file.write(
            "FlightId, LatitudeDegrees, LongitudeDegrees, AltitudeFeet, AirspeedKnots")
        for m in self.listMass:
            csv_file.write(
                "\n301817127," + str(m.position[1]) + "," + str(m.position[0]) + "," + str(
                    m.position[2] * 10000) + "," + str(alt_vit(m.position[2] * 10000)))
        csv_file.close()


class mass:

    def __init__(self, initPosition, initSpeed, m, freeLength, stiffness):
        self.mass = m
        self.position = initPosition
        self.speed = initSpeed
        self.velocity = np.linalg.norm(self.speed)
        self.mass1 = []
        self.mass2 = []
        self.force1 = np.array([0, 0, 0])
        self.force2 = np.array([0, 0, 0])
        self.force_visc = np.array([0, 0, 0])
        self.force_att = np.array([0, 0, 0])
        self.force_wind = np.array([0, 0, 0])
        self.force_oth = np.array([0, 0, 0])
        self.kVisc = 0.5
        self.cumultime = 0
        self.vit = 0
        self.freeLength = freeLength
        self.stiffness = stiffness
        self.acceleration = np.array([0, 0, 0])
        self.fstring = []
        self.up_force = np.array([0, 0, 0])
        self.norm_acc = np.linalg.norm(self.acceleration)

    def ps_wind_head(self, pos, t):
        r = self.fstring.interp.inter(pos[1], pos[0], pos[2] * 10000, t)
        heading = self.fstring.pos_end[0:2] - pos[0:2]
        heading = heading / (np.linalg.norm(heading)) ** (1 / 4)
        ps = heading[0] * r[1] + heading[1] * r[0]
        return ps

    def calculate_force(self):
        # Force of the strings
        k = self.stiffness
        p = self.position
        p1 = self.mass1.position
        p2 = self.mass2.position
        l = self.freeLength
        pn = p2 - p
        pp = p - p1

        deltax1 = np.linalg.norm(pp) - l
        deltax2 = np.linalg.norm(pn) - l

        norm_force1 = - k * deltax1
        norm_force2 = k * deltax2

        self.force1 = norm_force1 * pp / np.linalg.norm(pp)
        self.force2 = norm_force2 * pn / np.linalg.norm(pn)

        # Viscous force
        self.force_visc = - self.kVisc * self.speed

        # Wind force
        dx = 0.1
        x_n = self.ps_wind_head(
            self.position + [dx, 0, 0], np.floor(self.cumultime))
        x_p = self.ps_wind_head(
            self.position + [-dx, 0, 0], np.floor(self.cumultime))
        y_n = self.ps_wind_head(
            self.position + [0, dx, 0], np.floor(self.cumultime))
        y_p = self.ps_wind_head(
            self.position + [0, -dx, 0], np.floor(self.cumultime))
        self.force_wind = -1.2 * np.array(
            [(x_p - x_n) / (2 * dx), (y_p - y_n) / (2 * dx), 0])

        # Upward force

        targetalt, self.vit = self.fstring.buildalt.alt_vit(
            self.position[1], self.position[0])

        if self.position[2] * 10000 < targetalt:
            if self.position[2] * 10000 >= targetalt - 1000:
                if self.position[2] * 10000 <= targetalt - 100:
                    self.up_force = (
                        targetalt - self.position[2] * 10000) / 1000 * np.array([0, 0, 1])
                else:
                    self.up_force = np.array([0, 0, 0])
            else:
                self.up_force = np.array([0, 0, 1])

        if self.position[2] * 10000 > targetalt:
            if self.position[2] * 10000 <= targetalt + 1000:
                if self.position[2] * 10000 >= targetalt + 100:
                    self.up_force = (
                        targetalt - self.position[2] * 10000) / 1000 * np.array([0, 0, 1])
                else:
                    self.up_force = np.array([0, 0, 0])
            else:
                self.up_force = np.array([0, 0, -1])

    def integrate(self, deltaT):
        force = self.force1 + self.force2 + self.force_visc + \
            self.force_wind * 0.0001 + self.up_force
        self.acceleration = force / self.mass
        self.speed = self.speed + self.acceleration * deltaT
        self.position = self.position + self.speed * deltaT
        self.velocity = np.linalg.norm(self.speed)
        self.norm_acc = np.linalg.norm(self.acceleration)


def optim_corde():
    LONGI = [50.07472229003906, 11.226388931274414, 20000]
    MANGO = [51.692779541015625, 0.7905555367469788, 20000]
    EELDE = [53.21333312988281, 7.216944217681885, 20000]
    ROBTU = [48.823890686035156, 22.375, 20000]
    dep = MANGO
    arr = ROBTU
    file = open("../trajectory/trajetinteressants.csv", "rb")
    reader = csv.reader(file)
    rows = []
    for row in reader:
        rows.append(row)
    interp = interpol_from_npz('../npz/u.npz', '../npz/v.npz', '../npz/lats.npz',
                               '../npz/lons.npz', '../npz/altitude.npz', '../npz/time.npz')
    print 'inter loaded'
    c = []

    b = buildAlt(dep, arr, 1)
    start_point = np.array([dep[1], dep[0], dep[2] / 10000])
    end_point = np.array([arr[1], arr[0], arr[2] / 10000])
    print end_point
    corde = fstring(float(1), float(1), 20, 0.8 * np.linalg.norm(start_point - end_point),
                    start_point, end_point,
                    interp, b)
    corde.initialize(1)
    for i in range(1, 3000):
        if not(corde.update()):

            break
        if i % 30 == 0:
            corde.plot()
            print i
    corde.to_csv()
    c.append(evaluate2())
    print c

    # for i in range(0, len(rows), 2):
    #     row = rows[i]
    #     nextrow = rows[i + 1]
    #     # interp = interpol_from_npz('../npz/u_Par_Toul.npz', '../npz/v_Par_Toul.npz', '../npz/lats_Par_Toul.npz',
    #     #                            '../npz/lons_Par_Toul.npz', '../npz/altitude_Par_Toul.npz', '../npz/time_Par_Toul.npz')
    #     b = buildAlt([float(row[1]), float(row[2]), float(row[3])], [
    #                  float(nextrow[1]), float(nextrow[2]), float(nextrow[3])], 0.9)
    #     start_point = np.array(
    #         [float(row[2]), float(row[1]), float(row[3]) / 10000])
    #     end_point = np.array(
    #         [float(nextrow[2]), float(nextrow[1]), float(nextrow[3]) / 10000])
    #     print end_point
    #     corde = fstring(float(1), float(1), 30, 0.87 * np.linalg.norm(start_point - end_point),
    #                     start_point, end_point,
    #                     interp, b)
    #     corde.initialize(1)
    #     for i in range(1, 3000):
    #         if not(corde.update()):

    #             break
    #         if i % 30 == 0:
    #             corde.plot()
    #             print i
    #     corde.to_csv()
    #     c.append(evaluate2())
    #     print c

    # print "second opt"
    # start_point = np.array([53., 18., 25000])
    # end_point = np.array([42., 1., 25000])
    # interp = interpol_from_npz('../npz/u_bigger.npz', '../npz/v_bigger.npz', '../npz/lats_bigger.npz', '../npz/lons_bigger.npz', '../npz/altitude_bigger.npz', '../npz/time_bigger.npz')
    # c = []
    # alt = 25000
    # interp_p = interp_pot(interp, end_point)
    # corde = fstring(float(1), float(1), 30, 0.87*np.linalg.norm(start_point-end_point),
    #                 start_point, end_point,
    #                 interp_p)
    # corde.initialize(1)
    # for i in range(1, 1000000):
    #     if not(corde.update()):
    #         corde.to_csv()
    #         break
    #     if i % 30 == 0:
    #         corde.plot()
    #         print i
    # c.append(evaluate2())
    # print c

optim_corde()
=======
import numpy as np
import matplotlib.pyplot as plt
from interpolator import *
from execsim import *
from scipy import interpolate
import csv
from buildAlt import *


def alt_vit(alt):
    return 2.669e2 + alt * 4.985e-3


class fstring:

    def __init__(
            self, stiffness, m, number, lenght, pos_start, pos_end, interp, buildalt):
        self.listMass = []
        freeLength = lenght / number
        self.listMass.append(mass(pos_start, np.array([0, 0]),
                                  m, freeLength, stiffness))
        self.listMass[0].string = self
        self.interp = interp
        self.plotting = False
        self.velocity_stop = 0.02
        self.acc_stop = 0.02
        self.pos_start = pos_start
        self.pos_end = pos_end
        self.buildalt = buildalt
        for i in range(1, number):
            self.listMass.append(mass(pos_start + (pos_end - pos_start)
                                      * i / float(number - 1),
                                      np.array([0, 0, 0]), m,
                                      freeLength, stiffness))
            self.listMass[i].mass1 = self.listMass[i - 1]
            self.listMass[i - 1].mass2 = self.listMass[i]
            self.listMass[i].fstring = self

    def update(self):

        for i in range(1, len(self.listMass) - 1):
            mass = self.listMass[i]
            mass.calculate_force()
        for i in range(1, len(self.listMass) - 1):
            mass = self.listMass[i]
            mass.integrate(float(0.1))
        for i in range(1, len(self.listMass) - 1):
            pt1 = {'lat': self.listMass[i].position[
                1], 'lon': self.listMass[i].position[0]}
            pt2 = {'lat': self.listMass[i + 1].position[1],
                   'lon': self.listMass[i + 1].position[0]}
            dist = self.dist(pt1, pt2)
            self.listMass[i + 1].cumultime = self.listMass[i +
                                                           1].cumultime + dist / self.listMass[i + 1].vit

        cont = False
        velocity_null = True
        for i in range(1, len(self.listMass) - 1):
            if self.listMass[i].velocity > self.velocity_stop:
                velocity_null = False
        if velocity_null:
            for i in range(1, len(self.listMass) - 1):
                if self.listMass[i].norm_acc > self.acc_stop:
                    cont = True
        else:
            cont = True
        return cont

    def dist(self, pt1, pt2):
        phia = np.deg2rad(pt1['lat'])
        phib = np.deg2rad(pt2['lat'])
        dl = np.deg2rad(pt2['lon'] - pt1['lon'])
        rt = 6378
        return np.arccos(np.sin(phia) * np.sin(phib)
                         + np.cos(phia) * np.cos(phib) * np.cos(dl)) * rt / 1.852

    def initialize(self, type_init):
        pos_end = self.listMass[-1].position
        pos_start = self.listMass[0].position
        direc = pos_end - pos_start
        number = len(self.listMass)
        if type_init == 1:
            for i in range(1, number - 1):
                cos_comp = 3 * np.cos(3.14 * float(i) / float(number - 1))
                self.listMass[i].position = pos_start + np.array(
                    [direc[0] * float(i) / float(number - 1),
                     direc[1] * float(i) / float(number - 1)
                     + cos_comp, pos_end[2]])
            # for i in range(1, number - 1):
            #     cos_comp = np.cos(3.14 * float(i) / float(number - 1))
            #     self.listMass[i].speed = np.array(
            #         [direc[0] * float(i) / float(number - 1),
            #          direc[1] * float(i) / float(number - 1)
            #          + cos_comp])

    def plot(self):
        # if not(self.plotting):
        #     self.plotting = True
        #     XX, YY = np.meshgrid(self.interp.Long, self.interp.Lat)
        #     Ut = np.zeros(
        #         (len(self.interp.Lat), len(self.interp.Long)))
        #     Vt = np.zeros(
        #         (len(self.interp.Lat), len(self.interp.Long)))
        #     for i in range(0, len(self.interp.Lat)):
        #         for j in range(0, len(self.interp.Long)):
        #             r = self.interp.inter(self.interp.Long[j], self.interp.Lat[
        #                                   i], self.listMass[0].position[2], 0)
        #             Ut[i, j] = r[0]
        #             Vt[i, j] = r[1]
        #     plt.figure(2)
        #     plt.quiver(XX, YY, Vt, Ut)
        #     CS = plt.contour(XX, YY, Ut)
        #     plt.axis([self.interp.Long[0], self.interp.Long[
        #              -1], self.interp.Lat[-1], self.interp.Lat[0]])

        #     plt.clabel(CS, inline=2, fontsize=10)
        #     plt.colorbar(CS, shrink=0.8, extend='both')

        #     plt.draw()

        x = []
        y = []
        z = []
        d = []
        z_tar = []
        for mass in self.listMass:
            x.append(mass.position[0])
            y.append(mass.position[1])
            z.append(mass.position[2] * 10000)
            z_tar.append(self.buildalt.alt_vit(
                mass.position[1], mass.position[0])[0])
            d.append(self.dist({'lat': mass.position[1], 'lon': mass.position[0]}, {
                     'lat': self.pos_start[1], 'lon': self.pos_start[0]}))
        plt.figure(2)
        plt.plot(x, y)
        plt.scatter(x, y)
        plt.axis([self.interp.Long[0], self.interp.Long[
                 -1], self.interp.Lat[-1], self.interp.Lat[0]])
        plt.pause(0.01)
        plt.draw()
        plt.clf()

        plt.figure(5)
        plt.plot(d, z)
        plt.scatter(d, z)
        plt.scatter(d, z_tar, c='r')
        plt.draw()
        plt.clf()

    def to_csv(self):
        csv_file = open("../trajectory/contracts_corde.csv", "wb")
        csv_file.write(
            "FlightId, LatitudeDegrees, LongitudeDegrees, AltitudeFeet, AirspeedKnots")
        for m in self.listMass:
            csv_file.write(
                "\n301817127," + str(m.position[1]) + "," + str(m.position[0]) + "," + str(
                    m.position[2] * 10000) + "," + str(alt_vit(m.position[2] * 10000)))
        csv_file.close()


class mass:

    def __init__(self, initPosition, initSpeed, m, freeLength, stiffness):
        self.mass = m
        self.position = initPosition
        self.speed = initSpeed
        self.velocity = np.linalg.norm(self.speed)
        self.mass1 = []
        self.mass2 = []
        self.force1 = np.array([0, 0, 0])
        self.force2 = np.array([0, 0, 0])
        self.force_visc = np.array([0, 0, 0])
        self.force_att = np.array([0, 0, 0])
        self.force_wind = np.array([0, 0, 0])
        self.force_oth = np.array([0, 0, 0])
        self.kVisc = 0.5
        self.cumultime = 0
        self.vit = 0
        self.freeLength = freeLength
        self.stiffness = stiffness
        self.acceleration = np.array([0, 0, 0])
        self.fstring = []
        self.up_force = np.array([0, 0, 0])
        self.norm_acc = np.linalg.norm(self.acceleration)

    def ps_wind_head(self, pos, t):
        r = self.fstring.interp.inter(pos[1], pos[0], pos[2] * 10000, t)
        heading = self.fstring.pos_end[0:2] - pos[0:2]
        heading = heading / (np.linalg.norm(heading)) ** (1 / 4)
        ps = heading[0] * r[1] + heading[1] * r[0]
        return ps

    def calculate_force(self):
        # Force of the strings
        k = self.stiffness
        p = self.position
        p1 = self.mass1.position
        p2 = self.mass2.position
        l = self.freeLength
        pn = p2 - p
        pp = p - p1

        deltax1 = np.linalg.norm(pp) - l
        deltax2 = np.linalg.norm(pn) - l

        norm_force1 = - k * deltax1
        norm_force2 = k * deltax2

        self.force1 = norm_force1 * pp / np.linalg.norm(pp)
        self.force2 = norm_force2 * pn / np.linalg.norm(pn)

        # Viscous force
        self.force_visc = - self.kVisc * self.speed

        # Wind force
        dx = 0.1
        x_n = self.ps_wind_head(
            self.position + [dx, 0, 0], np.floor(self.cumultime))
        x_p = self.ps_wind_head(
            self.position + [-dx, 0, 0], np.floor(self.cumultime))
        y_n = self.ps_wind_head(
            self.position + [0, dx, 0], np.floor(self.cumultime))
        y_p = self.ps_wind_head(
            self.position + [0, -dx, 0], np.floor(self.cumultime))
        self.force_wind = -1.2 * np.array(
            [(x_p - x_n) / (2 * dx), (y_p - y_n) / (2 * dx), 0])

        # Upward force

        targetalt, self.vit = self.fstring.buildalt.alt_vit(
            self.position[1], self.position[0])

        if self.position[2] * 10000 < targetalt:
            if self.position[2] * 10000 >= targetalt - 1000:
                if self.position[2] * 10000 <= targetalt - 100:
                    self.up_force = (
                        targetalt - self.position[2] * 10000) / 1000 * np.array([0, 0, 1])
                else:
                    self.up_force = np.array([0, 0, 0])
            else:
                self.up_force = np.array([0, 0, 1])

        if self.position[2] * 10000 > targetalt:
            if self.position[2] * 10000 <= targetalt + 1000:
                if self.position[2] * 10000 >= targetalt + 100:
                    self.up_force = (
                        targetalt - self.position[2] * 10000) / 1000 * np.array([0, 0, 1])
                else:
                    self.up_force = np.array([0, 0, 0])
            else:
                self.up_force = np.array([0, 0, -1])

    def integrate(self, deltaT):
        force = self.force1 + self.force2 + self.force_visc + \
            self.force_wind * 0.0001 + self.up_force
        self.acceleration = force / self.mass
        self.speed = self.speed + self.acceleration * deltaT
        self.position = self.position + self.speed * deltaT
        self.velocity = np.linalg.norm(self.speed)
        self.norm_acc = np.linalg.norm(self.acceleration)


def optim_corde():
    LONGI = [50.07472229003906, 11.226388931274414, 20000]
    MANGO = [51.692779541015625, 0.7905555367469788, 20000]
    EELDE = [53.21333312988281, 7.216944217681885, 20000]
    ROBTU = [48.823890686035156, 22.375, 20000]
    dep = MANGO
    arr = ROBTU
    file = open("../trajectory/trajetinteressants.csv", "rb")
    reader = csv.reader(file)
    rows = []
    for row in reader:
        rows.append(row)
    interp = interpol_from_npz('../npz/u.npz', '../npz/v.npz', '../npz/lats.npz',
                               '../npz/lons.npz', '../npz/altitude.npz', '../npz/time.npz')
    print 'inter loaded'
    c = []

    b = buildAlt(dep, arr, 0.1)
    start_point = np.array([dep[1], dep[0], dep[2] / 10000])
    end_point = np.array([arr[1], arr[0], arr[2] / 10000])
    print end_point
    corde = fstring(float(1), float(1), 20, 0.87 * np.linalg.norm(start_point - end_point),
                    start_point, end_point,
                    interp, b)
    corde.initialize(1)
    for i in range(1, 3000):
        if not(corde.update()):

            break
        if i % 30 == 0:
            corde.plot()
            print i
    corde.to_csv()
    c.append(evaluate2())
    print c

    # for i in range(0, len(rows), 2):
    #     row = rows[i]
    #     nextrow = rows[i + 1]
    #     # interp = interpol_from_npz('../npz/u_Par_Toul.npz', '../npz/v_Par_Toul.npz', '../npz/lats_Par_Toul.npz',
    #     #                            '../npz/lons_Par_Toul.npz', '../npz/altitude_Par_Toul.npz', '../npz/time_Par_Toul.npz')
    #     b = buildAlt([float(row[1]), float(row[2]), float(row[3])], [
    #                  float(nextrow[1]), float(nextrow[2]), float(nextrow[3])], 0.9)
    #     start_point = np.array(
    #         [float(row[2]), float(row[1]), float(row[3]) / 10000])
    #     end_point = np.array(
    #         [float(nextrow[2]), float(nextrow[1]), float(nextrow[3]) / 10000])
    #     print end_point
    #     corde = fstring(float(1), float(1), 30, 0.87 * np.linalg.norm(start_point - end_point),
    #                     start_point, end_point,
    #                     interp, b)
    #     corde.initialize(1)
    #     for i in range(1, 3000):
    #         if not(corde.update()):

    #             break
    #         if i % 30 == 0:
    #             corde.plot()
    #             print i
    #     corde.to_csv()
    #     c.append(evaluate2())
    #     print c

    # print "second opt"
    # start_point = np.array([53., 18., 25000])
    # end_point = np.array([42., 1., 25000])
    # interp = interpol_from_npz('../npz/u_bigger.npz', '../npz/v_bigger.npz', '../npz/lats_bigger.npz', '../npz/lons_bigger.npz', '../npz/altitude_bigger.npz', '../npz/time_bigger.npz')
    # c = []
    # alt = 25000
    # interp_p = interp_pot(interp, end_point)
    # corde = fstring(float(1), float(1), 30, 0.87*np.linalg.norm(start_point-end_point),
    #                 start_point, end_point,
    #                 interp_p)
    # corde.initialize(1)
    # for i in range(1, 1000000):
    #     if not(corde.update()):
    #         corde.to_csv()
    #         break
    #     if i % 30 == 0:
    #         corde.plot()
    #         print i
    # c.append(evaluate2())
    # print c

optim_corde()
>>>>>>> 4e125ca9e75da880cc1bbf21c84d74b94b0fccc8
