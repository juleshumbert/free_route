#Heuristique considerant l'avion comme une particule ponctuelle, soumis a un champ de potentiel
#represente par le vent d'une part et par l'arrivee d'autre part'

import orthodromie as ort
import numpy as np
import matplotlib.pyplot as plt
import interpolator as it
import readFlight as rf
import execsim as ex
import csv
import path
import operator
import random
import sys

class interpolateAlt:
    
    
    def __init__(self, filename = 'altToVit.csv'):
        try:
            cr=csv.DictReader(open(filename,"rb"))
        except:
            print("Erreur d'ouverture du fichier resultat de altToVit")
            sys.exit(0)

        i=0
        self.altitude = []
        self.vitesse = []
        for row in cr:
            self.altitude.append(float(row["Altitude"]))
            self.vitesse.append(float(row["AirSpeed"]))
    #give the best airspeed for given altitude
    def interpolate(self,alt=35000):
        i = 0
        while i<len(self.altitude) and self.altitude[i]<alt :
            i += 1
        if i!=0 and i<len(self.altitude):
            delta1 = alt - self.altitude[i-1]
            delta2 = self.altitude[i] - alt
            return (delta2*self.vitesse[i-1]+delta1*self.vitesse[i])/(delta1+delta2)
        elif i==0:
            return self.vitesse[0]
        else:
            return self.vitesse[-1]
        
        
if __name__=="__main__":
    
    inte = interpolateAlt()
    print inte.interpolate(28000)
            
            
            
            