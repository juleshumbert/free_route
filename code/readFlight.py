import numpy as np
import matplotlib.pyplot as plt
from pylab import *
import csv
import sys

class Flight:

	def __init__(self,dep=[],arr=[]):
		self.arr = arr
		self.dep = dep
		self.time = []
		self.lat = []
		self.longi = []
		self.alt = [] 
		self.flown_dst = []
		self.dst_to_next_wp = []
		self.air_spd = []
		self.mach = []
		self.grd_spd = []
		self.uwind = []
		self.vwind = []
		self.route = []
		self.mass = []
		self.fuel_burn = []
		self.sum_fuel_burn = []

	def loadFromCSV(self,fileName):
		try:
		    cr=csv.DictReader(open("../res/"+fileName,"rb"))
		except:
			print("Erreur d'ouverture du fichier resultat de la simulation")
			sys.exit(0)

		i=0
		for row in cr:
			self.time.append(row["TimeSeconds"])
			self.lat.append(float(row["LatitudeDegrees"]))
			self.longi.append(float(row["LongitudeDegrees"]))
			self.alt.append(row["AltitudeFeet"])			
			self.flown_dst.append(row["FlownDistanceNM"])
			self.dst_to_next_wp.append(row["DistanceToNextWaypointNM"])
			self.air_spd.append(row["AirSpeedKnots"])
			self.mach.append(row["Mach"])				
			self.grd_spd.append(row["GroundSpeedKnots"])
			self.uwind.append(row["UWindKnots"])
			self.vwind.append(row["VWindKnots"])
			self.route.append(row["RouteDegrees"])
			self.mass.append(row["MassPounds"])
			self.fuel_burn.append(row["FuelBurnPounds"])
			self.sum_fuel_burn.append(row["SumFuelBurnPounds"])

			i=i+1

	def plotFlight(self,color='b'):
		axisScale=0.1
		miniLongi=min(self.longi)
		maxiLongi=max(self.longi)
		miniLat=min(self.lat)
		maxiLat=max(self.lat) 
		deltaLongi=maxiLongi-miniLongi
		deltaLat=maxiLat-miniLat

		if deltaLongi==0:
			deltaLongi=10

		if deltaLat==0:
			deltaLat=10

		plot(self.longi,self.lat,color+'o')
		axis([miniLongi-axisScale*deltaLongi,maxiLongi+axisScale*deltaLongi,miniLat-axisScale*deltaLat,maxiLat+axisScale*deltaLat])
		draw()
		
	def plotFlightOnGraph(self,graph,otherFlights=[]):
		x=[]
		y=[]

		x,y = graph.calculatePlot()
			#print str(nodeNumb)+" : "+str(graph[nodeNumb].children)
	
		axisScale=0.1
		miniLongi=min(y)
		maxiLongi=max(y)
		miniLat=min(x)
		maxiLat=max(x) 
		deltaLongi=maxiLongi-miniLongi
		deltaLat=maxiLat-miniLat

		if deltaLongi==0:
			deltaLongi=10

		if deltaLat==0:
			deltaLat=10

		plot(y,x,'bo')
		plot(self.longi,self.lat,'g^')
		
		if otherFlights <> []:
			for f in otherFlights:
				plot(f.longi,f.lat,'y^')
		
		axis([miniLongi-axisScale*deltaLongi,maxiLongi+axisScale*deltaLongi,miniLat-axisScale*deltaLat,maxiLat+axisScale*deltaLat])
		show()
		
	def clear(self):
		self.time = []
		self.lat = []
		self.longi = []
		self.alt = [] 
		self.flown_dst = []
		self.dst_to_next_wp = []
		self.air_spd = []
		self.mach = []
		self.grd_spd = []
		self.uwind = []
		self.vwind = []
		self.route = []
		self.mass = []
		self.fuel_burn = []
		self.sum_fuel_burn = []	
		
if __name__=="__main__":
	
	vol=Flight()
	#A=[33.,-12.,25000.,300.]
	#B=[33.942,-18.408,25000.,300.]
	#vol=Flight(A,B)
	vol.loadFromCSV("simresult.csv")
	vol.plotFlight()
	
	