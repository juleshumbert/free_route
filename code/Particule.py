#Heuristique considerant l'avion comme une particule ponctuelle, soumis a un champ de potentiel
#represente par le vent d'une part et par l'arrivee d'autre part'

import orthodromie as ort
import numpy as np
import matplotlib.pyplot as plt
import interpolator as it
import readFlight as rf
import execsim as ex
import csv
import path
import operator

class Particule():
    
    #Cree une particule aux coordonnees (lat,long) avec une vitesse dans le 
    #repere (u_theta, u_phi)
    #Cette particule est destinee a aller a une destination
    #elle a un cout variable
    #elle retient une partie de sa trajectoire passe, la distance qu'elle a parcouru et
    #celle qui lui reste a parcourir
    def __init__(self,origin,destination,alt,vitesse,path=[],cost=0.,ang=0.,gone=0,arrived=0):
        self.lat = origin[0]
        self.long = origin[1]
        self.latDest = destination[0]
        self.longDest = destination[1]
        self.alt = alt
        self.vitesse = vitesse
        self.cost = cost
        self.path = path
        if path==[]:
            self.path = [[self.lat,self.long,self.alt,self.vitesse]]
        self.calc_distance()
        self.ang = ang
        self.gone = gone
        self.arrived = arrived
        
    def calc_distance(self):
        self.distDone=0
        A=self.path[0]
        for B in self.path:
            self.distDone = self.distDone + ort.distanceApprox([A[0],A[1]],[B[0],B[1]])
            A=B
        self.distRest = ort.distanceApprox([A[0],A[1]],[self.latDest,self.longDest])    
        self.distTot = self.distDone + self.distRest 
             
    def maj_distance(self,point):
        self.path.append(point)
        last=self.path[-2]
        self.distDone = self.distDone + ort.distanceOrtho([last[0],last[1]],[point[0],point[1]])
        self.distRest = ort.distanceOrtho([point[0],point[1]] , [self.latDest,self.longDest])    
        self.distTot = self.distDone + self.distRest     
        
           
        
class Simulation:
    
    def __init__(self,dep,arr,pas=50):   
        self.dep = dep
        self.arr = arr     
        self.pas = pas
        self.particules={}
        self.nbparticules=0
        self.nbparticulesArrived=0
        
    #Donne le temps initial du trajet et le temps
    #dt en heure     
    def set_dt(self,tinit,dt):
        self.dt = dt    
        self.tinit = tinit
        
    #charge la carte des vents correspondants a des fichiers npz    
    def load_map(self,u,v,lats,lons,alt,time):
        inte = it.interpol_from_npz(u,v,lats,lons,alt,time)
        self.map = inte
        
    #lance un lancement de particule direction orthodromie  + ang 
    #rappel approximation de l'avance d'une particule avec un cap donne : 
    #lat ar = lat dep + d*cos cap
    #long ar =long dep + ((d*sin cap)/cos lat moy)
    #Cap 0 : nord
    #cap 90 : est
    #cap 180 : sud
    #la formule doit s'adapter a tous les cas je pense il y aura des problemes
    #A chaque iteration on prend en compte la direction du vent interpole
    #On introduit de nouvelles particules sur le chemin toutes les 10 iteration par exemple
    #alpha : importance de la direction du vent dans le choix du cap a chaque iteration
    #path : parcours anterieur eventuel de la particule, 
    #ang: decallage de cap par rapport a l'orthodromie
    #cost : cout estime initial de la particule
    def launchParticule(self,alt=250.,vitesse=400.,alpha=0.03,dep=[],arr=[],path=[],ang=0,cost=0.):
        if(dep==[]):
            dep=self.dep
        if(arr==[]):
            arr=self.arr    
        part = Particule(dep,arr,alt*100,vitesse,path,cost,ang)
        self.particules[self.nbparticules] = part
        self.nbparticules = self.nbparticules+1
        fin = 0
        i = 0
        t=[0]
        wind=[0,0]
        while(fin == 0):
            try:
                wind = self.map.inter(part.lat,part.long,alt,t[i])
            except:
                print('Sorti..')
            angl=np.arctan2(wind[1],wind[0])
            d= ort.distanceOrthoRad([part.lat,part.long],arr)
            cap0 = (1-alpha)*np.arccos((np.sin(np.pi/180*arr[0])-np.sin(np.pi/180*part.lat)*np.cos(d))/( \
                         np.cos(np.pi/180*part.lat)*np.sin(d)))+alpha*angl
            
            #garde en memoire son decalage de cap initial pendant un certain laps de temps
            if(i<10):
                cap0=cap0+part.ang
            
            lat1 = part.lat
            #mise a jour des coordonnees
            part.lat = lat1+part.vitesse*self.dt*np.cos(cap0)/60
            part.long = part.long + part.vitesse*self.dt*np.sin(cap0)/(np.cos(np.pi/360.*(lat1+part.lat)))/60
            
            
            #mise a jour cout
            part.cost = part.cost + np.dot(np.array([np.cos(cap0),np.sin(cap0)]),wind)
            part.maj_distance([part.lat,part.long,25000.,part.vitesse])
            t.append(i*self.dt*3600)
            i = i+1
            
            
            if(abs(part.lat-arr[0])+abs(part.long-arr[1])<0.5):
                fin = 1
                self.nbparticulesArrived=self.nbparticulesArrived+1
                print(self.nbparticulesArrived)
                part.arrived = 1
             
    
    #lance un nombre nb de particules a partir d'une particule
    #ces particules se decalent d'un angle aleatoire par rapport a la particule
    #dont elles sont issues
    def launchRandomParticule(self,particule,alpha=0.03,nb=1):
        for i in range(0,nb):
            self.launchParticule(particule.alt/100,particule.vitesse,alpha,[particule.lat,particule.long],[self.arr[0],self.arr[1]],
                             list(particule.path),0.1*np.random.rand(),particule.cost)   
    
    #Organise l'envoi de particule : envoie un certain nombres nInit de particules a partir de l'origine
    #chacun leur tour
    def OrganizedLaunch(self,nInit,alt=250.,vitesse=400.,alpha=0.03,dep=[],arr=[],path=[],cost=0.):        
        #envoie nInit particules avec des angles differents par rapport au cap initial orthodromique
        for i in range(0,nInit):
            r=0.8*np.random.randn()
            self.launchParticule(alt, vitesse, alpha, dep, arr, path,r , cost)
        #tri les particules selon leur cout approxime
        sortedPart=sorted(self.particules.values(), key=operator.attrgetter('distTot') )   
        return sortedPart
    
    #Cree un certain nombre de particule nInit placee a dep et ayant comme arrivee arr
    def InitializePart(self,nInit,alt=250.,vitesse=400.,alpha=0.03,dep=[],arr=[],path=[],cost=0.,tabAng=[]):
        if(dep == []):
            dep=self.dep
        if(arr == []):
            arr=self.arr
        if(tabAng == []):
            tabAng=0.8*np.random.randn(nInit)       
        for i in range(0,nInit):    
            self.particules[self.nbparticules] = Particule(dep,arr,alt*100,vitesse,path,cost,tabAng[i])
            self.nbparticules = self.nbparticules+1
    
    #Avance les particules indicees ind_part presentes vers l'arrivee
    def Advance(self,nIter,alpha=0.03,ind_part=[],dec=10):
        if(ind_part==[]):
            ind_part = list(self.particules.keys())
            
            
        for ind in ind_part:
            
            i = 0
            fin = 0
            t = [0]
            part = self.particules[ind]
            arr =[part.latDest,part.longDest]
            if(part.arrived==1):
                fin=1
            wind=[0,0]
            while (fin==0) and (i<nIter):
                try:
                    wind = self.map.inter(part.lat,part.long,part.alt/100.,t[i])
                except:
                    print('une particule sort de la carte !')
                    
                angl = np.arctan2(wind[1],wind[0])
                d = ort.distanceOrthoRad([part.lat,part.long],arr)
                cap0 = (1-alpha)*np.arccos((np.sin(np.pi/180*arr[0])-np.sin(np.pi/180*part.lat)*np.cos(d))/( \
                             np.cos(np.pi/180*part.lat)*np.sin(d)))+alpha*angl
                
                #garde en memoire son decalage de cap initial pendant un certain laps de temps
                if(i<dec) and (part.gone==0):
                    cap0 = cap0+part.ang
                
                lat1 = part.lat
            
                #mise a jour des coordonnees
                part.lat = lat1+part.vitesse*self.dt*np.cos(cap0)/60
                part.long = part.long + part.vitesse*self.dt*np.sin(cap0)/(np.cos(np.pi/360.*(lat1+part.lat)))/60
                
                
                #mise a jour cout
                part.cost = part.cost +np.dot(np.array([np.cos(cap0),np.sin(cap0)]),wind)
                part.maj_distance([part.lat,part.long,25000.,part.vitesse])
                t.append(i*self.dt*3600.)
                i = i+1
                
                if(abs(part.lat-arr[0])+abs(part.long-arr[1])<0.5):
                    fin = 1
                    self.nbparticulesArrived = self.nbparticulesArrived+1
                    part.arrived=1
            
            part.gone=1
            
            
    
    #Combine les fonctions initialize et advance dans un seul algorithme        
    def heuristique(self,nIter,nInit,alt=250.,vitesse=400.,alpha=0.03,dep=[],arr=[],path=[],cost=0.,tabAng=[]):
        self.InitializePart(nInit, alt, vitesse, alpha, dep, arr, path, cost, tabAng)
        deca=10
        for n in range(0,4): 
            print 'Iteration : ',n
            print 'Nombre de particules ',len(self.particules)   
            print 'nombre particules arrivees', self.nbparticulesArrived
            self.Advance(nIter,alpha,dec=deca)
            if (n<15):
                self.kill(0.5)
            sortedPart=sorted(self.particules.values(), key=operator.attrgetter('distTot')) 
            l=len(sortedPart)
            l1=int(0.5*len(sortedPart))
            for i in range(0,l1):
                m=sortedPart[i]
                if(m.arrived==0):
                    self.InsertParticule(m,alpha,2)
                
        sortedPart=sorted(self.particules.values(), key=operator.attrgetter('distTot') )   
        return sortedPart       
        
        #tue une proportion de particules
    def kill(self,proportion):
        sortedKey=sorted(self.particules.keys(), key = lambda num: self.particules[num].distTot,reverse=True)
           
        for i in range(0,int(proportion*len(sortedKey))):
            if sortedKey[i] in self.particules:
                 self.particules.pop(sortedKey[i],None)
                
    
    #selectionne les particules qui sont proches de l'optimal distance dans le bon sens du vent a priori
    def  selectBest(self,seuil):
        filt=dict((k, v) for (k, v) in self.particules.iteritems() if v.distTot<seuil)
        sortfilt=sorted(filt.values(), key=operator.attrgetter('cost'))
        return sortfilt
        
        
        
        
        
    def InsertParticule(self,particule,alpha=0.03,nb=1,tabAng=[]):
        self.InitializePart(nb,particule.alt/100.,particule.vitesse,alpha,[particule.lat,particule.long],[self.arr[0],self.arr[1]],
                             list(particule.path),particule.cost,tabAng) 
    
    
    
def saveTrajectory(list,cost):
    c = csv.writer(open("../res/resultatParticule.csv","wb"))
    c.writerow(["Etape","Latitude","Longitude","Altitude","Vitesse"])

    i=0
    for e in list:
        i=i+1
        c.writerow([i,e[0],e[1],e[2],e[3]])

    c.writerow("Cout")
    c.writerow([cost])
    




if __name__=="__main__":
    A=[33.,-13.] 
    B=[33.942,-18.408] 
    
    A=[53.,18.]
    B=[42.,1.]
    sim = Simulation(B,A)
    
    #sim.load_map('u_small.npz', 'v_small.npz', 'lats_small.npz','lons_small.npz','altitude_small.npz','time_small.npz')
    #sim.load_map('u_default.npz','v_default.npz','lats_default.npz','lons_default.npz','altitude_default.npz','time_default.npz')
    #sim.load_map('u.npz', 'v.npz', 'lats.npz','lons.npz','altitude.npz','time.npz')
    #Ranges Bigger : lat [55,40],logit[0,20],alt[200,1000],time[0,72000]
    sim.load_map('u_bigger.npz', 'v_bigger.npz', 'lats_bigger.npz','lons_bigger.npz','altitude_bigger.npz','time_bigger.npz')
    #dt=0.01 (petit trajet)
    dt=0.05 #grand trajet
    sim.set_dt(0.,dt)
    print sim.map.rangeLat,sim.map.rangeLong
    #tab=sim.OrganizedLaunch(1000,alpha=0.03)
    tab=sim.heuristique(12,1000)
    
    tab=sim.selectBest(950)
    for part in tab:
        print(part.ang)
        print(part.distTot)
    cout=ex.evaluate([[B[0],B[1],25000.,400.],[A[0],A[1],25000.,400.]])
    vol=rf.Flight()
    vol.loadFromCSV("simresult.csv")
   
    print len(sim.particules), ' Particules dans le domaine'
    coutmin=cout
    idmin='Avion'
    listmin=[[B[0],B[1],25000.,400.],[A[0],A[1],25000.,400.]]
    coutP=[]
    costP=[]
    costWind=[]
    angle=[]
    #Itere sur toutes les particules
    #for part in sim.particules :
    #itere sur les 10 meilleures particules a priori
    #for part in tab:
    for part in tab[0:10]:
        liste=part.path
        liste[-1]=[A[0],A[1],25000.,400.]
        cout2=ex.evaluate(liste)
        coutP.append(cout2)
        costP.append(part.distTot)
        costWind.append(part.cost)
        angle.append(part.ang)
        print cout2
        if(cout2<=coutmin):
            coutmin=cout2
            idmin=part
            listmin=list(liste)
    
    coutmin=ex.evaluate(listmin)
    volOpt=rf.Flight()
    volOpt.loadFromCSV("simresult.csv")
    
    print 'cout trajet dir : ',cout
    print 'cout via heuristique : ',coutmin    
    print 'dist dir',vol.flown_dst[-1]
    print 'dist bye',volOpt.flown_dst[-1]
    plt.plot(coutP,costP)
    plt.show()
    plt.plot(angle,coutP)
    plt.show()
    
    print(coutP)
    print(costWind)
    print(costP)
    taux=[float(x)/max(float(y),1) for x,y in zip(vol.sum_fuel_burn,vol.flown_dst)]
    taux2=[float(x)/max(float(y),1) for x,y in zip(volOpt.sum_fuel_burn,volOpt.flown_dst)]
    
    fig=plt.figure()
    plt.plot(vol.time[1:-2],taux[1:-2],'r')
    plt.plot(volOpt.time[1:-2],taux2[1:-2],'b')     
    plt.title('comparaison taux de carburant : rouge trajet direct, bleu trajet heuristique')
    plt.show()
    plt.figure()
    vol.plotFlight()
    volOpt.plotFlight()
    print vol.time[-1],volOpt.time[-1]
    
    saveTrajectory(listmin, coutmin)