import execsim as ex
import readFlight as rf
import numpy as np
import os
import csv
import matplotlib.pyplot as plt
from writeContracts import *
from readResult import *
from _ctypes import Array




class buildAltToVit:

    '''
    class used to approximate optimal airspeed to a given altitude,
    '''
    

    def __init__(self, altMin = 10000. , altMax = 35000. , resolution = 50., \
                 vitMin = 280., vitMax = 460.,resolutionVit = 100. \
                 ,filename = 'altToVit.csv', \
                 dep = [0.,0.], arr = [0.,0.8]):
        self.rangeAlt = np.linspace(altMin, altMax, resolution)
        self.rangeVit = np.linspace(vitMin,vitMax, resolutionVit)
        self.nAlt = np.size(self.rangeAlt)
        self.nVit = np.size(self.rangeVit)
        self.filename = filename
        self.v = rf.Flight()
        self.result = np.zeros([self.nAlt])
        self.dep = dep
        self.arr = arr 
        self.bigResult = np.zeros([self.nAlt,self.nVit])
    
    def buildCsvPov(self):
        for i in range(0,self.nAlt):
            cmin = float("inf")
            alt = self.rangeAlt[i]
            for j in range(0,self.nVit):
                vt = self.rangeVit[j] 
                c = ex.evaluate(trajet = [[self.dep[0],self.dep[1],alt,vt],[self.arr[0],self.arr[1],alt,vt]])
                self.bigResult[i,j] = c
                if (c<cmin):
                    cmin = c
                    self.result[i] = vt
             
        self.writeResult()
    
    
    
    
         
    def buildCsv(self):
        conso = np.zeros((len(self.rangeAlt),len(self.rangeVit)))
        for k in range(2, 3):
            for i in range(0,self.nAlt):
                cmin = float("inf")
                alt = self.rangeAlt[i]
                if alt+k*1000 < self.rangeAlt[-1]:
                    altn = alt + k*1000
                else:
                    altn = self.rangeAlt[i]
                for j in range(0,self.nVit):
                    vt = self.rangeVit[j] 
                    tr=[self.dep+[alt,vt],list(np.array(self.dep)*0.75+np.array(self.arr)*0.25) + [altn, vt], self.arr+[alt,vt]]
                    self.evaluatePerso(tr)
                    c = (float(self.v.sum_fuel_burn[-1]))
                    self.bigResult[i,j] = c
                    if (c<cmin):
                        cmin = c
                        self.result[i] = vt
                    conso[i, j] = c
                    trd=[self.dep+[alt,vt], self.arr+[alt,vt]]
                    self.evaluatePerso(trd)
                    c = (float(self.v.sum_fuel_burn[-1]))
                    conso[i,j] = conso[i, j] - c
                    print(i, j)
            xx, yy = np.meshgrid(self.rangeVit, self.rangeAlt)
            plt.figure(k)
            CS = plt.contourf(yy, xx, conso, )
            cbar = plt.colorbar(CS)
            plt.draw()
        self.writeResult()
                    
    def writeResult(self):
        with open(self.filename, 'w') as csvfile:
            fieldnames = ['Altitude', 'AirSpeed']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for i in range(0,self.nAlt):
                writer.writerow({'Altitude': self.rangeAlt[i], 'AirSpeed': self.result[i]})
              
     
    def evaluatePerso(self,trajet=[[43.6,1.43,30000,350],[48.51,2.2,30000,350]],\
                      resultName="simresult.csv", contractName="simcontract.csv"):
        generate("simcontract.csv",trajet)
        if os.sys.platform == 'linux2':
            os.system('./../bin/grib.usr/bin/sim ../trajectory/' + contractName +
                      ' -out ../res/' + resultName)
    
        else:
            os.chdir('../bin')
            os.system('sim ../trajectory/' + contractName +
                          ' -out ../res/' + resultName)
            os.chdir('../code')
        self.v.clear()
        self.v.loadFromCSV(resultName)
    


if __name__=="__main__":
    build=buildAltToVit(resolution=10,resolutionVit=10)
    

    build.buildCsvPov()
    build.writeResult() 
