#Heuristique considerant l'avion comme une particule ponctuelle, soumis a un champ de potentiel
#represente par le vent d'une part et par l'arrivee d'autre part'

import orthodromie as ort
import numpy as np
import matplotlib.pyplot as plt
import interpolator as it
import readFlight as rf
import execsim as ex
import csv
import path
import operator
import random
from matplotlib.path import Path

class Particule():

    #Cree une particule aux coordonnees (lat,long) avec une vitesse dans le
    #repere (u_theta, u_phi)
    #Cette particule est destinee a aller a une destination
    #elle a un cout variable
    #elle retient une partie de sa trajectoire passe, la distance qu'elle a parcouru et
    #celle qui lui reste a parcourir
    def __init__(self,origin,destination,alt,vitesse,path=[],cost=0.,ang=0.,gone=0,arrived=0,tabang=[],intr=0,alpha=0.03,tabvg=[]):
        self.lat = origin[0]
        self.long = origin[1]
        self.latDest = destination[0]
        self.longDest = destination[1]
        self.alt = alt
        self.vitesse = vitesse
        self.cost = cost
        self.path = path
        if path==[]:
            self.path = [[self.lat,self.long,self.alt,self.vitesse]]
        self.calc_distance()
        self.ang = ang
        self.gone = gone
        self.arrived = arrived
        self.tabang = tabang
        if tabang==[]:
            self.tabang=[(ang,0)]
        self.tabvg=tabvg
        self.iter = len(self.path)
        self.intr=intr
        self.alpha=alpha
        self.estimcost=self.distDone+alpha*self.cost
        self.endtime = 0.0
        self.vitmoy = 0.0
    
    def calc_distance(self):
        self.distDone=0
        A=self.path[0]
        for B in self.path:
            self.distDone = self.distDone + ort.distanceOrtho([A[0],A[1]],[B[0],B[1]])
            A=B
        self.distRest = ort.distanceOrtho([A[0],A[1]],[self.latDest,self.longDest])
        self.distTot = self.distDone + self.distRest

    def maj_distance(self,point,shift=0.,vg=0.0):
        self.path.append(point)
        last=self.path[-2]
        self.distDone = self.distDone + ort.distanceOrtho([last[0],last[1]],[point[0],point[1]])
        self.distRest = ort.distanceOrtho([point[0],point[1]] , [self.latDest,self.longDest])
        self.distTot = self.distDone + self.distRest
        self.tabang.append((shift,self.iter))
        self.iter = self.iter+1
        self.estimcost=self.distDone+self.alpha*self.cost
        self.tabvg.append(vg)
        self.vitmoy=sum(self.tabvg, 0.0) / len(self.tabvg)

class Simulation:

    def __init__(self,dep,arr,pas=50,tinit=0):
        self.dep = dep
        self.arr = arr
        self.pas = pas
        self.particules={}
        self.particulesArrived={}
        self.nbparticules=0
        self.nbparticulesArrived=0
        self.sense=1
        if(arr[1]<dep[1]):
            self.sense=-1
        self.tinit=tinit    

    #Donne le temps initial du trajet et le temps
    #dt en heure
    def set_dt(self,tinit,dt):
        self.dt = dt
        self.tinit = tinit

    #charge la carte des vents correspondants a des fichiers npz
    def load_map(self,u,v,lats,lons,alt,time):
        inte = it.interpol_from_npz(u,v,lats,lons,alt,time)
        self.map = inte
    
    
    #Estime une solution approchee a l'optimisation
    def firstSolution(self,alt=25000.,vitesse=400.,dep=[],arr=[]):
        if(dep == []):
            dep=self.dep
        if(arr == []):
            arr=self.arr
        self.cout = ex.evaluate(trajet=[[dep[0],dep[1],alt,vitesse],[arr[0],arr[1],alt,vitesse]],initTime=self.tinit)
        print 'Cout du modele avion : ', self.cout
        vol = rf.Flight()
        vol.loadFromCSV("simresult.csv")
        self.vol=vol


    #Estime des parametres adaptes a l'optimisation
    #nStep est le nombre de point parcouru souhaite entre depart et arrivee :
    # va donner un parametre dt adapte
    def setSomeParam(self,nStep=100):
        dtSec=(float(self.vol.time[-1])-float(self.vol.time[0]))/nStep
        dtH=dtSec/3600
        self.dt=dtH
        self.nStep = nStep
        modVitesse=[np.sqrt(float(x)**2+float(y)**2) for x,y in zip(self.vol.uwind,self.vol.vwind)]
        #distance parcourue via le modele avion
        self.dist = float(self.vol.flown_dst[-1])
        self.time = float(self.vol.time[-1])
    
        
    #Cree un certain nombre de particule nInit placee a dep et ayant comme arrivee arr
    def InitializePart(self,nInit,alt=25000.,vitesse=400.,alpha=0.03,dep=[],arr=[],\
                       path=[],cost=0.,Ang=[],tabang=[],intr=0,tabvg=[],method=np.random.randn):
        if(dep == []):
            dep=self.dep
        if(arr == []):
            arr=self.arr
        if(Ang == []):
            Ang=0.2*method(nInit)
            #Ang=np.linspace(0,0.2,nInit)
        
        for i in range(0,nInit):
            self.particules[self.nbparticules] = Particule(dep,arr,alt,vitesse,list(path),cost,Ang[i],\
                                                           gone=0,arrived=0,tabang=list(tabang),intr=intr,tabvg=list(tabvg),\
                                                           alpha=alpha)
            self.nbparticules = self.nbparticules+1

    #Avance les particules indicees ind_part presentes vers l'arrivee
    def Advance(self,nIter,alpha=0.03,ind_part=[],dec=10,tdeb=0.0):
        if(ind_part==[]):
            ind_part = list(self.particules.keys())

        for ind in ind_part:
            i = 0
            fin = 0
            t = [tdeb]
            part = self.particules[ind]
            arr =[part.latDest,part.longDest]
            if(part.arrived==1):
                fin=1
            if(ort.distanceOrthoRad([part.lat,part.long],arr)==0):
                fin=1    
            wind=[0,0]
            alpha = part.alpha
            while (fin==0) and (i<nIter):
                try:
                    wind = self.map.inter(part.lat,part.long,part.alt,t[i])
                except:
                    print part.ang
                    #print part.lat,part.long,part.alt,t[i]
                    print('une particule sort de la carte !')
                    print part.long
                    print part.lat
                    print ort.distanceOrtho([part.lat,part.long],arr)
                    self.particules.pop(ind)
                    break
                angl = np.arctan2(wind[1],wind[0])
                d = ort.distanceOrthoRad([part.lat,part.long],arr)
                s=self.sense
                if part.long>self.arr[1]:
                    s=-1
                    
                cap0 = s * (1-alpha) * np.arccos((np.sin(np.pi/180*arr[0])-np.sin(np.pi/180*part.lat)*np.cos(d)) / ( \
                             np.cos(np.pi/180*part.lat)*np.sin(d))) + alpha * angl
                                
                shift = 0.
                
                #garde en memoire son decalage de cap initial pendant un certain laps de temps
                if(i<dec) and (part.gone==0):
                    shift = (part.ang)/2

                cap0 = cap0+shift
                lat1 = part.lat
                long1 = part.long
                #calcul de la vitesse ground, le module de la vitesse air est 400, le cap est cap0
                w = [1.94384*vent for vent in wind] #vent en noeud marin
                vg=self.calculateGroundSpeed(part.vitesse,w,cap0)
                if(len(part.tabvg)!=0):
                    vg=int((vg+part.tabvg[-1])/2)
                    
                #mise a jour des coordonnees
                part.lat = lat1+vg*self.dt*np.cos(cap0)/60.
                part.long = part.long + vg*self.dt*np.sin(cap0)/(np.cos(np.pi/360.*(lat1+part.lat)))/60.

                d = ort.distanceOrtho([lat1,long1],arr)
                if(d<2*vg*self.dt):
                    d2=d*np.pi/(60.*180.)
                    cap0 = self.sense * (1-alpha) * np.arccos((np.sin(np.pi/180*arr[0])-np.sin(np.pi/180*lat1)*np.cos(d2)) / ( \
                             np.cos(np.pi/180*lat1)*np.sin(d2)))
                    vg=self.calculateGroundSpeed(part.vitesse,w,cap0)
                    fin = 1
                    part.lat = arr[0]
                    part.long = arr[1]
                    self.nbparticulesArrived = self.nbparticulesArrived + 1
                    part.arrived = 1
                    dtfin = d/vg
                    tfin = t[i] + (self.dt+dtfin) * 3600.   #*3600
                    part.endtime = tfin
                    self.particulesArrived[self.nbparticulesArrived]=part
                    self.particules.pop(ind,None)
                    
                #mise a jour cout
                part.cost = part.cost - np.dot(np.array([np.cos(cap0),np.sin(cap0)]),wind)
                part.maj_distance([part.lat,part.long,part.alt,part.vitesse],shift,vg)
                if fin==1:
                    part.vitmoy = (part.distDone/(part.endtime/3600.))
                
                t.append(tdeb+float(i)*self.dt*3600.)
                i = i+1

            part.gone=1
    #Avance les particules indicees ind_part presentes vers l'arrivee
    def AdvancePart(self,particule,alpha=0.03,tabAng = [],tdeb = 0.0):
        if(tabAng==[]):
            tabAng =[float(t[0]) for t in particule.tabang]
            
        l = len(tabAng)
        i = 0
        fin = 0
        t = [tdeb]
        lat = self.dep[0]
        long = self.dep[1]
        vitesse = particule.vitesse
        alt = particule.alt
        path = [[lat,long,alt,vitesse]]
        arrived = 0
        vgInit = 0
        wind=[0,0]
        arr = self.arr
        
        while (fin==0) and (i<l):
            try:
                wind = self.map.inter(lat,long,alt,t[i])
            except:
                #print part.lat,part.long,part.alt,t[i]
                print('une particule sort de la carte !')
                print long
                print lat
                
            angl = np.arctan2(wind[1],wind[0])
            d = ort.distanceOrthoRad([lat,long],arr)
            s=self.sense
            if long>self.arr[1]:
                s=-1
                    
            cap0 = s * (1-alpha) * np.arccos((np.sin(np.pi/180*arr[0])-np.sin(np.pi/180*lat)*np.cos(d)) / ( \
                             np.cos(np.pi/180*lat)*np.sin(d))) + alpha * angl
                                
            shift = tabAng[i]
            cap0 = cap0+shift
            w = [1.94384*vent for vent in wind] #vent en noeud marin
            vg=self.calculateGroundSpeed(vitesse,w,cap0)
            if(i!=0):
                vg=int((vg+vgInit)/2)
            vgInit = vg    
            lat1 = lat
            long1 =long
            #mise a jour des coordonnees
            lat = lat1+vg*self.dt*np.cos(cap0)/60.
            long = long + vg*self.dt*np.sin(cap0)/(np.cos(np.pi/360.*(lat1+lat)))/60.
            d = ort.distanceOrtho([lat1,long1],arr)
            if(d<2*vg*self.dt):
                d2=d*np.pi/(60.*180.)
                cap0 = self.sense * (1-alpha) * np.arccos((np.sin(np.pi/180*arr[0])-np.sin(np.pi/180*lat1)*np.cos(d2)) / ( \
                             np.cos(np.pi/180*lat1)*np.sin(d2)))
                fin = 1
                lat = arr[0]
                long = arr[1]
                fin = 1
                    
            t.append(tdeb+float(i)*self.dt*3600.)
            path.append([lat,long,alt,vitesse])
            i = i+1
         
        if fin==0:
            path.append([arr[0],arr[1],alt,vitesse])
         
        return path 
         
         
            
            
    def heuristiqueStep(self, nIter = 10 , nInit = 50 , alt = 25000.,\
                        vitesse = 400., alpha = 0.03, deca = 10, \
                        ordre = 'endtime', filterprop = 0.1 ,\
                        propKill = 0.1, limit = 400  ):
        
        self.InitializePart(nInit, alt, vitesse, alpha)
        t=self.tinit
        print t
        #nombre d'avancees necessaires a priori
        number = int(self.nStep / nIter)+1
        for n in range(0,number):
            indice=(n+1)*nIter
            print 'Iteration : ',n
            print 'Nombre de particules ',len(self.particules)
            print 'nombre particules arrivees', self.nbparticulesArrived
            print t
            self.Advance(nIter,alpha,dec=deca,tdeb=t)
            #number-n+1
            self.Advance((number+1)*nIter,alpha,dec=0,tdeb=self.tinit+(n+1)*nIter*self.dt*3600.)
            t=t+nIter*self.dt*3600.
            sortedPart = sorted(self.particulesArrived.values(), key=operator.attrgetter('endtime'))
            sortedPart2 = sorted(self.particulesArrived.values(), key=operator.attrgetter('distTot'))
            print 'endtime :' ,sortedPart[0].endtime
            if n<number-2:
                l = len(sortedPart)
                for i in range(0,20):
                    m = sortedPart[i]
                    m2 = sortedPart2[i]
                    mr = random.choice(sortedPart)
                    self.InsertParticuleInter(m,alpha,5,ind=indice)
                    self.InsertParticuleInter(m2,alpha,5,ind=indice)
                    self.InsertParticuleInter(mr,alpha,5,ind=indice)
            self.kill()        
                    
        print 'Iteration : ',n
        print 'Nombre de particules ',len(self.particules)
        print 'nombre particules arrivees', len(self.particulesArrived)
        sortedPart = sorted(self.particulesArrived.values(), key=operator.attrgetter(ordre) )
        return sortedPart
        
    #Fonction principale :
    #nIter : nombre de pas de temps dt par avances
    #nInit : nombre de particules emises a l'origine
    #alpha : importance relative du vent
    #deca : nombre de pas de temps pour lesquels on decalle le cap d'une particule emise
    #filterprop : on supprime les particules qui ont une distance estimee superieure a la ligne droite
    #             augmente du pourcentage mis en parametre
    #Retourne le dictionnaire de particules restante selon l'ordre de la distance totale ou autre 
    #
    def heuristique(self,nIter = 10,nInit = 50 , alt=25000. , vitesse=400. , alpha=0.03 ,deca=10,ordre='distTot',filterprop=0.1, \
                    propKill=0.1, limit = 400):
        self.InitializePart(nInit, alt, vitesse, alpha)
        t=self.t
        seuil = self.dist*(1.0+filterprop/100.)
        
        #nombre d'avancees necessaires a priori
        number = int(self.nStep / nIter)+1
        for n in range(0,number):
            
            print 'Iteration : ',n
            print 'Nombre de particules ',len(self.particules)
            print 'nombre particules arrivees', self.nbparticulesArrived
            self.Advance(nIter,alpha,dec=deca,tdeb=t)
            t=t+nIter*self.dt*3600.
            
            if n<number-1:
                
                self.kill(proportion = propKill , seuil = seuil,  limit = limit)
                sortedPart = sorted(self.particules.values(), key=operator.attrgetter('distTot'))
                sortedPart2 = sorted(self.particules.values(), key=operator.attrgetter('distDone'),reverse=True)
                l = len(sortedPart)
                l1 = int(0.3 * len(sortedPart))
                for i in range(0,l1):
                    m = sortedPart[i]
                    m2 = sortedPart2[i]
                    if(m.arrived == 0):
                        self.InsertParticule(m,alpha,4)
                    if(m2.arrived == 0):
                        self.InsertParticule(m2,alpha,4)
        
        print 'Iteration : ',n
        print 'Nombre de particules ',len(self.particules)
        print 'nombre particules arrivees', len(self.particulesArrived)
        sortedPart = sorted(self.particulesArrived.values(), key=operator.attrgetter(ordre) )
        return sortedPart

        #tue une proportion de particules et les particules
    def kill(self,proportion=1,limit=1000):
        #supprime les particules
        sortedKey=sorted(self.particulesArrived.keys(), key = lambda num: self.particulesArrived[num].endtime,reverse = True)
        for i in range(0,int(proportion*len(sortedKey))):
            if len(self.particules)<=limit:
                break
            if sortedKey[i] in self.particules and self.particules[sortedKey[i]].arrived==0: #\
                self.particules.pop(sortedKey[i],None)
        length=len(self.particules)
        
        
                
    
    #Classe les particules en accord avec un attribut donne : filter
    #ne garde par defaut que les particules qui ne sont pas trop eloigne du trajet rectiligne : pourcentage filterprop
    #Autres attributs utilisables : distTot,endtime,cost...
    def  selectBest(self,filterprop=0.1,filter='vitmoy',rev=True):
        seuil = self.dist*(1+filterprop/100.)
        filt=dict((k, v) for (k, v) in self.particules.iteritems() if v.distTot<seuil)
        l=len(filt)
        while l<5:
            print seuil
            print l
            seuil=seuil*1.2
            filt=dict((k, v) for (k, v) in self.particules.iteritems() if v.distTot<seuil)
            l=len(filt)
        
        sortfilt=sorted(filt.values(), key=operator.attrgetter(filter),reverse=rev)    
        return sortfilt

    def InsertParticule(self,particule,alpha=0.03,nb=1,Ang=[]):
        self.InitializePart(nb,particule.alt,particule.vitesse,alpha,[particule.lat,particule.long],[self.arr[0],self.arr[1]],
                             particule.path,particule.cost,Ang,tabang=particule.tabang,intr = 1,tabvg=particule.tabvg)
    def InsertParticuleInter(self , particule , alpha=0.03 , nb=1 , Ang=[], ind=0):
        ind=min(ind,len(particule.path)-1)
        self.InitializePart(nb , particule.alt , particule.vitesse , alpha , [particule.path[ind][0],particule.path[ind][1]],\
                            [self.arr[0],self.arr[1]],
                             particule.path[0:ind+1],particule.cost,Ang,tabang=particule.tabang[0:ind+1],intr = 1, \
                             tabvg=particule.tabvg[0:ind+1])    


    
    
    
    #wind in knots, airspeed in knots
    def calculateGroundSpeed(self,AirSpeed,wind,cap):
        angl = np.arctan2(wind[1],wind[0])
        #projection du vent sur la direction du cap
        proj = np.linalg.norm(wind,2)*np.cos(angl-cap)
        #composante orthogonale a la direction du cap
        orth = np.linalg.norm(wind,2)*np.sin(angl-cap)
        #vitesse sol direction cap0 du a la vitesse air (suppose tjrs positive..)
        vg = np.sqrt(AirSpeed**2-orth**2)
        #vraie vitesse ground
        vg = vg + proj
        return vg
    
    #update the position from initial coordinates dep with a ground spped vg, a step time dt, a cap
    #dt : in hour
    #vg : knots
    def updatePosition(self,dep,vg,dt,cap):
        lat = dep[0]+vg*dt*np.cos(cap)/60
        long = dep[1] + vg*self.dt*np.sin(cap)/(np.cos(np.pi/360.*(dep[0]+lat)))/60
        return [lat,long]
    

    
def saveTrajectory(list,cost):
    c = csv.writer(open("../res/resultatParticule.csv","wb"))
    c.writerow(["Etape","Latitude","Longitude","Altitude","Vitesse"])

    i=0
    for e in list:
        i=i+1
        c.writerow([i,e[0],e[1],e[2],e[3]])

    c.writerow("Cout")
    c.writerow([cost])

def launchBlind(sim,numbIter = 50 ,nIter = 10, nInit = 30, deca = 10, alpha = 0.03,vitesse = 400,alt = 25000,\
                        filterprop=0.1,propKill=0.5,limit=20,ordre='endtime'):
   
    sim.firstSolution(alt=alt,vitesse=vitesse)
    sim.setSomeParam(numbIter)
    
    tab=sim.heuristiqueStep(nIter = nIter, nInit = nInit, deca = deca, alpha = alpha,vitesse = vitesse,alt = alt,\
                        filterprop=filterprop,propKill=propKill,limit=limit,ordre=ordre)
    for i in range(0,0):
        tab = sim.heuristiqueStep(nIter = nIter, nInit = 0, deca = deca/2, alpha = alpha,vitesse = vitesse,alt = alt,\
                        filterprop=filterprop,propKill=propKill,limit=limit,ordre=ordre)
    A = sim.dep
    B = sim.arr
    tinit = sim.tinit
    cout = ex.evaluate(trajet=[[A[0],A[1],alt,vitesse],[B[0],B[1],alt,vitesse]],timestep=sim.dt*3600,initTime= tinit)
    print 'Cout du modele avion : ', cout
    vol = rf.Flight()
    vol.loadFromCSV("simresult.csv")

    print len(sim.particules), ' Particules dans le domaine'
    print len(tab), ' Particules optimales'

    coutmin=100000
    idmin='Avion'
    
    #itere sur les 10 meilleures particules a priori
    #for part in tab:
    i=0
    imin=0
    for part in tab[0:10]:
        liste=part.path
        cout2=ex.evaluate(trajet=liste,timestep=sim.dt*3600,initTime=tinit)
        v = rf.Flight()
        v.loadFromCSV("simresult.csv")
        
        print cout2
        if(cout2<=coutmin):
            coutmin=cout2
            idmin=part
            listmin=list(liste)
            imin=i
        i=i+1    
    coutmin=ex.evaluate(trajet=listmin,timestep=sim.dt*3600,initTime=tinit)
    plt.figure()
    volOpt=rf.Flight()
    volOpt.loadFromCSV("simresult.csv")

    shift=idmin.tabang
    evolShift=[float(i[0]) for i in shift]
    evol = polyfitAngle(sim, evolShift,degree = 5)
    path = sim.AdvancePart(idmin, tdeb = tinit,tabAng = evol)
    path2 =  sim.AdvancePart(idmin, tdeb = tinit,tabAng = evolShift[1::])
    vol_re_eval = rf.Flight()
    vol_lisse = rf.Flight()
    cout_re_eval = ex.evaluate(trajet=path2,timestep=sim.dt*3600,initTime=tinit) 
    vol_re_eval.loadFromCSV("simresult.csv")
    cout_poly =ex.evaluate(trajet=path,timestep=sim.dt*3600,initTime=tinit)
    vol_lisse.loadFromCSV("simresult.csv")
    vol_re_eval.plotFlight()
    plt.hold(True)
    vol_lisse.plotFlight()
    plt.show()
    print 'cout avion : ', cout
    print 'cout sortie particule : ',coutmin
    print 'cout reeval : ',cout_re_eval
    print 'cout lisse : ',cout_poly

def polyfitAngle(sim,tabang,degree = 2):
    l = len(tabang)
    t = np.linspace(start=sim.tinit, stop=sim.tinit+(l-1)*sim.dt, num = l-1)
    p = np.polyfit(x=t,y=tabang[1::],deg=degree)
    return np.polyval(p,t)
    
    
if __name__=="__main__":

    alt=35000.
    #alt=32500.
    vitesse=440.

    #regl0
    A=[33.,-12.]
    B=[33.942,-18.408]
    #B=[35.,-15.]
    #A=[32.,-16.]

#   #regl 1
    #A=[53.,18.]
    #B=[42.,1.]
 #  B=[55.,12.]
#   A=[41.,15.]
    #regl 2
    #A=[46.,12.]
    #B=[50.,9.]
    
    #toulouse paris
    A=[43.6,1.43]
    B=[48.51,2.2] 
    iT=0
    
    
    #test wind
    A=[40,-20]
    B=[47.5,-13]
    sim = Simulation(A,B,tinit=iT)

    #regl0
    #sim.load_map('u_small.npz', 'v_small.npz', 'lats_small.npz','lons_small.npz','altitude_small.npz','time_small.npz')
    #sim.load_map('u_default.npz','v_default.npz','lats_default.npz','lons_default.npz','altitude_default.npz','time_default.npz')
    #sim.load_map('../npz/u.npz', '../npz/v.npz', '../npz/lats.npz','../npz/lons.npz','../npz/altitude.npz','../npz/time.npz')
    #regl 1, 2
    #Ranges Bigger : lat [55,40],logit[0,20],alt[200,1000],time[0,72000]
    #sim.load_map('../npz/u_bigger.npz', '../npz/v_bigger.npz', '../npz/lats_bigger.npz',\
    #          '../npz/lons_bigger.npz','../npz/altitude_bigger.npz','../npz/time_bigger.npz')
    
    #sim.load_map('../npz/u_Par_Toul.npz', '../npz/v_Par_Toul.npz', '../npz/lats_Par_Toul.npz',\
    #           '../npz/lons_Par_Toul.npz','../npz/altitude_Par_Toul.npz','../npz/time_Par_Toul.npz')
    sim.load_map('../npz/u_wind.npz','../npz/v_wind.npz',\
                                       '../npz/lats_wind.npz','../npz/lons_wind.npz',\
                                       '../npz/altitude_wind.npz','../npz/time_wind.npz')
    
    print sim.map.rangeT,sim.map.rangeLat,sim.map.rangeLong,sim.map.rangeAlt
    
        
    
    
    #petit
    #Grand Trajet : gain de 16 kilos sur un trajet de 13000 environ  0.1% youhou
    numbIter = 50
    #Petit Trajet : 
    nIter=10
    nInit=100
    deca=10
    alpha=0.03
    filterprop=0.1
    propKill=0.5
    limit=20
    ordre = "endtime"
    ch = 0
    
    if ch == 0:
        launchBlind(sim,numbIter,nIter, nInit, deca, alpha,vitesse,alt,\
                        filterprop,propKill,limit,ordre)
    
    
    if ch == 1:
        
        numbIter=70
        sim.firstSolution(alt=alt,vitesse=vitesse)
        sim.setSomeParam(numbIter)
        print sim.dt
        tab=sim.heuristiqueStep(nIter = nIter, nInit = nInit, deca = deca, alpha = alpha,vitesse = vitesse,alt = alt,\
                            filterprop=filterprop,propKill=propKill,limit=limit,ordre='distTot')
        
        cout = ex.evaluate(trajet=[[A[0],A[1],alt,vitesse],[B[0],B[1],alt,vitesse]],timestep=sim.dt*3600,initTime=iT)
        print 'Cout du modele avion : ', cout
        vol = rf.Flight()
        vol.loadFromCSV("simresult.csv")
    
        print len(sim.particules), ' Particules dans le domaine'
        print len(tab), ' Particules optimales'
    
        coutmin=100000
        idmin='Avion'
        coutP=[]
        costP=[]
        costWind=[]
        vitesseMoy=[]
        vgTabAv=[]
        vgTabApp=[]
        TAv=[]
        TApp=[]
        flown=[]
        flownAv=[]
        endtime=[]
        endtimeAv=[]
        #itere sur les 10 meilleures particules a priori
        #for part in tab:
        i=0
        imin=0
        for part in tab[0:10]:
            liste=part.path
            cout2=ex.evaluate(trajet=liste,timestep=sim.dt*3600,initTime=iT)
            v = rf.Flight()
            v.loadFromCSV("simresult.csv")
            coutP.append(cout2)
            costP.append(part.distTot)
            costWind.append(part.cost)
            vitesseMoy.append(part.vitmoy)
            vgTabAv.append([float(vgrnd) for vgrnd in v.grd_spd])
            vgTabApp.append(part.tabvg)
            TAv.append([float(time) for time in v.time])
            TApp.append(np.linspace(0,part.endtime,len(part.tabvg)))
            flown.append(part.distTot)
            flownAv.append(float(v.flown_dst[-1]))
            endtime.append(part.endtime)
            endtimeAv.append(float(v.time[-1]))
            print cout2
            if(cout2<=coutmin):
                coutmin=cout2
                idmin=part
                listmin=list(liste)
                imin=i
            i=i+1    
        coutmin=ex.evaluate(trajet=listmin,timestep=sim.dt*3600,initTime=iT)
        plt.figure()
    
        volOpt=rf.Flight()
        volOpt.loadFromCSV("simresult.csv")
    
        print 'cout trajet dir : ',cout
        print 'cout via heuristique : ',coutmin
        print 'dist dir',vol.flown_dst[-1]
        print 'dist bye',volOpt.flown_dst[-1]
        print(coutP)
        print(vitesseMoy)
        print (flown)
        print (flownAv)
        print endtime
        print endtimeAv
    
    
        plt.plot(TAv[imin],vgTabAv[imin])
        plt.hold(True)
        plt.plot(TApp[imin],vgTabApp[imin])
        plt.draw()
        plt.figure()
    
    
        
        plt.figure()
        plt.plot(endtimeAv,coutP,'*')
        plt.draw()
        plt.figure()
        plt.plot(endtime,coutP,'o')
        plt.title('cout en fonction du temps')
        plt.draw()
        
        plt.figure()
        plt.plot(flownAv,coutP)
        plt.title('cout en fonction de la distance')
        plt.draw()
        
        plt.figure()
        plt.plot(vitesseMoy,coutP)
        plt.title('cout en fonction vitesse moyenne')
        plt.draw()
        
        plt.figure()
        plt.plot(costWind,coutP)
        plt.title('cout en fonction du cout vent')
        plt.draw()
    
        shift=idmin.tabang
        evolshift=[float(i[0]) for i in shift]
        plt.figure()
        plt.plot(evolshift)
        plt.title('Evolution d angle de decallage')
        plt.draw()
        
    
        taux=[float(x)/max(float(y),1) for x,y in zip(vol.sum_fuel_burn,vol.flown_dst)]
        taux2=[float(x)/max(float(y),1) for x,y in zip(volOpt.sum_fuel_burn,volOpt.flown_dst)]
    
        fig=plt.figure()
        plt.plot(vol.time[1:-2],taux[1:-2],'r')
        plt.plot(volOpt.time[1:-2],taux2[1:-2],'b')
        plt.title('comparaison taux de carburant : rouge trajet direct, bleu trajet heuristique')
        plt.draw()
        plt.figure()
        
        
        volOpt.plotFlight()
        plt.hold(True)
        vol.plotFlight()
        plt.show()
        plt.pause(10)
        print vol.time[-1],volOpt.time[-1],idmin.endtime
    
        saveTrajectory(listmin, coutmin)
