#Heuristique considerant l'avion comme une particule ponctuelle, soumis a un champ de potentiel
#represente par le vent d'une part et par l'arrivee d'autre part'

import orthodromie as ort
import numpy as np
import matplotlib.pyplot as plt
import interpolator as it
import readFlight as rf
import execsim as ex
import csv
import path
import operator
import random
import TauxMontee as tm
import buildAlt as bA
import extractFlight
from _ctypes import Array

class Particule():

    #Cree une particule aux coordonnees (lat,long) avec une vitesse dans le
    #repere (u_theta, u_phi)
    #Cette particule est destinee a aller a une destination
    #elle a un cout variable
    #elle retient une partie de sa trajectoire passe, la distance qu'elle a parcouru et
    #celle qui lui reste a parcourir
    #mode : 1 : montee , 2 :croisiere, 3: descente
    def __init__(self,origin,destination,alt,vitesse,path=[],ang=0.,gone=0,arrived=0,tabang=[],intr=0,alpha=0.03,tabvg=[],\
                 mode = 1, tabmode = []):
        self.lat = origin[0]
        self.long = origin[1]
        self.alt = alt
        self.vitesse = vitesse

        self.latO = origin[0]
        self.longO = origin[1]
        self.altO = origin[2]
        self.vitesseO = origin[3]


        self.latDest = destination[0]
        self.longDest = destination[1]
        self.altDest = destination[2]
        self.vitesseDest = destination[3]

        self.path = path
        if path==[]:
            self.path = [[self.lat,self.long,self.alt,self.vitesse]]
        self.calc_distance()
        self.ang = ang
        self.gone = gone
        self.arrived = arrived
        self.tabang = tabang
        if tabang==[]:
            self.tabang=[(ang,0)]
        self.tabvg=tabvg
        self.iter = len(self.path)
        self.intr=intr
        self.alpha=alpha
        self.endtime = 0.0
        self.vitmoy = 0.0
        self.mode = mode
        self.tabmode = tabmode
        if tabmode==[]:
            self.tabmode = [mode]

    def calc_distance(self):
        self.distDone=0
        A=self.path[0]
        for B in self.path:
            self.distDone = self.distDone + ort.distanceOrtho([A[0],A[1]],[B[0],B[1]])
            A=B
        self.distRest = ort.distanceOrtho([A[0],A[1]],[self.latDest,self.longDest])
        self.distTot = self.distDone + self.distRest

    def maj_distance(self,point,shift=0.,vg=0.0):
        self.path.append(point)
        last=self.path[-2]
        self.distDone = self.distDone + ort.distanceOrtho([last[0],last[1]],[point[0],point[1]])
        self.distRest = ort.distanceOrtho([point[0],point[1]] , [self.latDest,self.longDest])
        self.distTot = self.distDone + self.distRest
        self.tabang.append((shift,self.iter))
        self.iter = self.iter+1
        self.tabvg.append(vg)
        self.vitmoy=sum(self.tabvg, 0.0) / len(self.tabvg)

class Simulation3D:
    #definit l'objet simulation avec un depart, une arrivee et un temps initial
    #dep = [lat,long,alt,vitesse]
    #arr = [lat,long,alt,vitesse]
    def __init__(self, dep , arr , tinit = 0, dt = 200, altMax = 35000):
        self.dep = dep
        self.arr = arr
        self.particules = {}
        self.particulesArrived = {}
        self.nbparticules = 0
        self.nbparticulesArrived = 0
        self.sense = 1
        if(arr[1]<dep[1]):
            self.sense = -1
        self.tinit = tinit
        #en heure
        self.dt = dt/3600
        self.altMax = altMax
        self.simuMontee = tm.simuMontee(dep = self.dep, dt = dt, altMax = altMax)
        self.simuDescente = tm.simuDescente(arr = self.arr, dt = dt, altMax = altMax)
        self.simuMontee.simuMontee()
        self.simuDescente.simuDescente()
        self.alt_m = self.simuMontee.alt
        self.v_m = self.simuMontee.v
        self.alt_d = self.simuDescente.alt[::-1]
        self.v_d = self.simuDescente.v[::-1]
        self.distanceD = self.simuDescente.distance
        self.ba = bA.buildAlt(self.dep,self.arr,0.9)


    def redefineParam(self,dep,arr,altMax=35000):
        self.dep = dep
        self.arr = arr
        self.altMax = max([dep[2],arr[2],altMax])
        
        self.particules = {}
        self.particulesArrived={}

    #Change le temps initial de la simulation et le pas de temps
    def set_dt(self,tinit,dt):
        self.dt = dt
        self.tinit = tinit

    #charge la carte des vents correspondants a des fichiers npz
    def load_map(self,u,v,lats,lons,alt,time):
        inte = it.interpol_from_npz(u,v,lats,lons,alt,time)
        self.map = inte


    #Estime une solution approchee a l'optimisation
    def firstSolution(self,alt=25000.,vitesse=400.,dep=[],arr=[]):
        if(dep == []):
            dep=self.dep
        if(arr == []):
            arr=self.arr
        self.cout = ex.evaluate(trajet=[dep,arr],initTime=self.tinit)
        print 'Cout du modele avion : ', self.cout
        vol = rf.Flight()
        vol.loadFromCSV("simresult.csv")
        self.vol=vol


    #Estime des parametres adaptes a l'optimisation
    #nStep est le nombre de point parcouru souhaite entre depart et arrivee :
    # va donner un parametre dt adapte
    def setSomeParam(self,nStep=100):
        dtSec=(float(self.vol.time[-1])-float(self.vol.time[0]))/nStep
        dtH=dtSec/3600
        self.dt=dtH
        self.nStep = nStep
        modVitesse=[np.sqrt(float(x)**2+float(y)**2) for x,y in zip(self.vol.uwind,self.vol.vwind)]
        #distance parcourue via le modele avion
        self.dist = float(self.vol.flown_dst[-1])
        self.time = float(self.vol.time[-1])

        self.simuMontee = tm.simuMontee(dep = self.dep, dt = dtSec, altMax = self.altMax)
        self.simuDescente = tm.simuDescente(arr = self.arr, dt = dtSec, altMax = self.altMax)
        self.simuMontee.simuMontee()
        self.simuDescente.simuDescente()
        self.alt_m = self.simuMontee.alt
        self.v_m = self.simuMontee.v
        self.alt_d = self.simuDescente.alt[::-1]
        self.v_d = self.simuDescente.v[::-1]
        self.distanceD = self.simuDescente.distance

    #Cree un certain nombre de particule nInit placee a dep et ayant comme arrivee arr
    def InitializePart(self,nInit,alt=25000.,vitesse=400.,alpha=0.03,dep=[],arr=[],\
                       path=[],Ang=[],tabang=[],intr=0,tabvg=[],method=np.random.randn,mode = 1, tabmode = [],amplitude = 0.5):
        if(dep == []):
            dep=self.dep
        if(arr == []):
            arr=self.arr
        if(Ang == []):
            Ang=0.9*method(nInit)
            #Ang=np.linspace(0,0.2,nInit)

        for i in range(0,nInit):
            self.particules[self.nbparticules] = Particule(origin = dep,destination = arr,alt = alt,vitesse = vitesse,\
                                                           path = list(path),\
                                                           ang = Ang[i],gone=0,arrived=0,tabang=list(tabang),\
                                                           intr=intr,tabvg=list(tabvg),alpha=alpha,\
                                                           mode = mode, tabmode = tabmode)
            self.nbparticules = self.nbparticules+1

    def InitializeOrtho(self,alt=25000.,vitesse=400.,alpha=0.03,dep=[],arr=[]):
        if(dep == []):
            dep=self.dep
        if(arr == []):
            arr=self.arr
        self.particules['ref'] = Particule(origin = dep,destination = arr, alt = alt, vitesse = vitesse,ang = 0,alpha=0)
    #Avance les particules indicees ind_part presentes vers l'arrivee
    def Advance3D(self,nIter,alpha=0.03,ind_part=[],dec=10,tdeb=0.0):
        if(ind_part==[]):
            ind_part = list(self.particules.keys())

        for ind in ind_part:
            i = 0
            fin = 0
            t = [tdeb]
            part = self.particules[ind]
            arr =[part.latDest,part.longDest,part.altDest,part.vitesseDest]
            if(part.arrived==1):
                fin=1
            if(ort.distanceOrthoRad([part.lat,part.long],arr)==0):
                fin=1
            wind=[0,0]
            alpha = part.alpha
            while (fin==0) and (i<nIter):
                try:
                    wind = self.map.inter(part.lat,part.long,part.alt,t[i])
                except:
                    print part.ang
                    print part.lat,part.long,part.alt,t[i]
                    print('une particule sort de la carte !')
                    print part.long
                    print part.lat
                    print ort.distanceOrtho([part.lat,part.long],arr)
                    self.particules.pop(ind)
                    break
                angl = np.arctan2(wind[1],wind[0])
                d = ort.distanceOrthoRad([part.lat,part.long],arr)
                s=self.sense
                if part.long>self.arr[1]:
                    s=-1
                capAng = (np.sin(np.pi/180*arr[0])-np.sin(np.pi/180*part.lat)*np.cos(d)) / ( \
                             np.cos(np.pi/180*part.lat)*np.sin(d))
                if capAng<=-1:
                    capAng = -1
                if capAng>=1:
                    capAng = 1
                cap0 = s * (1-alpha) * np.arccos(capAng) + alpha * angl

                shift = 0.

                #garde en memoire son decalage de cap initial pendant un certain laps de temps
                if(i<dec) and (part.gone==0):
                    shift = (part.ang)/2

                cap0 = cap0+shift
                lat1 = part.lat
                long1 = part.long
                #calcul de la vitesse ground, le module de la vitesse air est 400, le cap est cap0
                w = [1.94384*vent for vent in wind] #vent en noeud marin
                d = ort.distanceOrtho([lat1,long1],arr)
                if d<(1.5*self.distanceD):
                    part.mode = 3
                #mode montee
                if part.mode == 1:
                    [na,nv] = self.simuMontee.simuOneStep(alt = part.alt)
                    part.alt = na
                    #part.vitesse = nv
                    part.vitesse =  2.669*10**2 + na * 4.985e-3 
                    if part.alt == self.altMax:
                        part.mode = 2

                #mode croisiere
                if part.mode == 2:
                    d = ort.distanceOrtho([lat1,long1],arr)
                    if d<(1.5*self.distanceD):
                        part.mode = 3

                if part.mode == 3:
                    [na,nv] = self.simuDescente.simuOneStep(alt = part.alt)
                    part.alt = na
                    #part.vitesse =nv
                    part.vitesse =  2.669*10**2 + na * 4.985e-3 
                part.tabmode.append(part.mode)
                vg = self.calculateGroundSpeed(part.vitesse,w,cap0)
                if(len(part.tabvg) != 0):
                    vg = int((vg+part.tabvg[-1])/2)

                #mise a jour des coordonnees
                part.lat = lat1+vg*self.dt*np.cos(cap0)/60.
                part.long = part.long + vg*self.dt*np.sin(cap0)/(np.cos(np.pi/360.*(lat1+part.lat)))/60.

                d = ort.distanceOrtho([lat1,long1],arr)
                if(d<2*vg*self.dt):
                    d2=d*np.pi/(60.*180.)
                    cap0 = self.sense * (1-alpha) * np.arccos((np.sin(np.pi/180*arr[0])-np.sin(np.pi/180*lat1)*np.cos(d2)) / ( \
                             np.cos(np.pi/180*lat1)*np.sin(d2)))
                    vg=self.calculateGroundSpeed(part.vitesse,w,cap0)
                    fin = 1
                    part.lat = arr[0]
                    part.long = arr[1]
                    part.alt = arr[2]
                    part.vitesse = arr[3]
                    self.nbparticulesArrived = self.nbparticulesArrived + 1
                    part.arrived = 1
                    dtfin = d/vg
                    tfin = t[i] + (self.dt+dtfin) * 3600.   #*3600
                    part.endtime = tfin
                    self.particulesArrived[ind]=part
                    self.particules.pop(ind,None)


                part.maj_distance([part.lat,part.long,part.alt,part.vitesse],shift,vg)
                if fin==1:
                    part.vitmoy = (part.distDone/(part.endtime/3600.))

                t.append(tdeb+float(i)*self.dt*3600.)
                i = i+1

            part.gone=1
    #Avance les particules indicees ind_part presentes vers l'arrivee
    def AdvancePart(self,particule,alpha=0.03,tabAng = [],tdeb = 0.0):
        if(tabAng==[]):
            tabAng =[float(t[0]) for t in particule.tabang]

        l = len(tabAng)
        i = 0
        fin = 0
        t = [tdeb]
        lat = self.dep[0]
        long = self.dep[1]
        vitesse = particule.vitesse
        alt = particule.alt
        path = [[lat,long,alt,vitesse]]
        arrived = 0
        vgInit = 0
        wind=[0,0]
        arr = self.arr

        while (fin==0) and (i<l):
            try:
                wind = self.map.inter(lat,long,alt,t[i])
            except:
                #print part.lat,part.long,part.alt,t[i]
                print('une particule sort de la carte !')
                print long
                print lat

            angl = np.arctan2(wind[1],wind[0])
            d = ort.distanceOrthoRad([lat,long],arr)
            s=self.sense
            if long>self.arr[1]:
                s=-1

            cap0 = s * (1-alpha) * np.arccos((np.sin(np.pi/180*arr[0])-np.sin(np.pi/180*lat)*np.cos(d)) / ( \
                             np.cos(np.pi/180*lat)*np.sin(d))) + alpha * angl

            shift = tabAng[i]
            cap0 = cap0+shift
            w = [1.94384*vent for vent in wind] #vent en noeud marin
            vg=self.calculateGroundSpeed(vitesse,w,cap0)
            if(i!=0):
                vg=int((vg+vgInit)/2)
            vgInit = vg
            lat1 = lat
            long1 =long
            #mise a jour des coordonnees
            lat = lat1+vg*self.dt*np.cos(cap0)/60.
            long = long + vg*self.dt*np.sin(cap0)/(np.cos(np.pi/360.*(lat1+lat)))/60.
            d = ort.distanceOrtho([lat1,long1],arr)
            if(d<2*vg*self.dt):
                d2=d*np.pi/(60.*180.)
                cap0 = self.sense * (1-alpha) * np.arccos((np.sin(np.pi/180*arr[0])-np.sin(np.pi/180*lat1)*np.cos(d2)) / ( \
                             np.cos(np.pi/180*lat1)*np.sin(d2)))
                fin = 1
                lat = arr[0]
                long = arr[1]
                fin = 1

            t.append(tdeb+float(i)*self.dt*3600.)
            path.append([lat,long,alt,vitesse])
            i = i+1

        if fin==0:
            path.append([arr[0],arr[1],alt,vitesse])

        return path




    def heuristiqueStep(self, nIter = 10 , nInit = 50 , alt = 25000.,\
                        vitesse = 400., alpha = 0.03, deca = 10, \
                        ordre = 'endtime', filterprop = 0.1 ,\
                        propKill = 0.1, limit = 400,amplitude = 0.5 ):

        print alt
        self.InitializeOrtho(alt,vitesse,alpha)
        self.InitializePart(nInit, alt, vitesse, alpha,amplitude = amplitude)
        t=self.tinit
        print t
        #nombre d'avancees necessaires a priori
        number = int(self.nStep / nIter)+1
        evolTime =[]
        for n in range(0,number):

            indice=(n+1)*nIter
            print 'Iteration : ',n
            print 'Nombre de particules ',len(self.particules)
            print 'nombre particules arrivees', len(self.particulesArrived)
            print t
            self.Advance3D(nIter,alpha,dec=deca,tdeb=t)
            #number-n+1
            self.Advance3D((number+1)*nIter,alpha,dec=0,tdeb=self.tinit+(n+1)*nIter*self.dt*3600.)
            t=t+nIter*self.dt*3600.
            sortedPart = sorted(self.particulesArrived.values(), key=operator.attrgetter('endtime'))
            sortedPart2 = sorted(self.particulesArrived.values(), key=operator.attrgetter('distTot'))
            print 'endtime :' ,sortedPart[0].endtime
            evolTime.append(sortedPart[0].endtime)
            if n<number-1:
                l = len(sortedPart)
                for i in range(0,40):
                    m = sortedPart[i]
                    m2 = sortedPart2[i]
                    mr = random.choice(sortedPart)
                    self.InsertParticuleInter(m,alpha,10,ind=indice,amplitude = amplitude)
                    self.InsertParticuleInter(m2,alpha,5,ind=indice,amplitude = amplitude)
                    self.InsertParticuleInter(mr,alpha,5,ind=indice,amplitude = amplitude)
            if n>2:
                self.kill()
            fig = plt.figure()
            plt.hold(True)
            color=['r','y','g','b','k']
            for i in range(0,5):
                path = sortedPart[i].path
                l=len(path)
                path=path[0:(min(l,indice+1))]
                lat=[p[0] for p in path]
                long=[p[1] for p in path]
                plt.plot(long,lat,color[i]+'o')
            
            self.map.plot2(0.5,38000,t)
            plt.axis([min(self.dep[1],self.arr[1]),max(self.dep[1],self.arr[1]),min(self.dep[0],self.arr[0]),max(self.dep[0],self.arr[0])])    
            plt.title('Trajectoire vols optimaux a la '+str(n+1)+'eme iteration')
            fig.savefig(str(n)+'_'+'BigMaxouIntermed.png')
                
        fig=plt.figure()
        plt.plot(range(0,len(evolTime)),evolTime)
        plt.title('Evolution temps arrivee en fonction des iterations')        
        fig.savefig('evolTime_BigMaxou.png')
        print 'Iteration : ',n
        print 'Nombre de particules ',len(self.particules)
        print 'nombre particules arrivees', len(self.particulesArrived)
        sortedPart = sorted(self.particulesArrived.values(), key=operator.attrgetter(ordre) )
        return sortedPart

    #Fonction principale :
    #nIter : nombre de pas de temps dt par avances
    #nInit : nombre de particules emises a l'origine
    #alpha : importance relative du vent
    #deca : nombre de pas de temps pour lesquels on decalle le cap d'une particule emise
    #filterprop : on supprime les particules qui ont une distance estimee superieure a la ligne droite
    #             augmente du pourcentage mis en parametre
    #Retourne le dictionnaire de particules restante selon l'ordre de la distance totale ou autre
    #
    def heuristique(self,nIter = 10,nInit = 50 , alt=25000. , vitesse=400. , alpha=0.03 ,deca=10,ordre='distTot',filterprop=0.1, \
                    propKill=0.1, limit = 400):
        self.InitializePart(nInit, alt, vitesse, alpha)
        t=self.t
        seuil = self.dist*(1.0+filterprop/100.)

        #nombre d'avancees necessaires a priori
        number = int(self.nStep / nIter)+1
        for n in range(0,number):

            print 'Iteration : ',n
            print 'Nombre de particules ',len(self.particules)
            print 'nombre particules arrivees', self.nbparticulesArrived
            self.Advance(nIter,alpha,dec=deca,tdeb=t)
            t=t+nIter*self.dt*3600.

            if n<number-1:

                self.kill(proportion = propKill , seuil = seuil,  limit = limit)
                sortedPart = sorted(self.particules.values(), key=operator.attrgetter('distTot'))
                sortedPart2 = sorted(self.particules.values(), key=operator.attrgetter('distDone'),reverse=True)
                l = len(sortedPart)
                l1 = int(0.3 * len(sortedPart))
                for i in range(0,l1):
                    m = sortedPart[i]
                    m2 = sortedPart2[i]
                    if(m.arrived == 0):
                        self.InsertParticule(m,alpha,4)
                    if(m2.arrived == 0):
                        self.InsertParticule(m2,alpha,4)
                
                
                
                        
        print 'Iteration : ',n
        print 'Nombre de particules ',len(self.particules)
        print 'nombre particules arrivees', len(self.particulesArrived)
        sortedPart = sorted(self.particulesArrived.values(), key=operator.attrgetter(ordre) )
        return sortedPart

        #tue une proportion de particules et les particules
    def kill(self,proportion=1,limit=10000):
        #supprime les particules
        sortedKey=sorted(self.particulesArrived.keys(), key = lambda num: self.particulesArrived[num].endtime,reverse = True)
        for i in range(0,int(proportion*len(sortedKey))):
            if len(self.particulesArrived)<=limit:
                break
            if sortedKey[i] in self.particulesArrived and sortedKey[i]!='ref':  #\
                self.particulesArrived.pop(sortedKey[i],None)





    #Classe les particules en accord avec un attribut donne : filter
    #ne garde par defaut que les particules qui ne sont pas trop eloigne du trajet rectiligne : pourcentage filterprop
    #Autres attributs utilisables : distTot,endtime,cost...
    def  selectBest(self,filterprop=0.1,filter='vitmoy',rev=True):
        seuil = self.dist*(1+filterprop/100.)
        filt=dict((k, v) for (k, v) in self.particules.iteritems() if v.distTot<seuil)
        l=len(filt)
        while l<5:
            print seuil
            print l
            seuil=seuil*1.2
            filt=dict((k, v) for (k, v) in self.particules.iteritems() if v.distTot<seuil)
            l=len(filt)

        sortfilt=sorted(filt.values(), key=operator.attrgetter(filter),reverse=rev)
        return sortfilt

    def InsertParticule(self,particule,alpha=0.03,nb=1,Ang=[]):
        self.InitializePart(nb,particule.alt,particule.vitesse,alpha,particule.path[-1],\
                            self.arr, particule.path , Ang , tabang=particule.tabang,intr = 1,tabvg=particule.tabvg)
    def InsertParticuleInter(self , particule , alpha=0.03 , nb=1 , Ang=[], ind=0,amplitude = 0.5):
        ind=min(ind,len(particule.path)-1)

        self.InitializePart(nInit = nb , alt = particule.path[ind][2] , vitesse = particule.path[ind][3] , alpha = alpha , \
                            dep = particule.path[ind] ,arr = self.arr, \
                             path = particule.path[0:ind+1], Ang = Ang,tabang=particule.tabang[0:ind+1],intr = 1, \
                             tabvg=particule.tabvg[0:ind+1],mode = particule.tabmode[ind],tabmode = particule.tabmode[0:ind+1],\
                             amplitude = amplitude)





    #wind in knots, airspeed in knots
    def calculateGroundSpeed(self,AirSpeed,wind,cap):
        angl = np.arctan2(wind[1],wind[0])
        #projection du vent sur la direction du cap
        proj = np.linalg.norm(wind,2)*np.cos(angl-cap)
        #composante orthogonale a la direction du cap
        orth = np.linalg.norm(wind,2)*np.sin(angl-cap)
        #vitesse sol direction cap0 du a la vitesse air (suppose tjrs positive..)
        vg = np.sqrt(AirSpeed**2-orth**2)
        #vraie vitesse ground
        vg = vg + proj
        return vg

    #update the position from initial coordinates dep with a ground spped vg, a step time dt, a cap
    #dt : in hour
    #vg : knots
    def updatePosition(self,dep,vg,dt,cap):
        lat = dep[0]+vg*dt*np.cos(cap)/60
        long = dep[1] + vg*self.dt*np.sin(cap)/(np.cos(np.pi/360.*(dep[0]+lat)))/60
        return [lat,long]



def saveTrajectory(list,cost):
    c = csv.writer(open("../res/resultatParticule.csv","wb"))
    c.writerow(["Etape","Latitude","Longitude","Altitude","Vitesse"])

    i=0
    for e in list:
        i=i+1
        c.writerow([i,e[0],e[1],e[2],e[3]])

    c.writerow("Cout")
    c.writerow([cost])

class resultatSimulation():
    def __init__(self,bestPart,sim):
        self.bestPart = bestPart
        self.path = bestPart.path
        self.part_ortho = sim.particulesArrived['ref']
        self.path_ortho= self.part_ortho.path
        self.dt = sim.dt
        self.sim = sim
        self.tinit = sim.tinit
        self.dep = sim.dep
        self.arr = sim.arr
        self.cout = ex.evaluate(trajet=self.path,timestep = sim.dt*3600, initTime=self.tinit)
        self.volOpti = rf.Flight()
        self.volOpti.loadFromCSV("simresult.csv")
        self.cout_p_to_p = ex.evaluate(trajet=[self.dep,self.arr],timestep=sim.dt*3600,initTime= self.tinit)
        self.vol_p_to_p = rf.Flight()
        self.vol_p_to_p.loadFromCSV("simresult.csv")
        self.cout_ortho = ex.evaluate(trajet=self.path_ortho,timestep=sim.dt*3600,initTime= self.tinit)
        self.vol_ortho = rf.Flight()
        self.vol_ortho.loadFromCSV("simresult.csv")
    def printCost(self,name):
        print "cout ortho : ",self.cout_ortho
        print "cout point a point : ",self.cout_p_to_p
        print "cout Particule : ",self.cout
        print "carburant gagne : ",self.cout_ortho-self.cout
        print "taux : ", (self.cout_ortho-self.cout)*100/self.cout_ortho,"%"
        return [name,self.cout_ortho,self.cout_p_to_p,self.cout,(self.cout_ortho-self.cout)*100/self.cout_ortho]
    def plot_flight_save(self,num = 1):
        fig = plt.figure()
        plt.hold(True)
        self.sim.map.plot2(0.5,35000,0)
        self.volOpti.plotFlight(color = 'r')
        self.vol_p_to_p.plotFlight(color = 'y')
        self.vol_ortho.plotFlight(color = 'b')

        plt.title('Trajectoire du vol : rouge : optimal, jaune : point a point, bleu : orthodromie')
        fig.savefig(''+str(num)+'_'+'trajectoires.png')

    def save_trajectory(self,num=1):
        c = csv.writer(open("../res/"+str(num)+"_trajetOpti.csv","wb"))
        c.writerow(["Etape","Latitude","Longitude","Altitude","Vitesse"])
        cout = self.cout_ortho
        list = self.path
        i=0
        for e in list:
            i=i+1
            c.writerow([i,e[0],e[1],e[2],e[3]])

    def save_cost(self,num=1):
        c = csv.writer(open("../res/"+str(num)+"_cost.csv","wb"))

        c.writerow(["Cout optimal","Cout Ortho","Cout p_to_p"])
        c.writerow([self.cout,self.cout_ortho,self.cout_p_to_p])
        c.writerow(self.dep)
        c.writerow(self.arr)




def launchBlind(sim,numbIter = 65 ,nIter = 10, nInit = 30, deca = 10, alpha = 0.03,\
                        filterprop=0.1,propKill=0.5,limit=20,ordre='endtime',name = '1',\
                        amplitude=0.5):

    sim.firstSolution()
    sim.setSomeParam(numbIter)
    altInt = sim.dep[2]
    vitInt = sim.dep[3]
    tab=sim.heuristiqueStep(nIter = nIter, nInit = nInit, deca = deca, alpha = 0.03,\
                        vitesse = vitInt, alt = altInt ,filterprop=filterprop,propKill=propKill,limit=limit,ordre=ordre,\

                        amplitude = amplitude
                        )

    for i in range(0,0):
        tab = sim.heuristiqueStep(nIter = nIter, nInit = 0, deca = deca/2, alpha = alpha,vitesse = vitInt,alt = altInt,\
                        filterprop=filterprop,propKill=propKill,limit=limit,ordre=ordre)
    A = sim.dep
    B = sim.arr
    tinit = sim.tinit


    coutmin=100000
    idmin='Avion'
    #itere sur les 10 meilleures particules a priori
    #for part in tab:
    i=0
    imin=0

    for part in tab[0:10]:
        liste=part.path
        cout2=ex.evaluate(trajet=liste,timestep=sim.dt*3600,initTime=tinit)
        v = rf.Flight()
        v.loadFromCSV("simresult.csv")

        print cout2
        if(cout2<=coutmin):
            partmin = part
            coutmin=cout2
            idmin=part
            listmin=list(liste)
            imin=i
        i=i+1


    result = resultatSimulation(partmin,sim)
    p = result.printCost(name)
    result.plot_flight_save(name)
    result.save_cost(name)
    result.save_trajectory(name)
    return p

def polyfitAngle(sim,tabang,degree = 2):
    l = len(tabang)
    t = np.linspace(start=sim.tinit, stop=sim.tinit+(l-1)*sim.dt, num = l-1)
    p = np.polyfit(x=t,y=tabang[1::],deg=degree)
    return np.polyval(p,t)


if __name__=="__main__":

    iT = 0
    #Trajet 1
    lgood=[]
    A=[33,-27, 35000,440]
    B=[49,-10, 15000,440]
    lgood.append([A,B])
    #Trajet 2 favorable a cote de l'ortho
    A=[55,-10,35000,440]
    B=[65,12,35000,440]
    
    lgood.append([A,B])

    #Trajet 3 favorable a cote de l'ortho
    A=[58,-10,35000,440]
    B=[68,12,35000,440]
    
    lgood.append([A,B])
    #Trajet 4 favorable a cote de l'ortho
    A=[58,-10,35000,440]
    B=[68,20,35000,440]

    
    lgood.append([A,B])
    #Trajet 5 : trajet 4 reverse
    B=[58,-10,35000,440]
    A=[68,20,35000,440]
    
    lgood.append([A,B])

    #Trajet 6 beaucoup de vent defavorable
    A=[70,20,35000,440]
    B=[58,-10,35000,440]
    
    lgood.append([A,B])
    #Trajet 7
    A=[40,-10,35000,440]
    B=[55,20,35000,440]

    lgood.append([A,B])
    #trajet 8



    #trajet Reel 1 130830_170094736
    #A=[50.3875,6.22889,25000,400]
    #B=[46.6167,6.55,25000,400]

    #trajet reel 2 130830_170107018
    #A = [45.9417,3.18,25000,400]
    #B = [46.9658,2.32972,25000,400]
    #trajet 3 reel 130830_170080500,
    #A=[50.4589,5.91889,25000,400]
    #B=[51.1308,2.0,25000,400]
    #trajet 4 reel 130830_170085115
    #A=[47.5889,5.09861,33299.86876640419,440]
    #B=[46.4439,6.67944,34000.0,440]
    #trajet 6 reel 130830_170099763
    #A=[50.4128,5.93306,30000,450]
    #B=[47.9667,6.11667,31299.999999999996,450]

    #trajet 7
    #A = [46.4783,10.0433,35000.0,440.0]
    #B = [51.0642,1.0675,35000.0,440.0]
    #trajet 8
    #A =[50.3333,-5.7,34000.0,450.0]
    #B =[50.84,6.03722,29600.0,450.0]
    #trajet 9
    #A=[46.4783,10.0433,25000.0,440.0]
    #B=[51.0394,1.16056,25000.0,440.0]

    #trajet 10
    #A=[50.0911,1.64639,30000.0,440.0]
    #B=[46.5622,10.4792,30000.0,450.0]


    #B=[50.0911,1.64639,30000.0,440.0]
    #A=[46.5622,10.4792,30000.0,440.0]
    
    lmaxim=[]
    
    LONLI=[50.07472229003906, 11.226388931274414,20000,350]
    MANGO=[51.692779541015625, 0.7905555367469788,20000,350]
    TIMUS=[53.68333435058594, 49.43166732788086,20000,350]
    EELDE=[53.21333312988281, 7.216944217681885,20000,350]
    ROBTU=[48.823890686035156, 22.375,20000,350]
    lmaxim.append([LONLI,MANGO])
    lmaxim.append([EELDE,ROBTU])
    lmaxim.append([LONLI,EELDE])
    lmaxim.append([MANGO,ROBTU])
    ROSKO=[69.37027740478516,18.262222290039062,20000,350]
    BALIX=[59,-10,20000,350]
    lmaxim=[[ROSKO,BALIX]]
    #Trajet 6 beaucoup de vent defavorable
    #A=[70,20,35000,440]
    #B=[58,-10,35000,440]
    #lmaxim=[[A,B]]

    amplitude = 0.5
    iT = 0
    #Trajet 5 : trajet 4 reverse
    #B=[58,-10,35000,440]
    #A=[68,20,35000,440]


    #regl0
    #sim.load_map('u_small.npz', 'v_small.npz', 'lats_small.npz','lons_small.npz','altitude_small.npz','time_small.npz')
    #sim.load_map('u_default.npz','v_default.npz','lats_default.npz','lons_default.npz','altitude_default.npz','time_default.npz')
    sim = Simulation3D(A,B,tinit=iT,altMax= 38000)
    sim.load_map('../npz/u.npz', '../npz/v.npz', '../npz/lats.npz','../npz/lons.npz','../npz/altitude.npz','../npz/time.npz')
    #regl 1, 2
    #Ranges Bigger : lat [55,40],logit[0,20],alt[200,1000],time[0,72000]
    #sim.load_map('../npz/u_bigger.npz', '../npz/v_bigger.npz', '../npz/lats_bigger.npz',\
    #          '../npz/lons_bigger.npz','../npz/altitude_bigger.npz','../npz/time_bigger.npz')

    #sim.load_map('../npz/u_Par_Toul.npz', '../npz/v_Par_Toul.npz', '../npz/lats_Par_Toul.npz',\
    #           '../npz/lons_Par_Toul.npz','../npz/altitude_Par_Toul.npz','../npz/time_Par_Toul.npz')
    #range uwind : 30,50] [-30,-8], [0s,20000s]
    #sim.load_map('../npz/u_wind.npz','../npz/v_wind.npz',\
    #                                   '../npz/lats_wind.npz','../npz/lons_wind.npz',\
    #                                   '../npz/altitude_wind.npz','../npz/time_wind.npz')

    print sim.map.rangeT,sim.map.rangeLat,sim.map.rangeLong,sim.map.rangeAlt




    #petit
    #Grand Trajet : gain de 16 kilos sur un trajet de 13000 environ  0.1% youhou
    numbIter =100
    #Petit Trajet :
    nIter=10
    nInit=500
    deca=10
    alpha=0.03
    filterprop=0.1
    propKill=0.5
    limit=20
    ordre = "endtime"
    ch = 0
    #listFlight =  extractFlight.ListFlight()
    l=[]
    if ch == 0:
        i = 0
        for traj in lmaxim:
            dep=traj[0]
            arr=traj[1]
            sim.redefineParam(dep,arr,altMax=38000 )
            name = 'MaxBigtrajet_plot'+str(i)
            p=launchBlind(sim,numbIter,nIter, nInit, deca, alpha,\
                        filterprop,propKill,limit,ordre,name = name,amplitude = amplitude)

            
            
            l.append(p)    
            i = i+1
        c = csv.writer(open("../res/MaxBigtrajetPlots.csv","wb"))
        c.writerow(["name","Cout ortho","Cout p_to_p","cout opti","pourcentage"])
        for pc in l:
            c.writerow(pc)
        l=[]    
          

    if ch == 1:

        numbIter=70
        sim.firstSolution(alt=alt,vitesse=vitesse)
        sim.setSomeParam(numbIter)
        print sim.dt
        tab=sim.heuristiqueStep(nIter = nIter, nInit = nInit, deca = deca, alpha = alpha,vitesse = vitesse,alt = alt,\
                            filterprop=filterprop,propKill=propKill,limit=limit,ordre='distTot')

        cout = ex.evaluate(trajet=[[A[0],A[1],alt,vitesse],[B[0],B[1],alt,vitesse]],timestep=sim.dt*3600,initTime=iT)
        print 'Cout du modele avion : ', cout
        vol = rf.Flight()
        vol.loadFromCSV("simresult.csv")

        print len(sim.particules), ' Particules dans le domaine'
        print len(tab), ' Particules optimales'

        coutmin=100000
        idmin='Avion'
        coutP=[]
        costP=[]
        costWind=[]
        vitesseMoy=[]
        vgTabAv=[]
        vgTabApp=[]
        TAv=[]
        TApp=[]
        flown=[]
        flownAv=[]
        endtime=[]
        endtimeAv=[]
        #itere sur les 10 meilleures particules a priori
        #for part in tab:
        i=0
        imin=0
        for part in tab[0:10]:
            liste=part.path
            cout2=ex.evaluate(trajet=liste,timestep=sim.dt*3600,initTime=iT)
            v = rf.Flight()
            v.loadFromCSV("simresult.csv")
            coutP.append(cout2)
            costP.append(part.distTot)
            costWind.append(part.cost)
            vitesseMoy.append(part.vitmoy)
            vgTabAv.append([float(vgrnd) for vgrnd in v.grd_spd])
            vgTabApp.append(part.tabvg)
            TAv.append([float(time) for time in v.time])
            TApp.append(np.linspace(0,part.endtime,len(part.tabvg)))
            flown.append(part.distTot)
            flownAv.append(float(v.flown_dst[-1]))
            endtime.append(part.endtime)
            endtimeAv.append(float(v.time[-1]))
            print cout2
            if(cout2<=coutmin):
                coutmin=cout2
                idmin=part
                listmin=list(liste)
                imin=i
            i=i+1
        coutmin=ex.evaluate(trajet=listmin,timestep=sim.dt*3600,initTime=iT)
        plt.figure()

        volOpt=rf.Flight()
        volOpt.loadFromCSV("simresult.csv")

        print 'cout trajet dir : ',cout
        print 'cout via heuristique : ',coutmin
        print 'dist dir',vol.flown_dst[-1]
        print 'dist bye',volOpt.flown_dst[-1]
        print(coutP)
        print(vitesseMoy)
        print (flown)
        print (flownAv)
        print endtime
        print endtimeAv


        plt.plot(TAv[imin],vgTabAv[imin])
        plt.hold(True)
        plt.plot(TApp[imin],vgTabApp[imin])
        plt.draw()
        plt.figure()



        plt.figure()
        plt.plot(endtimeAv,coutP,'*')
        plt.draw()
        plt.figure()
        plt.plot(endtime,coutP,'o')
        plt.title('cout en fonction du temps')
        plt.draw()

        plt.figure()
        plt.plot(flownAv,coutP)
        plt.title('cout en fonction de la distance')
        plt.draw()

        plt.figure()
        plt.plot(vitesseMoy,coutP)
        plt.title('cout en fonction vitesse moyenne')
        plt.draw()

        plt.figure()
        plt.plot(costWind,coutP)
        plt.title('cout en fonction du cout vent')
        plt.draw()

        shift=idmin.tabang
        evolshift=[float(i[0]) for i in shift]
        plt.figure()
        plt.plot(evolshift)
        plt.title('Evolution d angle de decallage')
        plt.draw()


        taux=[float(x)/max(float(y),1) for x,y in zip(vol.sum_fuel_burn,vol.flown_dst)]
        taux2=[float(x)/max(float(y),1) for x,y in zip(volOpt.sum_fuel_burn,volOpt.flown_dst)]

        fig=plt.figure()
        plt.plot(vol.time[1:-2],taux[1:-2],'r')
        plt.plot(volOpt.time[1:-2],taux2[1:-2],'b')
        plt.title('comparaison taux de carburant : rouge trajet direct, bleu trajet heuristique')
        plt.draw()
        plt.figure()


        volOpt.plotFlight()
        plt.hold(True)
        vol.plotFlight()
        plt.show()
        plt.pause(10)
        print vol.time[-1],volOpt.time[-1],idmin.endtime

        saveTrajectory(listmin, coutmin)
