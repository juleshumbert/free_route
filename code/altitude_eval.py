import numpy as np
import matplotlib.pyplot as plt
from interpolator import *
from execsim import *
from scipy import interpolate
import csv

c = []
for alt in range(10000, 30000, 1000):
    csv_file = open("../trajectory/contracts_corde.csv", "wb")
    csv_file.write(
        "FlightId, LatitudeDegrees, LongitudeDegrees, AltitudeFeet, AirspeedKnots")
    csv_file.write("\n301817127," + "41, 5," +str(alt)+",300")
    csv_file.write("\n301817127," + "43, 5," +str(alt)+",300")
    csv_file.close()
    c.append(evaluate2())
    print c
