# -*- coding: utf-8 -*-
"""
Created on Thu Feb 04 14:13:05 2016

@author: Rodolphe
"""
import numpy as np
import matplotlib.pyplot as plt
from graph import *
from pylab import *
from execsim import *
from readFlight import *
from orthodromie import *


def nextPoint(A,B,m,angle,pas):

	lat=A[0]
	longi=A[1]
	
	pointList=[]
	pointList.append(newPoint(A,calculateR(A,B),pas)+[A[2],A[3]])
	
	C=pointList[0]		
	latC=C[0]
	longiC=C[1]
	
	for i in range(m):
		angle=angle+i*angle		
		pointList.append([lat+np.sin(angle)*(longiC-longi)+np.cos(angle)*(latC-lat),longi+np.cos(angle)*(longiC-longi)-np.sin(angle)*(latC-lat),A[2],A[3]])	
	
	angle=-angle
	for i in range(m):
		angle=angle+i*angle		
		pointList.append([lat+np.sin(angle)*(longiC-longi)+np.cos(angle)*(latC-lat),longi+np.cos(angle)*(longiC-longi)-np.sin(angle)*(latC-lat),A[2],A[3]])	
	
	oldCost=-1
	choosenPoint=[]
	for point in pointList:
		
		cost=evaluate([point,B])
		if cost<oldCost or oldCost==-1:
			oldCost=cost
			choosenPoint=point
			
	return choosenPoint
	
def optimize(A,B,m,angle,n):
	pas=distanceOrtho(A,B)/n
	path=[A]
	C=A
	for i in range(n-1):
		C=nextPoint(C,B,m,angle,pas)
		print C
		path.append(C)
	
	path.append(B)
	return path

if __name__ == "__main__":
	
#	A=[53.,18.,25000.,300.]
#	B=[42.,1.,25000.,300.]
	
# Paris-Toulouse
	A=[43.6,1.43,25000.,300.]
	B=[48.51,2.20,25000.,300.]

	n=25		#Nombre de points de recherches
	m=6	#Nombre de branches de recherche par point
	angle=0.02	#Angle de recherche en radians
	
	print evaluate(optimize(A,B,m,angle,n))
	
	vol=Flight()
	vol.loadFromCSV("simresult.csv")
	vol.plotFlight()
	
#	C=nextPoint(A,B,m,angle,pas)
#	
#	x=[]
#	y=[]
#	
#	for point in C:
#		y.append(point[0])
#		x.append(point[1])
#	
#	x=x+[B[1],A[1]]
#	y=y+[B[0],A[0]]
#	plot(x,y,'bo')
	
	#plot([A[0],B[0],C[0]],[A[1],B[1],C[1]],'bo')