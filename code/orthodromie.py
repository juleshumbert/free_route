import numpy as np
import matplotlib.pyplot as plt
from pylab import *
from numpy import sin


#A(latitude,longitude) en degres
#B idem
#Attention coordonnees positive dans le sens Nord,Est exemple 
#Paris=[48.,2.] NewYork=[40.,-74]
def calculateR(A,B):
	latA=A[0]*np.pi/180
	latB=B[0]*np.pi/180
	longA=A[1]*np.pi/180
	longB=B[1]*np.pi/180

	R=np.arctan(1/(np.cos(latA)*np.tan(latB)/np.sin(longB-longA)-np.sin(latA)/tan(longB-longA)))
	R=R*180/np.pi

	if longB < longA and R>0:
		R=R-180

	if longB > longA and R<0:
		R=R+180

	return R

#Le pas en NM
def newPoint(A,angle,pas):
	latA=A[0]
	longA=A[1]

	angle=angle*np.pi/180
	deltaY=np.cos(angle)*pas
	deltaX=np.sin(angle)*pas

	deltaLat=deltaY/60 #en NM
	deltaLong=deltaX/(60*cos(latA*np.pi/180))

	B=[(latA+deltaLat),(longA+deltaLong)]

	return B

#Pas en NM
#Renvoie un path : une liste de points espaces de pas (en NM) suivant
#l'orthodromie entre A et B
def orthodromie(A,B,pas=0,n=0):
	path=[]
	listeR=[]
	
	#Calcul du pas le plus proche en dessous du pas demande qui fait tomber 
	#juste 
	if pas !=0:
		n=int(np.floor(distanceOrtho(A,B)/pas)+2)
	
	pas=distanceOrtho(A,B)/n

	for i in range(n):
		path.append(A)
		A=newPoint(A,calculateR(A,B),pas)

	path.append(B)


	return path

#Calcule la distance orthodromique en NM entre deux points A et B
def distanceOrtho(A,B):
	latA=A[0]
	latB=B[0]
	longA=A[1]
	longB=B[1]
	cosi = np.sin(latA*np.pi/180.)*np.sin(latB*np.pi/180.)+  \
				   np.cos(latA*np.pi/180.)*cos(latB*np.pi/180.)*cos((longA-longB)*np.pi/180.)
	if cosi>=1:
		cosi = 1
	if cosi<=-1:
		cosi = -1				   
	dist=60.*180./np.pi*np.arccos(cosi)
				   
	
	return dist
 
#calcule la distance orthodromique en radian
def distanceOrthoRad(A,B):
	latA=A[0]
	latB=B[0]
	longA=A[1]
	longB=B[1]
	cosi = np.sin(latA*np.pi/180.)*np.sin(latB*np.pi/180.)+  \
				   np.cos(latA*np.pi/180.)*cos(latB*np.pi/180.)*cos((longA-longB)*np.pi/180.)
	if cosi>=1:
		cosi = 1
	if cosi<=-1:
		cosi = -1	
	dist = np.arccos(cosi)
	return dist

def plotPath(path):
	x=[]
	y=[]

	for point in path:
		x.append(point[0])
		y.append(point[1])

	axisScale=0.1
	miniLongi=min(y)
	maxiLongi=max(y)
	miniLat=min(x)
	maxiLat=max(x) 
	deltaLongi=maxiLongi-miniLongi
	deltaLat=maxiLat-miniLat

	if deltaLongi==0:
		deltaLongi=10

	if deltaLat==0:
		deltaLat=10

	plot(y,x,'bo')
	axis([miniLongi-axisScale*deltaLongi,maxiLongi+axisScale*deltaLongi,miniLat-axisScale*deltaLat,maxiLat+axisScale*deltaLat])
	show()


if __name__=="__main__":	
	B=[70.,2.]
	A=[70.,-74.]
	pas=50
#	A=[48.,2.]
#	B=[40.,74.]	
	path=orthodromie(A,B,pas)
	plotPath(path)

    