import matplotlib.pyplot as plt
import readFlight as rf
import numpy as np
import os
import csv
import matplotlib.pyplot as plt
from writeContracts import *
from readResult import *
from _ctypes import Array


class buildAlt:

    def __init__(self, dep, arr, coef=0.9, alt_toc=38000):
        self.dep = dep[0:2]
        self.arr = arr[0:2]
        self.alt_dep = dep[2]
        self.alt_arr = arr[2]
        self.alt_toc = alt_toc
        self.coef = coef

        self.alt_montee, self.t_montee = self.montee()
        self.vit_montee = self.alt2vit(self.alt_montee)
        self.dist_montee = np.zeros(len(self.t_montee))
        for i in range(len(self.t_montee) - 1):
            self.dist_montee[i + 1] = self.dist_montee[i] + \
                (self.t_montee[i + 1] - self.t_montee[i]) * \
                self.vit_montee[i] / 3600
        self.dist_to_toc = self.dist_montee[np.argmax(self.alt_montee)]

        self.alt_descente, self.t_descente = self.descente()
        self.vit_descente = self.alt2vit(self.alt_descente)
        self.dist_descente = np.zeros(len(self.t_descente))
        for i in range(len(self.t_montee) - 1):
            self.dist_descente[i + 1] = self.dist_descente[i] + \
                (self.t_descente[i + 1] - self.t_descente[i]
                 ) * self.vit_descente[i] / 3600
        self.dist_to_tod = self.dist_descente[np.argmin(self.alt_descente)]

        dep = {'lat': self.dep[0], 'lon': self.dep[1]}
        arr = {'lat': self.arr[0], 'lon': self.arr[1]}
        dist_tot = self.dist(dep, arr)

    def dist(self, pt1, pt2):
        phia = np.deg2rad(pt1['lat'])
        phib = np.deg2rad(pt2['lat'])
        dl = np.deg2rad(pt2['lon'] - pt1['lon'])
        rt = 6378
        return np.arccos(np.sin(phia) * np.sin(phib)
                         + np.cos(phia) * np.cos(phib) * np.cos(dl)) * rt / 1.852

    def alt2vit(self, alt):
        return 2.669e2 + alt * 4.985e-3

    def taux_montee(self, alt, wgt=169532):
        tc = np.exp(-3.775229 + 3.226931e-6 * alt ** 1.2 +
                    5.633243e-06 * wgt + 2.077038e-11 * alt ** 1.2 * wgt)
        taux = 1 / (tc * ((3.226931e-6 + 2.077038e-11 * wgt) * 1.2 * alt ** 0.2))
        return taux

    def taux_descente(self, alt, wgt=169532):
        return -1 / (0.00017045 * alt ** -0.3)

    def max_alt(self, wgt):
        return 57100 - 0.11793 * wgt

    def montee(self):
        t = np.linspace(1, 3600, 1000)
        alt = np.zeros(1000)
        alt[0] = self.alt_dep
        for i in range(len(t) - 1):
            taux = self.taux_montee(alt[i])
            next_alt = alt[i] + (t[i + 1] - t[i]) * taux / 3600 * self.coef
            if next_alt < 38000:
                alt[i + 1] = next_alt
            else:
                alt[i + 1] = 38000
        return alt, t

    def descente(self):
        t = np.linspace(1, 3600, 1000)
        alt = np.zeros(1000)
        alt[0] = self.alt_toc
        for i in range(len(t) - 1):
            taux = self.taux_descente(alt[i])
            next_alt = alt[i] + (t[i + 1] - t[i]) * \
                taux / 3600 * self.coef * 0.45
            if next_alt > self.alt_arr:
                alt[i + 1] = next_alt
            else:
                alt[i + 1] = self.alt_arr
        return alt, t

    def alt_vit(self, lat, lon):
        point = {'lat': lat, 'lon': lon}
        dep = {'lat': self.dep[0], 'lon': self.dep[1]}
        arr = {'lat': self.arr[0], 'lon': self.arr[1]}
        dist_dep = self.dist(point, dep)
        dist_arr = self.dist(point, arr)
        dist_tot = self.dist(dep, arr)

        if dist_arr < self.dist_to_tod:
            i = 0
            while i < range(len(self.dist_descente)) and self.dist_descente[
                    i] <= self.dist_to_tod - dist_arr:
                i += 1
            return (self.alt_descente[i] + self.alt_descente[i + 1]) / \
                2, (self.vit_descente[i] + self.vit_descente[i + 1]) / 2

        elif dist_dep < self.dist_to_toc:
            i = 0
            while i < range(len(self.dist_montee)) and self.dist_montee[
                    i] <= dist_dep:
                i += 1
            return (self.alt_montee[i] + self.alt_montee[i + 1]) / \
                2, (self.vit_montee[i] + self.vit_montee[i + 1]) / 2
        else:
            return self.alt_toc, self.alt2vit(self.alt_toc)
