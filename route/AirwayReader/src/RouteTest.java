import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import fr.onera.gamme.DataPool;
import fr.onera.gamme.GammeException;
import fr.onera.gamme.provider.BasicProvider;
import fr.onera.gamme.provider.xml.SAXImportProvider;
import fr.onera.gamme.units.quantities.Angle;
import fr.onera.simsky.GAMMEApplication;
import fr.onera.simsky.Route;
import fr.onera.simsky.RouteAvailability;
import fr.onera.simsky.RouteSegment;
import fr.onera.simsky.Timesheet;
import fr.onera.simsky.xml.SimSkyDOMExportProvider;

public class RouteTest {

    public static void main(String[] args) throws FileNotFoundException, GammeException {

        GAMMEApplication.initialize();

        DataPool pool = new DataPool(new BasicProvider(false));
        SAXImportProvider importer = new SAXImportProvider(pool);
        importer.open();
        importer.importEntities(new FileInputStream("routes.xml"));
        importer.close();

        Set<Route> routes = pool.getEntities(Route.class);

        System.out.println("Load " + routes.size() + " routes");

        List<Route> ecacRoutes = new ArrayList<Route>();
        for(Route route : routes) {
            boolean routeOk = false;
            List<RouteSegment> segments = new ArrayList<RouteSegment>();
            for(RouteSegment segment : route.getSegments()) {
                double slat = segment.getStart().getLat(Angle.degree);
                double slon = segment.getStart().getLon(Angle.degree);
                double elat = segment.getStart().getLat(Angle.degree);
                double elon = segment.getStart().getLon(Angle.degree);
                // If one point is in ECAC zone, keep it.
                if(slat >= 25 && slat <= 70 && slon >= -30 && slon <= 40)
                    routeOk = true;
                if(elat >= 25 && elat <= 70 && elon >= -30 && elon >= 40)
                    routeOk = true;
                List<RouteAvailability> pdrCdr1Segments = new ArrayList<RouteAvailability>();
                for(RouteAvailability availability : segment.getAvailability()) {
                    if(availability.getStatus() == RouteAvailability.CodeRouteAvailabilityType.OPEN ||
                            (availability.getStatus() == RouteAvailability.CodeRouteAvailabilityType.COND &&
                            availability.getCdrType() == RouteAvailability.ConditionalRouteType.CDR_1) ) {
                        RouteAvailability a = availability.clone();
                        a.setTimeInterval(new ArrayList<Timesheet>());
                        pdrCdr1Segments.add(a);
                    }
                }
                RouteSegment newSegment = segment.clone();
                newSegment.setAvailability(pdrCdr1Segments);
                segments.add(newSegment);
            }
            Route newRoute = route.clone();
            newRoute.setSegments(segments);
            if(routeOk)
                ecacRoutes.add(newRoute);
        }

        System.out.println("Keep " + ecacRoutes.size() + " in the ECAC zone");

        SimSkyDOMExportProvider exporter = new SimSkyDOMExportProvider(pool, "routes_ECAC.xml");
        exporter.open();
        for(Route route : ecacRoutes)
            exporter.exportEntity(route);
        exporter.close();

    }

}
