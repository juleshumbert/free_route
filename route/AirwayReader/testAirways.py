import sys
sys.path.append("/home/jules/Documents/free_route/AirwayReader/lib/simsky-datamodel.jar")
sys.path.append("/home/jules/Documents/free_route/AirwayReader/lib/simsky-date.jar")
sys.path.append("/home/jules/Documents/free_route/AirwayReader/lib/simsky-storagexml.jar")
sys.path.append("/home/jules/Documents/free_route/AirwayReader/lib/log4j-1.2.16.jar")
sys.path.append("/home/jules/Documents/free_route/AirwayReader/lib/javassist.jar")
sys.path.append("/home/jules/Documents/free_route/AirwayReader/lib/JDataPool.jar")
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.util.ArrayList
import java.util.List
import java.util.Set
import fr.onera.gamme.DataPool
import fr.onera.gamme.GammeException
import fr.onera.gamme.provider.BasicProvider
import fr.onera.gamme.provider.xml.SAXImportProvider
import fr.onera.gamme.units.quantities.Angle
import fr.onera.simsky.GAMMEApplication
import fr.onera.simsky.Route
import fr.onera.simsky.RouteAvailability
import fr.onera.simsky.RouteSegment
import fr.onera.simsky.Timesheet
import fr.onera.simsky.xml.SimSkyDOMExportProvider
#import matplotlib.pyplot as plt
import numpy as np

fr.onera.simsky.GAMMEApplication.initialize()
pool = fr.onera.gamme.DataPool(fr.onera.gamme.provider.BasicProvider(False))
importer = fr.onera.gamme.provider.xml.SAXImportProvider(pool)
importer.open()
importer.importEntities(java.io.FileInputStream("routes.xml"))
importer.close()
r = fr.onera.simsky.Route()
routes = pool.getEntities(r.getClass())
for r in routes:
    print r.getDesignator()
    for s in r.getSegments():
        # plt.plot(np.array([s.getStart().getLat(),s.getEnd().getLat()]), np.array([s.getStart().getLon(),s.getEnd().getLon()]))
